require 'spec_helper'
require_relative '../../helpers/custom_helpers'

describe CustomHelpers do
  class TemplateMock
    extend CustomHelpers

    def self.link_to(text, url, class: '', rel: ''); end
  end

  describe 'handbook_product_page?' do
    context 'handbook product page' do
      let(:relative_path) { 'handbook/product/product-processes/communication.html.md' }

      it 'returns true' do
        expect(TemplateMock.handbook_product_page?(relative_path)).to be(true)
      end
    end

    context 'not handbook product page' do
      let(:relative_path) { 'handbook/product-management/index.html.md' }

      it 'returns false' do
        expect(TemplateMock.handbook_product_page?(relative_path)).to be(false)
      end
    end
  end

  describe 'static_site_editor_link' do
    let(:relative_path) { 'handbook/product/product-processes/communication.html.md' }
    let(:repo) { 'https://gitlab.com/gitlab-com/www-gitlab-com/' }
    let(:link_class) { 'external-link' }

    before do
      allow(TemplateMock).to receive(:link_to)
    end

    it 'generates a link that opens source file in the static site editor' do
      expect(TemplateMock).to receive(:link_to).with(
        'Static Site Editor (alpha)',
        "#{repo}-/sse/#{ERB::Util.url_encode('master/source/handbook/product/product-processes/communication.html.md')}",
        class: link_class
      )

      TemplateMock.static_site_editor_link(repo, relative_path, link_class: link_class)
    end
  end
end
