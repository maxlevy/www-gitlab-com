#!/usr/bin/env ruby
#
# Generate a release post entry file in the correct location.

require 'json'
require 'net/http'
require 'optparse'
require 'yaml'
require 'shellwords'

Options = Struct.new(
  :author,
  :dry_run,
  :force,
  :issue_url,
  :type,
)
INVALID_TYPE = -1
INVALID_TIER = -1

module ReleasePostHelpers
  Abort = Class.new(StandardError)
  Done = Class.new(StandardError)

  MAX_FILENAME_LENGTH = 99 # GNU tar has a 99 character limit

  def capture_stdout(cmd)
    IO.popen(cmd, &:read)
  end

  def fail_with(message)
    raise Abort, "\e[31merror\e[0m #{message}"
  end

end

class ReleasePostOptionParser
  extend ReleasePostHelpers

  Type = Struct.new(:name, :description)
  TYPES = [
    Type.new('top', 'Top feature'),
    Type.new('primary', 'Primary feature'),
    Type.new('secondary', 'Secondary feature'),
    Type.new('deprecation', 'Deprecation'),
  ].freeze
  TYPES_OFFSET = 1

  class << self
    def parse(argv)
      options = Options.new

      parser = OptionParser.new do |opts|
        opts.banner = "Usage: #{__FILE__} [options] [issue url]\n\n"

        opts.on('-f', '--force', 'Overwrite an existing entry') do |value|
          options.force = value
        end

        opts.on('-n', '--dry-run', "Don't actually write anything, just print") do |value|
          options.dry_run = value
        end

        opts.on('-t', '--type [string]', String, "The category of the change, valid options are: #{TYPES.map(&:name).join(', ')}") do |value|
          options.type = parse_type(value)
        end

        opts.on('-h', '--help', 'Print help message') do
          $stdout.puts opts
          raise Done.new
        end
      end

      parser.parse!(argv)

      # Issue URL is everything that remians
      options.issue_url = argv.join(' ').strip.squeeze(' ').tr("\r\n", '')

      options
    end

    def read_type
      read_type_message

      type = TYPES[$stdin.gets.to_i - TYPES_OFFSET]
      assert_valid_type!(type)

      type.name
    end

    private

    def parse_type(name)
      type_found = TYPES.find do |type|
        type.name == name
      end
      type_found ? type_found.name : INVALID_TYPE
    end

    def read_type_message
      $stdout.puts "\n>> Please specify the index for the category of release post item:"
      TYPES.each_with_index do |type, index|
        $stdout.puts "#{index + TYPES_OFFSET}. #{type.description}"
      end
      $stdout.print "\n?> "
    end

    def assert_valid_type!(type)
      unless type
        raise Abort, "Invalid category index, please select an index between 1 and #{TYPES.length}"
      end
    end
  end
end

class Issue
  STAGE_PREFIX = 'devops::'
  GROUP_PREFIX = 'group::'
  CATEGORY_PREFIX = 'Category:'

  def initialize(url)
    @url = url

    uri = URI(url + '.json')
    response = Net::HTTP.get(uri)
    @issue = JSON.parse(response)
  end

  def title
    @issue['title']
  end

  def labels
    @issue['labels'].map{|label| label["title"]}
  end

  def available_in
    available_in = ['ultimate']
    return available_in if labels.include? 'GitLab Ultimate'

    available_in.unshift('premium')
    return available_in if labels.include? 'GitLab Premium'

    available_in.unshift('starter')
    return available_in if labels.include? 'GitLab Starter'

    available_in.unshift('core')
  end

  def group_label
    labels.find{|label| label.start_with? GROUP_PREFIX}
  end

  def stage_label
    labels.find{|label| label.start_with? STAGE_PREFIX}
  end

  def stage
    stage_label.delete_prefix(STAGE_PREFIX)
  end

  def categories
    category_labels = labels.select{|label| label.start_with? CATEGORY_PREFIX}
    category_labels.map{|label| titleize(label.delete_prefix(CATEGORY_PREFIX))}
  end

  def url
    @url
  end

  private

  def titleize(x)
    "#{x.split.each{|word| word.capitalize!}.join(' ')}"
  end
end

class ReleasePostEntry
  include ReleasePostHelpers

  attr_reader :options

  def initialize(options)
    @options = options
    @issue = Issue.new(options.issue_url)
    @reporter = reporter
  end

  def execute
    assert_feature_branch!
    assert_issue_url!
    assert_new_file!

    # Read type from $stdin unless it is already set
    options.type ||= ReleasePostOptionParser.read_type
    assert_valid_type!

    $stdout.puts "\n\e[32mcreate\e[0m #{file_path}"
    $stdout.puts contents

    $stdout.puts "\n\e[34mhint\e[0m add a screenshot, commit, and push your changes!"
    $stdout.puts push_hint

    unless options.dry_run
      write

      if editor
        system("#{editor} '#{file_path}'")
      end
    end
  end

  private

  def contents
    return deprecation_content if options.type == 'deprecation'

    feature_content
  end

  def feature_content
    yaml_content = YAML.dump(
      'features' => {
        options.type => [
          {
            'name' => @issue.title,
            'available_in'  => @issue.available_in,
            'gitlab_com' => true,
            'documentation_link' => 'https://docs.gitlab.com/ee/#amazing',
            'image_url' => '/images/unreleased/feature-a.png',
            'reporter' => reporter,
            'stage' => @issue.stage,
            'categories' => @issue.categories,
            'issue_url' => @issue.url,
            'description' => "Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.\nPerferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.\n",
          }
        ]
      }
    )
    remove_trailing_whitespace(yaml_content)
  end

  def deprecation_content
    yaml_content = YAML.dump(
      'deprecations' => {
        'feature_name' => @issue.title,
        'documentation_link' => 'https://docs.gitlab.com/ee/#amazing',
        'reporter' => reporter,
        'due' => 'April 22, 2019',
        'issue_url' => @issue.url,
        'description' => "Lorem ipsum [dolor sit amet](#link), consectetur adipisicing elit.\nPerferendis nisi vitae quod ipsum saepe cumque quia `veritatis`.\n",
      }
    )
    remove_trailing_whitespace(yaml_content)
  end

  def push_hint
    <<-EOT
    git add #{file_path}
    git commit -m "#{@issue.title}
    git push -u origin
    EOT
  end

  def write
    File.write(file_path, contents)
  end

  def editor
    ENV['EDITOR']
  end

  def assert_feature_branch!
    return unless branch_name == 'master'

    fail_with "Create a branch first!"
  end

  def assert_new_file!
    return unless File.exist?(file_path)
    return if options.force

    fail_with "#{file_path} already exists! Use `--force` to overwrite."
  end

  def assert_issue_url!
    return unless options.issue_url.length < 1

    fail_with "Provide an issue URL for the release post item"
  end

  def assert_valid_type!
    return unless options.type && options.type == INVALID_TYPE

    fail_with 'Invalid category given!'
  end

  def file_path
    base_path = File.join(
      unreleased_path,
      branch_name.gsub(/[^\w-]/, '-'))

    # Add padding for .yml extension
    base_path[0..MAX_FILENAME_LENGTH - 5] + '.yml'
  end

  def unreleased_path
    path = File.join('data', 'release_posts', 'unreleased')

    path
  end

  def branch_name
    @branch_name ||= capture_stdout(%w[git symbolic-ref --short HEAD]).strip
  end

  def reporter
    username = ENV['GITLAB_USERNAME'] || capture_stdout(%[git config gitlab.username]).strip

    if username == ''
        $stdout.puts "\n\e[34mhint\e[0m configure your GitLab username to save time"
        $stdout.puts "    git config --global gitlab.username YOUR_USERNAME"

        username = 'pm1'
    end

    return username
  end

  def remove_trailing_whitespace(yaml_content)
    yaml_content.gsub(/ +$/, '')
  end
end

if $0 == __FILE__
  begin
    options = ReleasePostOptionParser.parse(ARGV)
    ReleasePostEntry.new(options).execute
  rescue ReleasePostHelpers::Abort => ex
    puts ex.message
    exit 1
  rescue ReleasePostHelpers::Done
    exit
  end
end

# vim: ft=ruby
