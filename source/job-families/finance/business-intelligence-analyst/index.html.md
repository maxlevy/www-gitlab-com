---
layout: job_family_page
title: "Business Intelligence Analyst"
---

The Business Intelligence Analyst is a talented, driven individual who has a passion for solving problems.
At GitLab, you get to do more than just write code, you get to build solutions as an engineer with the largest all remote company in the world.
You will be involved in a mission critical project working with smart, dedicated people like yourself, and making an impact with both internal stakeholders and external investors.

## Responsibilities
- Crack tough business problems and present insights in a compelling way that influences action
- Design and develop BI Strategies to include assisting with ETL, data modeling and building new reports and dashboards.
- Develop, document and implement data models within Snowflake, GitLab’s data warehouse
- Develop financial and predictive analysis
- Assist in systems, scoping and requirements gathering process
- Provide technical guidance and mentoring to cross functional analysts
- Understand and document the full lifecycle of data
- Expand our database with clean data (ready for analysis) by implementing data quality tests while continuously reviewing, optimizing, and refactoring existing data models
- Contribute to and implement data warehouse and data modeling best practices, keeping reliability, performance, scalability, security, automation, and version control in mind
- This position reports to the Manager, FP&A

## Requirements
- 3+ years experience in an analytics role. Preferred experience in consulting, investment banking or a high growth startup
- Experience building data models, reports and dashboards in a data visualization tool
- Experience cleaning and modeling large quantities of raw, disorganized data
- Experience with a variety of data sources including SalesForce, Zuora, and NetSuite.
- Demonstrate capacity to clearly and concisely communicate complex business logic, technical requirements, and design recommendations through iterative solutions
- Deep understanding of SQL in analytical data warehouses (we use Snowflake SQL) and in business intelligence tools (we use Periscope)
- Hands on experience working with SQL, Python, dbt or another modeling language
- Working knowledge of GitLab, Git and the command line
- Working knowledge deploying cohort and predictive models into production
- Deep understanding of relational and non-relational databases, SQL and query optimization techniques, and demonstrated ability to both diagnose and prevent performance problems
- Positive and solution-oriented mindset
- Comfort working in a highly agile, intensely iterative environment
- Self-motivated and self-managing, with strong organizational skills
- Ability to thrive in a fully remote organization
- Share and work in accordance with our values
- Successful completion of a background check
- Ability to use GitLab

## Performance Indicators
- [Adoption of Data Team BI charts throughout company](/business-ops/metrics#adoption-of-data-team-bi-charts-throughout-company)

## Job Grade

The Business Intelligence Analyst is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).


## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our team page.

- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 30 minute interview with a Data Analyst
- Next, candidates will be invited to schedule a 45 minute interview with our Manager, FP&A
- Next, candidates will be invited to schedule a 30 minute interview with our Internal Strategy Consultant
- Next, candidates will be invited to schedule a 30 minute interview with our Senior Manager, Sales Strategy
- Next, candidates will be invited to schedule a 30 minute interview with our VP, Finance
- Finally, candidates may be asked to interview with the CEO
- Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our hiring page.
