---
layout: job_family_page
title: "Professional Services Project Manager"
---

To learn more, see the [Professional Services Engineer handbook](/handbook/customer-success/professional-services-engineering)

## Summary

Manage high-complexity projects to successful completion from initiation through delivery to closure. The Gitlab Consulting team is looking for a Senior Project Manager to join the Professional Services team to work with our global customers. In this role, you will coordinate with cross-functional teams to complete distinct projects both on time and within budget. You'll need to have substantial experience managing IT and software consulting projects and relying on traditional systems and software development methodologies, as well as practical experience with agile project management methodologies.

## Job Grade

The Senior Professional Services Project Manager is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).  

## Responsibilities

- Develop and manage schedules to deliver technical and consulting services to GitLab customers
- Manage customer expectations, project scope, and resources needed to successfully deliver to customers’ needs
- Develop and communicate project, portfolio, and program status, risks and issues to all levels of stakeholders, ranging from technical staff to executives
- Manage customer escalations, coordinating with customers and GitLab stakeholders (e.g., executive team, sales, technical account managers, etc.)
- Be accountable for reliable and repeatable program delivery processes, metrics and operational procedures
- Act as a liaison to subcontractors and/or delivery partners
- Develop a foundational knowledge of Gitlab’s technologies
- Maintain an awareness of emerging Gitlab's technologies

## Requirements

- Knowledge of specific industry project management and technical delivery methodologies for software (e.g., Project Management Institute (PMI) methodologies, agile/scrum, and/or software SDLC)
- Bachelor's degree in engineering, computer science or related field
- 5+ years of experience working as a project manager on IT-based projects
- 3+ years experience managing projects with external customers or clients, preferably enterprise customers. Professional Services experience is a plus.
- Demonstrated ability to motivate the advisor team and individual contributors and mediate conflicts
- Experience using various project management and/or agile tools
- Understanding of software development lifecycle, preferably experience in an agile and/or DevOps environment
- Excellent customer-facing and internal communication skills
- Great written and verbal communication skills
- Solid organizational skills, including attention to detail and ability to handle multiple priorities
- Willingness to be present on customer sites and to travel up to 20%
- Demonstrated ability to think strategically about business, products, and technical challenges
- Ability to use GitLab

## Performance Indicators

* [CSAT](/handbook/support/support-ops/#customer-satisfaction-survey-csat ) >8.0/10 and [Project Margin](/handbook/customer-success/professional-services-engineering/#long-term-profitability-targets ) > 20% for assigned projects

## Hiring Process

Candidates can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

- Qualified candidates for the Professional Services Project Manager will receive a short questionnaire.
- Selected candidates will be invited to schedule a [screening call](/handbook/hiring/#screening-call) with a member of our Recruiting team.
- Next, candidates will be invited to schedule a first interview with our Director of Customer Success.
- For the Professional Services Project Manager role, candidates will be invited to schedule an interview with a Customer Success peer and may be asked to participate in a demo of a live install of GitLab.
- For the Federal Professional Services Project Manager role, candidates will be invited to schedule interviews with members of the Customer Success team and our Federal Regional Sales Director.
- Candidates may be invited to schedule an interview with our VP of customer success.

Additional details about our process can be found on our [hiring page](/handbook/hiring/interviewing/).
