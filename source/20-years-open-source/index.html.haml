---
title: 20 Years of Open Source
description: 20 years of open source
canonical_path: "/20-years-open-source/"
suppress_header: true
extra_css:
  - twenty-years.css
extra_js:
  - twenty-years.js
---
.twenty-years-header
  %h1 20 years of open source
.twenty-years-container
  .tile
    %p
      Long before the term “open source” was a thing, pre-1980s, most software was effectively free and open source. It was common for source code to be distributed freely, encouraging a hacker culture which is the basis of free and open source philosophy. But as copyright and restrictive licensing for software began to encroach on these freedoms, Richard Stallman saw this culture threatened, and advocated that
      %a{ href: "https://www.gnu.org/philosophy/why-free.html", target: "_blank" }software should not have owners.
      He argued that society "needs information that is truly available to its citizens  –  for example, programs that people can read, fix, adapt, and improve, not just operate," and that “above all, society needs to encourage the spirit of voluntary cooperation in its citizens. When software owners tell us that helping our neighbors in a natural way is ‘piracy,’ they pollute our society’s civic spirit."
    %p
      This philosophy drove Stallman to launch the
      %a{ href: "https://www.gnu.org/", target: "_blank" }GNU Project,
      spurred the free software movement, and is the basis for the open source movement, both of which we owe so much to today. It is thanks to this groundwork that "open source" and its accompanying principles were established in 1998. Because of that movement, GitLab is home to hundreds of thousands of public projects, and is built and improved on by a 2,000 community of contributors. Join us on a journey from open source’s roots in the 1980s to where it is today, as we celebrate 20 years of free and open source software.

.timeline-container
  .event
    .year
      .dot
      %h1 1983
    .tile.event-tile
      = image_tag "/images/20-years-open-source/1983-gnu-project.svg", class: "event-tile-image", alt: "20 years open source gnu project gitlab svg"
      %h2 GNU Project launches
      %p
        Richard Stallman, an alumnus of MIT’s Artificial Intelligence Lab, created the GNU Project inspired by his ethos of free software. The project went on to create a free operating system as part of the journey to a
        %a{ href: "https://www.gnu.org/philosophy/free-sw.html", target: "_blank" }free software
        community, making way for the “copyleft” movement and the GNU General Public License (GPL).
    .callout.event-tile
      %h3
        %i.fas.fa-star
        Free software
      %p Software that users have the freedom to run, copy, distribute, study, change, and improve at will.
    .callout.event-tile
      %h3
        %i.fas.fa-star
        CopyLeft
      %p A method of licensing in which anyone can freely use, read, modify, and distribute the code but cannot apply additional licensing restrictions. The copyleft method, which would eventually manifest as the GNU General Public Licence (GPL), ensured any software created by extracting from or building on free software remained free. If someone distributed modifications of the code, in addition to the modified software they had to make the corresponding source code available.

  .event
    .year
      .dot
      %h1 1990s
    .tile.event-tile
      %h2 Free software explosion
      %p Much of the best-known, most ubiquitous free software today, has its roots in the ‘90s. Free operating system and desktop environment projects like Debian (1993) and GNOME (1999) took off, laying the groundwork for building a truly FOSS ecosystem. The 1995 release of the Apache HTTP Server, a cross-platform web server, was key to the early growth of the World Wide Web. Apache continues to be used to serve a large proportion of websites today.

  .event
    .year
      .dot
      %h1 1991
    .tile.event-tile
      = image_tag "/images/20-years-open-source/1991-linux-kernel.svg", class: "event-tile-image", alt: "20 years open source linux kernel gitlab svg"
      %h2 Advent of the Linux kernel
      %p The Linux kernel, an open source, monolithic and Unix-like operating system kernel, was originally created by Linus Torvalds for his personal computer as a hobby, with no vision for any big, professional application. However, together, the kernel and GNU OS created the first completely free software operating system.

  .event
    .year
      .dot
      %h1 1997
    .tile.event-tile
      %h2 “The Cathedral and the Bazaar”
      %i Published by Eric Raymond
      %p
        %a{ href: "http://www.catb.org/esr/writings/homesteading/cathedral-bazaar/", target: "_blank" } This comparison between the development styles of the GNU and Linux projects
        reflects on the hacker community and software development principles.
      %p
        Raymond likens the style of development of what he thought to be “the most important software” to the cathedral, and Linux and GNU’s style to the bazaar:
      .flex-container
        .full-width
          %h3.margin-top20 The cathedral
          %p Source code is developed by a small group and not released until it’s complete.
        .full-width
          %h3.margin-top20 The bazaar
          %p Source code is developed out in the open, over the internet.
      %p.margin-top20
        The subversive style of Linux and others was intriguing to Raymond, and made him reconsider his attitudes. The essay was hugely influential and a driving force behind Netscape’s
        %a{ href: "http://web.archive.org/web/20021001071727/wp.netscape.com/newsref/pr/newsrelease558.html", target: "_blank" }open sourcing of Netscape Communicator in 1998,
        prompting the
        %a{ href: "https://www.mozilla.org/en-US/about/history/details/", target: "_blank" }launch of the Mozilla project
        which would go on to create free and open source browser Mozilla Firefox.

  .milestone
    .milestone-content
      %h1 1998
      %h3
        Open source says
        %br
        “Hello, world”

  .event
    .year
      .dot
      %h1 1998
    .tile.event-tile
      = image_tag "/images/20-years-open-source/1998-open-source.svg", class: "event-tile-image", alt: "20 years open source gitlab svg"
      %h2 “Open source” is born
      %i January 1998
      %p Up until this point, free software was not widely adopted commercially. Plagued by politics and associated with the GPL which many considered far too liberal (“cooperation is more important than copyright”), the movement was unpopular with corporations, who often viewed free software as a threat to their own products.
      %p Ahead of the release of Netscape Communicator’s source code, a strategy session was held in Palo Alto with some of the major influencers of the free software movement, including Linus Torvalds, Tim O’Reilly, James Zawinski, and others. To capitalize on the attention and momentum from the Navigator news, they rebranded “free software” as “open source,” to distance from the political associations of the free software movement, and bring the principles of free software to the commercial software industry. Richard Stallman rejected the term, preferring “free software” and its associated message of social values and users’ freedom, resulting in division among the free and open source community over which terminology they identify with. Free and open source software (FOSS) encompasses both terms.

    .tile.event-tile
      %h2 Open Source Initiative established
      %i February 1998
      %p To evangelize “open source,” Raymond founded the Open Source Initiative, citing “higher quality, better reliability, greater flexibility, lower cost, and an end to predatory vendor lock-in” as some of the primary reasons to advocate for open source. Commercial companies did not welcome open source at first, with one Microsoft executive famously claiming that “Open source is an intellectual property destroyer. I can’t imagine something that could be worse for the software business and the intellectual property business.”
    .callout.event-tile
      %h3
        %i.fas.fa-star
        Principles of open source
      %p Open source doesn't just mean access to the source code. The distribution terms of open-source software must comply with the following criteria:
      %ol
        %li Free redistribution
        %p The license shall not restrict any party from selling or giving away the software as a component of an aggregate software distribution containing programs from several different sources. The license shall not require a royalty or other fee for such sale.
        %li Source Code
        %p The program must include source code, and must allow distribution in source code as well as compiled form. Where some form of a product is not distributed with source code, there must be a well-publicized means of obtaining the source code for no more than a reasonable reproduction cost, preferably downloading via the Internet without charge. The source code must be the preferred form in which a programmer would modify the program. Deliberately obfuscated source code is not allowed. Intermediate forms such as the output of a preprocessor or translator are not allowed.
        %li Derived Works
        %p The license must allow modifications and derived works, and must allow them to be distributed under the same terms as the license of the original software.
        %li Integrity of the author's source code
        %p The license may restrict source-code from being distributed in modified form only if the license allows the distribution of "patch files" with the source code for the purpose of modifying the program at build time. The license must explicitly permit distribution of software built from modified source code. The license may require derived works to carry a different name or version number from the original software.
        %li No discrimination against persons or groups
        %p The license must not discriminate against any person or group of persons.
        %li No discrimination against fields of endeavor
        %p The license must not restrict anyone from making use of the program in a specific field of endeavor. For example, it may not restrict the program from being used in a business, or from being used for genetic research.
        %li Distribution of license
        %p The rights attached to the program must apply to all to whom the program is redistributed without the need for execution of an additional license by those parties.
        %li License must not be specific to a product
        %p The rights attached to the program must not depend on the program's being part of a particular software distribution. If the program is extracted from that distribution and used or distributed within the terms of the program's license, all parties to whom the program is redistributed should have the same rights as those that are granted in conjunction with the original software distribution.
        %li License must not restrict other software
        %p The license must not place restrictions on other software that is distributed along with the licensed software. For example, the license must not insist that all other programs distributed on the same medium must be open-source software.
        %li License must be technology-neutral
        %p No provision of the license may be predicated on any individual technology or style of interface.
      %p
        From
        %a{ href: "https://opensource.org/osd", target: "_blank" } opensource.org/osd

  .milestone
    .milestone-content
      %h1 1999
      %h3 Gathering momentum

  .event
    .year
      .dot
      %h1 1999
    .tile.event-tile
      %h2 Apache license created
      %i June 1999
      %p Formed in June 1999, the Apache Software Foundation (ASF) is a decentralized community of developers producing free and open source software. Apache’s license, a permissive free software license, allows for derivative or modified works to be distributed under a different license to the original. This offered greater flexibility than the GPL, paving the way for commercial interest to monetize open source software. The ASF also pioneered a new way for FOSS and corporations to interact: companies could be represented in the foundation by individual participants. This allowed for businesses to have an influence while maintaining the project’s autonomy.
    .tile.event-tile
      %h2 Red Hat goes public
      %p Red Hat, founded in 1993, brings FOSS to the enterprise by testing and contributing to it on behalf of customers to ensure reliability, security, and suitability. The company’s IPO garnered the eighth-largest first-day gain in the history of Wall Street; further affirmation that open source and business don’t need to compete, but can work together for everyone’s benefit. Red Hat would go on to become the first billion-dollar open source company, surpassing $US 1.13 billion in annual revenue in 2012.

  .event
    .year
      .dot
      %h1 2004
    .tile.event-tile
      %h2 Ubuntu is released
      %p
        Produced by Canonical and the FOSS community. Ubuntu is an award-winning free and open source operating system and Linux distribution, based on Debian’s architecture and infrastructure. It’s named after the southern African philosophy of ubuntu, loosely translating to “humanity to others” or “a person is a person because of other people.”
        %a{ href: "http://thecloudmarket.com/stats", target: "_blank" }Ubuntu is the most popular operating system for the cloud,
        and is the reference operating system for OpenStack.

  .event
    .year
      .dot
      %h1 2005
    .tile.event-tile
      = image_tag "/images/20-years-open-source/2005-git-released.svg", class: "event-tile-image", alt: "20 years open source git release gitlab svg"
      %h2 Git is released
      %i Created by Linus Torvalds
      %p Git, the most popular distributed version control system (DVCS), was developed by the creator of Linux because he thought no existing open source DCVS was fit for his needs as a kernel maintainer. He and other Linux Kernel maintainers had been using BitKeeper, a proprietary DVCS, but their free-of-charge license was revoked when a Samba maintainer started to reverse-engineer it with a view to creating an open source replacement tool. Over time, Torvalds and other developers grew Git from a simple content tracker to a powerful DVCS, allowing developers to work asynchronously, across the globe, on the same code. Git forms the basis of modern code collaboration tools like GitHub, GitLab, and Bitbucket.

  .milestone
    .milestone-content
      %h1 2008
      %h3 Open source enters the spotlight

  .event
    .year
      .dot
      %h1 2008
    .tile.event-tile
      %h2 GitHub launches
      %p GitWeb built on top of Git to provide a web interface for browsing a Git repository, while SourceForge offered the first glimpse of modern code collaboration by offering a central location to host and manage free, open source projects. What really changed the landscape though, was the launch of GitHub in 2008: by applying modern communication features inspired by social media sites, GitHub empowered social coding. It provided the first truly accessible UI to manage and review feature branches, and the ability to merge them with one-click “Pull Requests.” As a result, FOSS projects flocked to GitHub as a place to not only host code, but to grow a community as well. The speed of innovation and network effect arising out of GitHub made open source attractive to enterprises.
    .tile.event-tile
      %h2 Google releases Android as open source
      %p The first version of the smartphone operating system was released as open source in September 2008. Android is now the world’s most popular mobile platform. Android is based on the Linux kernel, so its popularity means that Linux is now the dominant kernel on both mobile platforms (via Android), and supercomputers, and is a key player in server operating systems.
    .tile.event-tile
      = image_tag "/images/20-years-open-source/2008-free-software-legal.svg", class: "event-tile-image", alt: "20 years open source legal gitlab svg"
      %h2 Free software licenses ruled legally binding
      %p In an important legal milestone for the FOSS movement, the US federal appeals court ruled that free software licenses definitely do set legally binding conditions on the use of copyrighted work, and are therefore enforceable under existing copyright law. What this means is that end users in violating of the licensing conditions lose their license, thus infringing copyright. Although this poses a licensing risk, most commercial software vendors continue to use open source software in commercial products, while adhering to the license terms.

  .event
    .year
      .dot
      %h1 2010
    .tile.event-tile
      %h2 Oracle acquires MySQL
      %p After Sun Microsystems acquired popular open source database MySQL in 2008, Sun itself was acquired by Oracle in 2010, at which point all its copyrights, patents, and trademark became the property of Oracle – including MySQL. Oracle’s attempts to commercialize it understandably ruffled feathers in the FOSS community, with the prevailing sentiment being that it was treated as an unnecessary obstacle to people using Oracle’s own database software. This, together with Oracle’s treatment of other open source projects Java and OpenOffice, drew attention to the sometimes problematic relationship between corporations and open source software. Uncertainty over the database’s future led to the community forking the project into new database systems beyond Oracle’s control, a cautionary tale that the community is still key to open source projects’ success.

  .event
    .year
      .dot
      %h1 2011
    .tile.event-tile
      %h2 GitLab created
      %i
        First commit by co-founder
        %a{ href: "/company/team/#dzaporozhets" }Dmitriy Zaporozhets
      %p
        Wishing for a tool to collaborate easily with his team, our CTO and co-founder created GitLab from his house in Ukraine – there was no running water either, but this didn’t strike him as nearly as pressing an issue as a better solution for code collaboration.
        %a{ href: "https://gitlab.com/gitlab-org/gitlab-ce/commit/9ba1224867665844b117fa037e1465bb706b3685", target: "_blank" }With this first commit,
        Zaporozhets began developing GitLab as an open source project with a community which would grow to the nearly 2,000-strong contributors we have today.

  .event
    .year
      .dot
      %h1 2014
    .tile.event-tile
      = image_tag "/images/20-years-open-source/2014-kubernetes-launches.svg", class: "event-tile-image", alt: "20 years open source kubernetes gitlab svg"
      %h2 Kubernetes launches
      %i Created by Google
      %p The open source container orchestration platform, designed by Google, has become one of the largest and most contributed-to open source projects and emerged as the market leader for container orchestration.

  .event
    .year
      .dot
      %h1 2015
    .tile.event-tile
      %h2 Microsoft open sources Visual Studio Code
      %p The popular source code editor for Windows, Linux and macOS was developed by Microsoft and open sourced on GitHub. This came shortly after the open sourcing of their .NET framework in 2014, and was followed by the open sourcing of PowerShell, the task-based command-line shell and scripting language, in 2016. These moves provide a counterpoint to the controversial relationship between Microsoft and open source in the past.

  .event
    .year
      .dot
      %h1 2018
    .tile.event-tile
      %h2 GitHub acquired by Microsoft
      %p GitHub, home to over 85 million code repositories, was acquired by Microsoft for US$ 7.5 billion in June 2018. The news has already made waves in the FOSS community, many of whom are skeptical of Microsoft’s intentions. The ramifications for the popular open source code hosting and collaboration platform and its community have yet to be seen, however the acquisition is confirmation of the growing influence of software developers.

  .milestone
    .milestone-content
      %h1 Beyond
      %h3 It's up to you!

.content-container
  .tile
    %p
      Get involved.
      = link_to "Become a contributor to GitLab", "/contributing"
      or join one of the thriving
      = link_to "open source projects", "/open-source"
      using GitLab to make it easy for everyone to contribute. If you want to learn more and get the lowdown direct from source, we'll be at OSCON at booth 611 from July 18-19. Come say hi!
