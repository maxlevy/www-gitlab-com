---
layout: markdown_page
title: "Scaling Working Group"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Attributes

| Property        | Value           |
|-----------------|-----------------|
| Date Created    | June 22, 2020 |
| End Date | TBD |
| Slack           | [#wg_scaling](https://gitlab.slack.com/archives/C016JU3CZKJ) (only accessible from within the company) |
| Google Doc      | [Scaling Working Group Agenda](https://drive.google.com/drive/search?q=scaling%20working%20group) (only accessible from within the company) |
| Issue Board     | TBD             |

### Background

This group was formed out of the conclusion of the [Sharding Working Group](/company/team/structure/working-groups/sharding).  Our goals, exit criteria, etc. are still being defined.

## Roles and Responsibilities

| Working Group Role                       | Person                          | Title                                    |
|------------------------------------------|---------------------------------|------------------------------------------|
| Executive Stakeholder                    | Christopher Lefelhocz           | VP of Development           |
| Facilitator                              | Craig Gomes                     | Engineering Manager, Memory/Database            |
| DRI for Scaling  Working Group           | Craig Gomes                     | Engineering Manager, Memory/Database            |
| Functional Lead                          | Nailia Iskhakova                | Software Engineer in Test, Database      |
| Functional Lead                          | Josh Lambert                    | Group Manager, Product Management, Enablement  |
| Functional Lead                          | Gerardo "Gerir" Lopez-Fernandez | Engineering Fellow, Infrastructure       |
| Functional Lead                          | Andreas Brandl                  | Staff Backend Engineer, Database         |
| Member                                   | Chun Du                         | Director of Engineering, Enablement      |
| Member                                   | Tanya Pazitny                   | Quality Engineering Manager, Enablement  |
| Member                                   | Mek Stittri                     | Director of Quality Engineering          |