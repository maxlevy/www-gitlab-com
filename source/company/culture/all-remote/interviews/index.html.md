---
layout: handbook-page-toc
title: "How do you conduct interviews remotely?"
canonical_path: "/company/culture/all-remote/interviews/"
description: How do you conduct interviews remotely?
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're curating a list of questions that we at GitLab appreciate hearing answers to from others in remote organizations. We'll also address a popular question: "How do you interview remotely?"

## How do you interview remotely?

Whether you're wanting to learn how to hire remotely, or just wanting to know how to get hired for a remote position, it helps to know how you should prepare for a remote interview. You only get one chance to make a first impression, so keep these tips in mind.

## How do I assess culture fit remotely?

For hiring teams, a common challenge is this: "How do I assess culture fit remotely?"

To answer this question, you first need to unlearn a bit, and change your frame of mind. A hiring manager should *not* aspire to assess culture fit. Rather, you should aspire to assess *values fit*. 

For many, it is assumed that culture is simply the aura, energy, or vibe one gets when walking into an office. This is largely driven by decor and personas in the room. It is dangerous to allow company culture to be dictated by such factors, as this will create an oscillating culture that changes depending on mood or socioeconomic conditions. 

A company culture is a company's list of values. Culture is an assurance that each employee respects, admires, and feels invested in a company's values, and that leadership works to ensure values are not violated. As GitLab, a sub-value within our Diversity, Inclusion & Belonging  value is "[culture fit is a bad excuse](https://about.gitlab.com/handbook/values/#culture-fit-is-a-bad-excuse)."

Remote interviewers should link a company's values during the interview and have a conversation to assess a candidate's alignment and understanding of those values. Particularly in a remote setting, [values](/handbook/values/) serve as the north star, guiding every business decision by people you cannot physically see and shaping how colleagues treat one another. 

### Hiring managers/recruiters/interviewers/etc.

Conducting a remote interview is all about preparation, and most importantly, setting expectations for the person being interviewed. You also have technical considerations to make since the person joining the video/conference call is using a different setup than you, or they may not have interviewed remotely before. Be clear about how to join the interview and the technical requirements. Make sure the process and/or email templates are documented internally so that others can communicate this process to future interviewees.

1. **Specify if it is a phone or video interview.** Nothing can derail the interview faster than being caught offguard. Communicate this ahead of time.
1. **Give instructions for how to connect to the video call.** Include download instructions for the software you're using or a step-by-step guide if connecting from the web.
1. **Give the candidate an outline.** The team at [FlexJobs](https://www.flexjobs.com/employer-blog/best-practices-conducting-remote-interviews/) recommends an outline with an interview start time, the name of the interviewer and anyone else they will be meeting, and the general order of events so they know when to speak.
1. **Have a practice meeting.** Use this opportunity to make sure your audio and video is coming through clearly.
1. **Have questions prepared and in front of you ahead of time.** Owl Labs has prepared [this list of remote questions](https://www.owllabs.com/blog/remote-job-interview-questions) if you need some inspiration.
1. **Share the process and documentation with others.** Make templates and processes available to other interviewers so that everyone communicates effectively.

### Remote job candidates/interviewees/etc.

Remote interviews are just like in-person interviews in many ways: You're still trying to make a good impression and communicate your strengths, you're just not doing it from the same room. One of the ways that remote interviews can go wrong is if you don't take into account your environment and let yourself become distracted. Preparation is just as important in a remote interview as an in-person interview.

1. **Read instructions from your interviewer carefully (and ask questions if something is unclear).** Make sure that you understand the requirements for the interview and make sure to download any programs and test ahead of time.
2. **Have a distraction-free area for the interview, if possible.** Background noise or clutter can take attention away from you, which is not what you want.
3. **Make eye contact.** Look into the camera as much as possible, not the screen. 
4. **Have a practice meeting.** Try to download any software for a video call far enough in advance to test your audio and video settings.

### Bonus best practices

1. **Don't casually browser the internet on your computer during an interview.** If you wear glasses, the other person may be able to see what is on your screen and that can be a little embarrassing. Only have what you need for the interview and don't get distracted.

1. **Use headphones with a microphone.** Not only will this make you sound better, it will reduce listening fatigue for the other person as well so that you can have a better conversation. One of the more common phrases you'll hear from GitLab CEO Sid Sijbrandij is, "[Can you put your headphones on?](https://about.gitlab.com/blog/2019/06/28/five-things-you-hear-from-gitlab-ceo/)"

## General remote-work interview questions

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/IFBj9KQSQXA" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->


GitLab seeks to learn from others who are embracing remote work. This list of questions is directed at other peers working within a remote organization. Some are aimed at executives and hiring managers. In the video above, found on the [GitLab Unfiltered YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A), Sid asks the below questions to InVision Chief People Office Mark Frein.

We are working on a submission process for remote companies who want to engage in interviews with GitLab to share their perspective on these questions and more — stay tuned! 

1. What’s driving society’s appetite for workplace flexibility and remote opportunities? 
1. What qualities are important to look for in new hires as you scale a remote organization?
1. How does all-remote contribute to people being themselves/vulnerable?
    What benefits have you seen manifest from this?
1. Do you need special leadership qualities to lead an all-remote team, or does what you learn leading traditional teams transfer over? 
1. To what degree can remote comfortability be learned?
    - Is it easier to learn/adapt than learning to function in a co-located organization?
1. Do you feel that all-remote organizations contribute to solving cost-of-living, traffic and housing crises in cities like San Francisco and Los Angeles? 
1. Do you feel that all-remote organizations are more inclusive by their nature?
    - Is all-remote a mechanism to tackle ageism in the workplace? 
1. What’s the impact of bringing skilled jobs to rural communities and underserved countries?
    - What kind of GDP-altering potential is there if all-remote itself is scaled?
1. Can existing co-located organizations transition to all-remote?
1. What encouragement would you give new startups today to structure their company as an all-remote entity? 
1. What types of businesses are best suited for all-remote?
1. Why do employees apply to work in an all-remote culture?
1. What’s the most important communication channel or practice that you use internally to keep your remote team connected and engaged?
1. What advantages do remote organizations have when it comes to building and sustaining their company culture?
1. What are some things you’d encourage newly remote employees to do?
    - Join a [WiFi Tribe](https://wifitribe.co/) digital nomad chapter?
    - Consider relocating (temporarily or permanently)?
    - Working in new time zones?
1. How do you weave in-person touch points or communal interactions into your organization? 

## Interview guests and answers

Below is an archive of past interview guests, answering the above questions and discussing remote work with GitLab team members.

### Pick Your Brain interviews

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/WBf_DA0FF9k" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

An archive of Pick Your Brain interviews are below. We've also created a [Pick Your Brain Playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthafBVmoPPVMvBc_Gg2nsyQb) on [GitLab's YouTube channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg).

Learn more about scheduling a GitLab [Pick Your Brain series](/handbook/eba/#pick-your-brain-meetings).

- [GitLab CEO Sid Sijbrandij and InVision Chief People Officer Mark Frein](/blog/2019/07/31/pyb-all-remote-mark-frein/)
- [GitLab CEO Sid Sijbrandij and FormAssembly CEO Cedric Savarese](/blog/2017/08/11/pick-your-brain-interview-cedric-savarese/)
- [GitLab CEO Sid Sijbrandij and Polymail Co-founder and CEO Brandon Foo](/blog/2017/06/02/pick-your-brain-interview-brandon-foo/)
- [GitLab CEO Sid Sijbrandij and Stitch Co-founder and CEO Jake Stein](/blog/2017/08/18/pick-your-brain-interview-jake-stein/)
- [GitLab CEO Sid Sijbrandij and SaaS.CEO Founder Vincent Jong](/blog/2018/01/26/pick-your-brain-interview-vincent-jong/)
- [GitLab CEO Sid Sijbrandij and Crazy Wisdom Podcast host Stewart Alsop III](https://www.youtube.com/watch?v=23XIx6n9SsQ)
- [GitLab CEO Sid Sijbrandij and Outklip Founder Sunil Kowlgi](/blog/2019/04/18/lessons-on-building-a-distributed-company/)
- [GitLab CEO Sid Sijbrandij and Zapier's Mike Knoop and Noah Manger](/blog/2018/01/08/zapier-pick-your-brain-interview/)
- [GitLab CEO Sid Sijbrandij and FineTune CTO Kwan Lee](/blog/2017/09/15/pick-your-brain-interview-kwan-lee/)
- [GitLab CEO Sid Sijbrandij and Buffer CEO Joel Gascoigne](/blog/2017/03/14/buffer-and-gitlab-ceos-talk-transparency/)
- [GitLab CEO Sid Sijbrandij and leadership psychologist Banu Hantal](/blog/2019/06/21/cofounder-relations/)
- [GitLab CEO Sid Sijbrandij and Slab co-founder Jason Chen](/blog/2016/07/14/building-an-open-source-company-interview-with-gitlabs-ceo/)

## GitLab All-Remote Snapshot interviews

GitLab's all-remote culture empowers team members to [live and work where they are most fulfilled](/company/culture/all-remote/people/), and to structure their day in a manner that suits them. 

If you have yet to work within an all-remote environment, it can be difficult to fully understand the potential. Each individual is [able to approach all-remote differently](/company/culture/all-remote/people/), leveraging the autonomy it enables to improve one's life. 

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/QTPeyRW766Q" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered video](https://youtu.be/QTPeyRW766Q) above, two GitLab team members share their experiences on working in an all-remote setting, providing a glimpse at what's possible when embracing this style of work.*

To provide insight and [transparency](/handbook/values/#transparency) into the lives of GitLab team members — and give those considering remote work [ideas for structuring their own lives](/company/culture/all-remote/resources/#blogs-from-gitlab-team-members) — we ask the below questions in a video interview. While GitLab's entire team is remote, there can be great diversity in answers. 

1. Who are you, how long have you been at GitLab, and what do you do at GitLab?
1. What attracted you to GitLab?
1. Was all-remote a big contributor to why you chose to work at GitLab?
1. What notable elements of an all-remote culture have impacted how you structure your day and life?
1. Would you walk us through a typical day in your life?
1. If you were to give advice to someone considering working for an all-remote company, what would you tell them?

Explore answers from GitLab team members in the [Remote Work playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc) at the GitLab Unfiltered YouTube channel. 

GitLab team members interested in being interviewed should message [@dmurph](https://gitlab.com/dmurph). 

----

Return to the main [all-remote page](/company/culture/all-remote/).
