---
layout: handbook-page-toc
title: "Considerations for a Productive Home Office or Remote Workspace"
canonical_path: "/company/culture/all-remote/workspace/"
description: Considerations for a Productive Home Office or Remote Workspace
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're detailing a variety of considerations and recommendations for constructing an ergonomic, productive, and fulfilling home office or remote workplace. 

## How do I design and build the optimal home office?

Particularly for those who are working remotely for the first time, the notion of [constructing a home office](/blog/2019/09/12/not-everyone-has-a-home-office/) can be a perplexing one. In colocated settings, environmental design professionals are retained to create the work setting. In a remote environment, that burden shifts to the employee. 

While there is tremendous [benefit](/company/culture/all-remote/benefits/) to ditching the cubicle, it's not uncommon for a remote worker to feel ill-equipped to design their own workspace. A bespoke work area is fundamentally different than any other area in the home. It's important to be intentional about the space where you do the bulk of your work, designing it to be comfortable and to create an atmopshere where you are productive and focused. 

It's important to view your home office as an evolving project. [Iteration](/handbook/values/#iteration) applies to workspaces, too. As you begin to interact with your office, you may determine that monitors should be shifted, lighting should be added or tweaked, or that you actually prefer silence or ambient noise over headphones playing music. 

For a glimpse at how one GitLab team member thought through his home workspace, check out Brandon L.'s blog post, *[How To Stay Productive In Your Home Office](/blog/2019/11/06/how-to-stay-productive-in-your-home-office/)*.

## Not sure what to buy?

Look at our [equipment examples page](/handbook/spending-company-money/equipment-examples/) to see some of the items that other GitLab team members have purchased, and please consider adding to the list if there is something you'd like to share.

## Chairs

Seating should be viewed not as an expense, but as an *investment* — in your health, comfort, and productivity. 

While each person's primary workspace will vary, it's important to place a high degree of consideration on your chair. Many remote workers spend a significant portion of their day seated. While [resources exist](https://www.steelcase.com/research/articles/topics/ergonomics/movement-in-the-workplace/) to explain how frequently you should take a break, stand up, stretch, and engage in activity during the workday, you should be careful not to skimp on seating. 

Unless you plan to utilize a standing desk, which is covered below, your chair is apt to be the single most important element of your home office. Not all ergonomic chairs are created equal, and a chair that works well for one person may not be ideal for another. It's important to [consider your posture](https://my.clevelandclinic.org/health/articles/4485-back-health-and-posture), and make adjustments to habits if needed, to make the most of an ergonomic chair. 

If at all possible, visit a physical store to try out a variety of ergonomic seating options, or purchase online from a retailer that offers a generous return policy. 

[Steelcase](https://www.steelcase.com/products/office-chairs/) and [Herman Miller](https://www.hermanmiller.com/products/seating/) both offer solid options. While retail pricing on chairs from these firms are lofty, they are commonly found on used marketplaces for less. To assist in your research, consider [Wirecutter's exhaustive (and continually updated) guide to office chairs](https://thewirecutter.com/reviews/best-office-chair/). 

For employers, consider offering an [allowance](/handbook/spending-company-money/) for employees to invest in a high-quality ergonomic chair.

## Desks

While some remote workers prefer to [bounce around](/blog/2019/09/23/how-to-push-code-from-a-hammock/) during the day, those looking to [invest in a desk](https://www.nationalbusinessfurniture.com/blog/complete-guide-to-office-desks) for a dedicated workspace should consider a few elements. 

It's wise to plan your desk purchase around your equipment list, rather than the other way around. If you plan to use one or more desktop monitors (which is recommended), measure the physical dimensions and ensure that your desk has room to hold them. Factor in other items such as external speakers, scanners, printers, microphone mounts, webcams, external webcam lighting, etc. 

Generally, a larger desk is preferred, as it reduces crowding. A clean, uncrowded desk is a happier place to work from. 

### Standing desks

Should your budget allow, [an adjustable standing desk](https://www.autonomous.ai) is a great, ergonomic option. This allows you to easily move your desk higher in order to stand while working, and then adjust it back down to sit for a time. This is a good article on [The Best Standing Desks](https://thewirecutter.com/reviews/best-standing-desk/) in higher price ranges.

Research is ongoing as to the [ideal ratio of sitting to standing](https://uwaterloo.ca/kinesiology/how-long-should-you-stand-rather-sit-your-work-station) during a workday. 

> Using advanced ergonomic and health risk calculations, [Jack Callaghan](https://uwaterloo.ca/kinesiology/people-profiles/jack-callaghan), a professor in Waterloo’s Department of Kinesiology, has found that the ideal sit-stand ratio lies somewhere between 1:1 and 1:3  – a vast departure from traditional wisdom. 

### Standing mats

If you plan to stand for an extended portion of your working day, consider investing in a [standing desk mat](https://thewirecutter.com/reviews/best-standing-desk-mat/). Also called anti-fatigue mats, these provide cushion for one's feet and enables a more natural shifting of weight while working. 

## Webcams

In an all-remote setting, [video calls](/handbook/communication/#video-calls) are vital to maintaining close relationships with clients, partners, and colleagues. While voice calls are flexible and allow for uniquely efficient lifestyles (e.g. listening to a conference call while running in a park), it's important to integrate video into [workplace communication](/company/culture/all-remote/meetings/). 

While most phones and laptops ship with passable webcams, they do not offer optimal quality. A dedicated webcam offers not only a higher resolution camera, but is able to handle low-light scenarios with greater poise. Many dedicated webcams also include a software suite for touching up one's appearance, tweaking white balance, and applying background themes when paired with a green screen.

Consider [selecting a webcam](https://thewirecutter.com/reviews/the-best-webcams/) with a versatile mount, enabling it to be set atop a desktop monitor as well as a laptop. For more, read [5 tips for mastering video calls](/blog/2019/08/05/tips-for-mastering-video-calls/) on the GitLab blog. 

## Headphones

Particularly in noisy environments, wearing headphones creates [a more positive video experience for all](/blog/2019/06/28/five-things-you-hear-from-gitlab-ceo/). Your choice of headphone will vary depending on your workspace. For example, if you're using a dedicated microphone, you may prefer comfortable studio-style headphones without an in-line mic. If you want to reduce the amount of hardware you're using, headphones with an in-line mic will be more appropriate. 

GitLab recommends trying out various headphone styles in advance if possible. Open-ear vs. closed-ear, for example, provides a very different listening experience. Noise-cancelling headphones are great for crowded coworking spaces, but may blot out too much sound for [stay-at-home parents](/company/culture/all-remote/people/#parents) who need to be aware of what's happening inside the home. 

Some prefer in-ear headphones rather than over-the-head headphones, and it's important to consider long-term comfortability for those who may find themselves in [video calls](/blog/2019/08/05/tips-for-mastering-video-calls/) for multiple hours per day. 

Bluetooth headphones can be problematic for roles requiring a significant amount of calls due to limitations of microphone quality, latency, and battery life. If you opt for Bluetooth headphones, it is recommended that you use a separate wired microphone. Too, be mindful of Bluetooth headphones being paired with multiple devices (e.g. one pair of headphones with a pairing history involving a laptop as well as a phone). Bluetooth headsets can easily jump between devices, thus it is recommended to only pair one set per device to avoid unexpected disconnects during video calls. 

What constitutes "good headphones" varies significantly depending on preference. We recommend digging into [Wirecutter's various headphone guides](https://thewirecutter.com/electronics/headphones/) for researched suggestions.

## Lighting

In colocated settings, the human eye is capable of focusing on conversation participants while deprioritizing suboptimal surroundings. When [communicating](/company/culture/all-remote/informal-communication/) via webcam, one needs to be more cognizant of the lighting around them.

[Meetings](/company/culture/all-remote/meetings/) are about the work, [not the background](/company/culture/all-remote/meetings/#meetings-are-about-the-work-not-the-background), but those who are [designing their home office](/blog/2019/09/12/not-everyone-has-a-home-office/) may want to consider lighting before too many absolutes are put in place. Here are a few lighting tips to be mindful of.

1. Avoid backlighting or sidelighting when possible (e.g. design your office so that outside light shines onto your face, not your back or side).
1. Consider smart bulbs (e.g. [Philips Hue](https://www2.meethue.com/en-us/bulbs)) to light your office, which can be tweaked to create a soft, pleasing light regardless of your wall color. 
1. While [enclosed rooms](/blog/2019/09/12/not-everyone-has-a-home-office/) are ideal for controlling light, they may feel inhibiting to work from.
1. Aim to avoid shadows and changing light conditions.

While studio lighting is ideal, not everyone will be inclined to install large, heat-generating light boxes in their home office. As remote work and livestreaming become more popular, companies are devising smaller solutions. Elgato's [Key Light](https://www.youtube.com/watch?v=d2qR-wMPoTE) is a great example. By placing one Key Light at the edge of a desk and facing its LEDs directly into the wall, a soft, refreshing light is [bounced back onto the participant](https://www.youtube.com/watch?v=RckLFNRKPfU).

[DIY solutions](https://www.diyphotography.net/look-good-webcam-vlog/) are relatively easy to create with a mount and a light ring.

## Microphones

Whenever possible, avoid using the inbuilt microphone of a laptop, phone, or desktop monitor when communicating. These microphones tend to be of low quality, and do little to stop background noise. 

At the very least, utilize a pair of Bluetooth or wired earbuds with an in-line microphone. These are commonly included with most smartphones. 

For those who spend significant time on video calls, consider a [dedicated USB microphone](https://thewirecutter.com/reviews/the-best-usb-microphone/) (and, if desired, a desk mount for added ergonomic positioning). For example, [Blue Microphones](https://www.bluedesigns.com/) offers a variety of options that are crafted with creators, streamers, and podcasters in mind, and all provide exceptional clarity and noise reduction on [video calls](/handbook/communication/#video-calls). 

Several GitLab team members have positive experiences with the [M-Audio UberMic](https://www.amazon.com/M-Audio-Uber-Mic-Professional-Microphone/dp/B0767N58ZY). 

## Monitors

If you feel that your digital workspace is too cramped when relying solely on a laptop, consider using at least one [external monitor](https://thewirecutter.com/reviews/best-monitors/). To boot, external monitors offer flexible positioning, which allows your neck to be situated in a more natural position. 

External monitors are especially useful to remote employees, as much of their day is made up of video calls. An external display enables one screen to be used for video chatting, while another screen is used for documentation, referencing pages, etc. 

If you feel overwhelmed by too much information spread across multiple monitors, consider [window management software solutions](https://thesweetsetup.com/window-management-macos-2018/) such as [Divvy](https://mizage.com/divvy/) and [Magnet](https://apps.apple.com/us/app/magnet/id441258766?ign-mpt=uo%3D4&mt=12). 

If you'd like a portable monitor and you primarily use a MacBook laptop, consider expanding your visual workspace with [Sidecar](https://www.apple.com/newsroom/2019/06/apple-previews-macos-catalina/) (available in macOS Catalina). This allows an iPad to double as an secondary display for your Mac.

## External keyboard and mouse

Laptop keyboards are engineered to *fit*, not to be ergonomic. Whenever possible, consider working in a space where there's room to utilize an external keyboard. This allows you to adjust the keyboard so that you're typing in a natural way, reducing strain on your wrist and fingers.

There are a wide variety of ergonomic keyboards, and it's worth trying out a few in a retail location if possible. If this is not practical, Wirecutter has assembled [a well-researched guide on the best ergonomic keyboards](https://thewirecutter.com/reviews/comfortable-ergo-keyboard/). 

Traditional mice can put strain on the wrist by creating movements in an unnatural position. Fortunately there are plenty of other options availabe such as vertical mice, trackpads, trackballs and pen tablets. Each has their own advantages. Find one that is comfortable for you and minimizes wrist movement as much as possible (vertical mice, for example, engage your shoulder and arm more than your wrist and trackballs rely on moving your fingers). You can also consider a left-handed mouse. Painless Movement has [a regularly updated list of the best ergonomic mice](https://www.painlessmovement.com/best-ergonomic-mouse-wrist-pain/) as well as comprehensive information on the various types towards the bottom of the page. It's important for remote workers to be cognizant of repetitive motion. While it may seem extreme to some, consider installing two mice and switching hands to move one's cursor. 

## Ergonomic considerations

In colocated settings, it is often possible to work with someone trained in environmental design or ergonomics in order to create a comfortable workspace. For remote employees, you can consider hiring a trained ergonomic consultant to provide professional input and recommendations on how your home office should be constructed. 

## Finding the ideal temperature

Research has [found](https://www.bbc.com/worklife/article/20160617-the-never-ending-battle-over-the-best-office-temperature) that there is no single temperature in which all people are more productive. One  issue with colocation is the divide between those who believe an office is too warm, and those who feel that it's too cold. Those new to remote work should consider testing several temperatures to see which suits them, or adjust temperature based on your work activity. 

## Busy/available indicators

While remote workers should relish the benefits of being close to friends and family while working, some may prefer a more formal approach to signaling their availability. 

For example, the [Luxafor Flag](https://luxafor.com/flag-usb-busylight-availability-indicator/) and [Luxafor Switch](https://luxafor.com/luxafor-switch-meeting-room-availability-indicator-light/) light indicators utilize a color system to alert those around you (or outside of your home office) whether or not they are free to interrupt without requesting permission. 

Alternatively, those with a nearby light fixture could install a color-changing smart bulb, utilizing their phone or computer to change the color to indicate availability. 

## GitLab Knowledge Assessment: Considerations for a productive home office or remote workspace

Anyone can test their knowledge on considerations for a productive home office or remote workspace by completing the [knowledge assessment](https://docs.google.com/forms/d/e/1FAIpQLSeuswfs_t1Dl2vbmapTLNRHa2rfi5zwUg2IQ7ZQI7hxc1gB5g/viewform). Earn at least an 80% or higher on the assessment to receive a passing score. Once the quiz has been passed, you will receive an email acknowledging the completion from GitLab. We are in the process of designing a GitLab Remote Certification and completion of the assessment will be one requirement in obtaining the [certification](/handbook/people-group/learning-and-development/certifications/). If you have questions, please reach out to our [Learning & Development team](/handbook/people-group/learning-and-development/) at `learning@gitlab.com`.

## <%= partial("company/culture/all-remote/is_this_advice_any_good_remote.erb") %>

Just as it is valid to [ask if GitLab's product is any good](/is-it-any-good/), we want to be transparent about our expertise in the field of remote work. 

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
