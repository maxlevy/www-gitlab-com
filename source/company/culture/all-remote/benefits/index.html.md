---
layout: handbook-page-toc
title: "All-Remote Benefits"
canonical_path: "/company/culture/all-remote/benefits/"
description: Benefits of operating a remote company
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

On this page, we're highlighting benefits of operating an all-remote company for employers, employees, and the world.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Xw-31PZkHOo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

In the [video](https://www.youtube.com/watch?v=Xw-31PZkHOo) above, GitLab Director of Global People Operations [Carol Teskey](https://gitlab.com/cteskey) shares her view on the many benefits of all-remote, and the competitive advantages that come along with it.

## All-Remote Advantages

Reimagining how one's day can be structured, and how that can easily vary from one 24-hour period to the next, is empowering. The freedom and flexibility that comes with all-remote enables employees to view work in an entirely new light.

Rather than forcing one to build their life about a predefined daily schedule that involves an unavoidable commute, all-remote shifts that responsibility back to the individual.

A number of studies from the likes of [Google](https://www.forbes.com/sites/abdullahimuhammed/2019/05/18/5-important-takeaways-from-googles-two-year-study-of-remote-work/#1a536957439a), [Buffer](https://buffer.com/state-of-remote-work-2019), [FlexJobs](https://www.techrepublic.com/article/why-remote-work-has-grown-by-159-since-2005/), and [IWG](http://assets.regus.com/pdfs/iwg-workplace-survey/iwg-workplace-survey-2019.pdf) show that driven individuals who place a high degree of value on autonomy and flexibility can experience new levels of joy and productivity in an all-remote environment.

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/tTQAU78QSt8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, two GitLab colleagues discuss the benefits of all-remote. For those with friends and family in various locales, team members are empowered to spend time with loved ones, investing in those relationships, without the dread of counting vacation days.*

> An added benefit that I didn't anticipate, but have taken full advantage of, is being able to visit friends and family that are in locales that aren't necessarily close to where my home base is. Otherwise, I'd have to take paid time off (PTO) just to visit them.
>
> I've seen friends and family in Nashville, Raleigh, Iowa, New York, Boston, and all over the west coast. It's not just visiting for a weekend. It's being there for friends, family, birthdays, baby showers, and weddings. Across the board it has been a huge benefit [of all-remote at GitLab]. - *Jackie G., Manager, Marketing Programs at GitLab*

### For employees

It’s important for employees to truly take advantage of the freedoms and possibilities enabled by remote, rather than simply remaining where they are and switching an office desk for a room in their home. This is the core benefit of **remote** as compared to **work-from-home**.

Consider relocating to a lower cost-of-living environment, and look to integrate work into life rather than vice-versa. Remote work enables you to optimize for things like superior air quality and medical facilities, a healthier community environment, and better schools for your children. All of this requires premeditated action — to embrace the reality that work is now decoupled from geography. 

In the home, aim to create a dedicated working space (or work from a coworking space, external office, or cafe). It’s vital to intentionally separate work and life to prevent [burnout](/company/culture/all-remote/mental-health/). Moreover, don’t stop engaging with people. You may not fill your social quota entirely from work interactions, which enables you to pay closer attention to family, neighbors, and community opportunities. 

Be intentional about replacing your recovered commute time. Whether it’s fitness, cooking, cleaning, spending time with family, or resting, be deliberate about reclaiming that time and using it to improve your overall wellbeing. 

1. You have more [flexibility](http://shedoesdatathings.com/post/1-year-at-gitlab/) in your [daily life](/company/culture/all-remote/people/#those-who-value-flexibility-and-autonomy) (for [kids](/blog/2019/07/25/balancing-career-and-baby/), parents, friends, groceries, sports, deliveries).
1. No more time, [stress](https://www.forbes.com/sites/markeghrari/2016/01/21/a-long-commute-could-be-the-last-thing-your-marriage-needs/#5baf10f04245), or money wasted on a [commute](https://www.inc.com/business-insider/study-reveals-commute-time-impacts-job-satisfaction.html) (subway and bus fees, gas, car maintenance, tolls, etc.).
1. It's safer. With no commute, there's no risk of getting into an accident traveling to and from work.
1. You can optimize your life for superior air quality and medical facilities, a healthier community environment, and better schools for your children, rather than access to onsite jobs.
1. Reduced [interruption stress](/blog/2018/05/17/eliminating-distractions-and-getting-things-done/) and increased [productivity](https://www.inc.com/brian-de-haaff/3-ways-remote-workers-outperform-office-workers.html).
1. Ability to [travel to other places](/blog/2017/01/31/around-the-world-in-6-releases/) without taking vacation (family, fun, etc.).
1. Freedom to relocate, be [location independent](/blog/2019/06/25/how-remote-work-at-gitlab-enables-location-independence/), or even [travel with other remote professionals](/company/culture/all-remote/resources/#organizations-for-traveling-remote-work).
1. Less exposure to germs from sick coworkers.
1. It can be [easier to communicate](/company/culture/all-remote/informal-communication/) with difficult colleagues remotely, [reducing distractions](/blog/2018/03/15/working-at-gitlab-affects-my-life/) from interpersonal drama or office politics.
1. You can [set up and decorate your office or workspace](https://thriveglobal.com/stories/how-remote-work-can-reduce-stress-and-revitalize-your-mindset/) in whatever way [works best for you](/blog/2019/08/01/working-remotely-with-children-at-home/).
1. You can [choose your working hours](/company/culture/all-remote/people/#worklife-harmony) based on when you're [most productive](/company/culture/all-remote/asynchronous/).
1. You have the opportunity to [meet and work with people from many locations around the world](/handbook/incentives/#visiting-grant), widening one's view of the world and creating opportunities to learn about new cultures.
1. [Onboarding may be less stressful socially](/company/culture/all-remote/learning-and-development/#how-do-you-onboard-new-team-members).
1. Eating at home is better (sometimes) and cheaper.
1. Taxes can be cheaper in some countries.
1. Work clothes [are not required](/blog/2019/07/09/tips-for-working-from-home-remote-work/).

From family time to travel plans, there are [many examples and stories](/company/culture/all-remote/stories/) of how remote work has impacted the lives of GitLab team members around the world.

> **“The flexibility makes family life exponentially easier, which reduces stress and makes you more productive and motivated. You can’t put a dollar value on it – it’s priceless.”** - Haydn, Regional Sales Director, GitLab  

### For your organization

Remote enables a more diverse and inclusive workforce, greater efficiency in workflows, and broader global coverage in servicing clients. It significantly de-risks a business, making it more resilient in the face of crises and able to maintain continuity regardless of whether an office is open or closed. 

Companies may need to create a [remote leadership team](/company/culture/all-remote/remote-work-emergency-plan/) and invest in manager training. However, there is little debate on whether this is an option. Remote work will soon simply be "work," with global flexibility an expectation. Companies which have multiple offices already have teams which are remote to one another, and thus need a solid baseline of remote fluency to operate in optimal fashion. 

Limiting your company's recruiting pipeline to a certain geographic region, or sourcing employees who are able and willing to relocate, is a competitive *dis*advantage.

Not only does this create a less [inclusive](/company/culture/inclusion/) hiring process which reaches a less diverse set of candidates, it forces your organization to compete primarily on the basis of [salary](https://hired.com/page/state-of-salaries).

1. You're able to hire great people [no matter where they live](/jobs/faq/#country-hiring-guidelines).
   * GitLab has a truly global footprint, with team members in [65+ countries](/company/team/).
1. Employees are [more productive with fewer distractions](/blog/2018/05/11/day-in-life-of-remote-sdr/), so long as leadership is supportive in equipping teams with the requisite tools, structure, and culture to thrive.
   * For example, GitLab has a KPI of [65 tickets closed per support staff per month](/handbook/support/performance-indicators/#average-daily-tickets-closed-per-support-team-member).
1. [Increased savings on office costs](https://globalworkplaceanalytics.com/the-remote-work-roi-calculator-v0-95) and [compensation](/blog/2019/02/28/why-we-pay-local-rates/) (due to hiring in lower-cost regions).
   * GitLab **maintains a more balanced payroll** than other San Francisco Bay Area colocated companies. Learn more in the [Compensation Calculator](/handbook/total-rewards/compensation/compensation-calculator) section of GitLab's handbook.
   * Each function has a new hire location factor target. As an example, [marketing](/handbook/business-ops/data-team/kpi-index/#marketing-kpis) targets < 0.72 and [finance](/handbook/business-ops/data-team/kpi-index/#finance-team-kpis) targets < 0.69.  (For reference, San Francisco is 1.00)
   * We have a [people group KPI](/handbook/business-ops/data-team/kpi-index/#people-group-kpis) for our average overall location factor to be < 0.65.
1. All-remote naturally attracts [self-motivated people](/blog/2019/03/28/what-its-like-to-interview-at-gitlab/).
1. It's easier to quickly grow and scale your company.
   * GitLab **does not own/lease office space**. As colocated companies scale in size, their spend related to real estate will swell. This not only encompasses office space, but related activities such as security, cleaning, remodeling, etc. It also hampers [growth](/company/culture/all-remote/scaling/). Hiring too many people in too short a time span will force you to move offices in quick succession, creating massive cost and inefficiency.
1. Employees are [increasingly](https://www.iofficecorp.com/blog/workplace-design-statistics) expecting remote work options from their employers.
   * GitLab sees **over 3,000 applicants per week** for open [vacancies](/jobs/). Each applicant is given access to our [strategy](/company/strategy/) and an overview of [what it's like to work at GitLab](/jobs/faq/#whats-it-like-to-work-at-gitlab) *before* they are asked to invest time and energy in applying. We get more qualified applicants due to our [transparency](/handbook/values/#transparency), showcased through examples like [role description videos](https://youtu.be/BOeXgGu1Vco) hosted on the [GitLab Unfiltered YouTube channel](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A).
1. Companies often experience [lower employee turnover](https://www.owllabs.com/blog/remote-work-statistics) and higher morale with remote work.
   * GitLab enjoys an **[85% year-over-year voluntary retention rate](/handbook/people-group/people-group-metrics/#team-member-retention)**.  All-remote contributes to our high retention of people, as they're able to [take their career with them](/company/culture/all-remote/people/) through life's changes.
1. You have [fewer meetings](/company/culture/all-remote/meetings/) and more focus on results and [output of great work](/handbook/values/#results).
   * Each meeting which is avoided by [asynchronous communication](/handbook/communication/) and intentional [documentation](/company/culture/all-remote/management/#scaling-by-documenting) saves thousands of dollars in wages. Too, the time savings can be repurposed for generating additional value and empowering people to live [richer lives](/company/culture/all-remote/people/) as they work more [efficiently](/handbook/values/#efficiency).
1. You don't have to pay to relocate someone to join your team.
   * GitLab's recruiting department tracks [cost per hire](/handbook/hiring/metrics/#cost-per-hire), which currently does not have a target.
1. With employees located all over the world [working asynchronously](/blog/2019/02/27/remote-enables-innovation/), contributions can continue even when one time zone's working day is over.
   * GitLab has a people group KPI stating that the [percent of sent Slack messages that are not DMs be > 50%](/handbook/business-ops/data-team/kpi-index/#people-group-kpis).
1. There's also business continuity in the case of local disturbances or natural disasters (e.g. political or weather-related events).
1. Greater flexibility [can mean greater diversity](https://business.linkedin.com/content/dam/me/business/en-us/talent-solutions/resources/pdfs/global-talent-trends-2019.pdf) in your organization.

### For the world

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/BC7Prkm3v3g" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

*In the [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) video above, two GitLab colleagues discuss the benefits of living in a lower cost-of-living environment near friends, family, and community.*

[Research from the University of New Hampshire](https://carsey.unh.edu/publication/rural-depopulation) has found that "35% of rural counties in the United States are experiencing protracted and significant population loss." Speaking to shrinking towns across Europe, [a 2016 report from the European Parliamentary Research Service](http://www.europarl.europa.eu/thinktank/en/document.html?reference=EPRS_BRI(2016)586632) notes that "younger members of society prefer to migrate to more economically vibrant regions and cities in search of better job prospects as, in most of these territories, professional opportunities remain limited and confined to specific fields (e.g. agriculture and tourism)."

We believe all-remote has the power to pause, and perhaps even reverse, these trends of depopulation.

Working remotely gives each person the autonomy to serve in a place that matters to them – a place that has shaped them – contributing significantly to the wellbeing of a population that may be at risk of losing its foundation, should talent continue to flee to the usual job centers.

1. There's evidence that [remote work can reduce the effects of urban crowding](https://qz.com/work/1641664/remote-workers-are-the-solution-to-urban-crowding/) for many cities around the world. Some states and countries are even [offering incentives](http://fortune.com/2019/06/22/google-housing-plan-bay-area/) to encourage remote work. Here are [13 examples](https://www.bankrate.com/personal-finance/smart-money/places-that-will-pay-you-to-move/#slide=1) around the world.
1. For global companies, bringing better-paying jobs to low-cost regions has positive economic impacts and works to reverse the [trends](http://www.europarl.europa.eu/thinktank/en/document.html?reference=EPRS_BRI(2016)586632) of [depopulation in rural regions](https://carsey.unh.edu/publication/rural-depopulation).

## Contribute your lessons

GitLab believes that all-remote is the [future of work](/company/culture/all-remote/vision/), and remote companies have a shared responsibility to show the way for other organizations who are embracing it. If you or your company has an experience that would benefit the greater world, consider creating a [merge request](https://docs.gitlab.com/ee/user/project/merge_requests/) and adding a contribution to this page.

----

Return to the main [all-remote page](/company/culture/all-remote/).
