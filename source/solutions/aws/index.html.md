---
layout: solutions
title: GitLab on AWS
description: "GitLab on AWS."
canonical_path: "/solutions/aws/"
suppress_header: true
---

***
{:.header-start}

![AWS logo](/images/logos/aws-partner-devops.svg)
{:.header-right}

# GitLab on AWS
{:.header-left}

A complete DevOps platform to build, test, secure and deploy on AWS
{:.header-left}

[Talk to an expert](/sales/){: .btn .cta-btn .orange}
{:.header-left}

***
{:.header-end}

> As organizations continue their journey around digital transformation, DevOps has become the go-to set of best practices to increase their velocity of delivering value to the business while increasing quality. Breaking down the walls and reducing the handoffs necessary to complete a full delivery lifecycle is also very important and remains a challenge to most organizations.

> GitLab is a complete DevOps platform, delivered as a single application with bring-your-own-infrastructure flexibility. You can deploy anywhere from the cloud to on-premise using GitLab. By running GitLab on AWS you get a complete DevOps platform running and deploying to AWS cloud infrastructure.

***

## Joint AWS and GitLab Benefits

GitLab collapses cycle times by driving higher efficiency across all stages of the software development lifecycle running on AWS.

* Projects delivered on-time and budget
    * Eliminate bottlenecks for agility, faster DevOps lifecycle, reduce re-work, reduce unpredictable spend.
* Increase team productivity and velocity.
    * Attract, retain, and enable top talent, move engineers from integrations and maintenance to innovation, happy developers.
* Increase market share and revenue
    * Faster time to market, disrupt the competition; increased innovation; more expansive product roadmap.
* Increase customer satisfaction
    * Decrease security exposure, cleaner, and easier audits reduce disruptions.
{:.list-benefits}

[Learn More](https://about.gitlab.com/resources/downloads/GitLab_AWS_Solution_Brief.pdf){: .btn .cta-btn .purple}

***

## Develop Cloud-Native Applications Faster with GitLab and AWS

GitLab is available both as a Self-Managed package to install, configure, and administer on your infrastructure or as a SaaS offering that you can simply sign up and start using. GitLab Self-Managed runs great on everything from bare metal and VMs to AWS.

Check out Gitlab’s offerings on the

[![AWS Marketplace](/images/logos/aws-marketplace.svg)](https://aws.amazon.com/marketplace/seller-profile?id=9657c703-ca56-4b54-b029-9ded0fadd970){:.max-width-400}

Learn How to simplify Your Software Delivery Toolchain with GitLab and AWS Marketplace [**here**](https://about.gitlab.com/resources/downloads/esg_business_justification_brief_aws_marketplace_may2020.pdf)

## Joint Solution Capabilities with AWS

* ![AWS EC2 logo](/images/logos/aws-ec2.svg) Elastic Compute Cloud (EC2)
    * Amazon EC2 provides scalable computing capacity in the AWS cloud. GitLab allows users to scale jobs across multiple machines. When used together, GitLab on EC2 can significantly reduce users infrastructure costs. **[[Learn More]](https://docs.gitlab.com/ee/install/aws/)**
* ![AWS Elastic Kubernetes logo](/images/logos/aws-elastic-kubernetes-fill.svg) Elastic Kubernetes Services (EKS)
    * AWS Elastic Kubernetes Service (EKS) is a managed Kubernetes service. GitLab offers integrated cluster creation for EKS. If a user is an existing AWS user, EKS is the only Kubernetes service that lets the user take advantage of the tight integration with other AWS services and features. **[[Learn More]](https://about.gitlab.com/blog/2020/03/09/gitlab-eks-integration-how-to/)**
* ![AWS Fargate logo](/images/logos/aws-fargate-fill.svg) Fargate 
    * GitLab makes it easy to quickly deploy containers to Fargate. Fargate makes it easy to host and scale containers on AWS.  By using GitLab, with Fargate, users can deploy and scale container-based applications early, often and with ease. **[[Learn More]](/customers/trek10/)**
* ![AWS Lambda logo](/images/logos/aws-lambda-fill.svg) Lambda 
    * Lambda is a computing service that runs code in response to events and automatically manages the computing resources required by that code. GitLab allows users to run Lambda functions and create serverless applications. GitLab supports the development of Lambda functions by using a combination of the Serverless Framework and GitLab CI/CD. **[[Learn More]](https://docs.gitlab.com/ee/user/project/clusters/serverless/aws.html)**
* ![AWS SAM logo](/images/logos/aws-sam-squirrel-mascot.png) Serverless Application Model (SAM)
    * Amazon SAM is an open source framework for building serverless applications. AWS SAM provides a command line interface called AWS SAM CLI to make it easier to create and manage your serverless applications. GitLab is a complete DevOps platform that allows developers to deploy serverless applications built using the AWS SAM CLI. **[[Learn More]](https://about.gitlab.com/blog/2019/02/04/multi-account-aws-sam-deployments-with-gitlab-ci/)**
{:.list-capabilities}

***

## GitLab and AWS Customer Success Stories
{:.no-color}

* [Axway aims for elite DevOps status with GitLab](https://about.gitlab.com/customers/axway-devops/)
* [BI Worldwide Corporate Product Development Team removed technology barriers to focus on building microservices](https://about.gitlab.com/customers/bi_worldwide/)
* [Focused on customizing AWS enterprise-ready solutions, Trek10 uses GitLab to foster stronger client relationships](https://about.gitlab.com/customers/trek10/)
{:.list-resources}

***

## GitLab and AWS Joint Solutions in Action
{:.no-color}

* [WEBCAST] - [Deploy AWS Lambda applications with ease](https://about.gitlab.com/webcast/aws-gitlab-serverless/)
* [INSTAGRAPHIC] - [Simplifying Your Software Delivery Toolchain with GitLab and AWS Marketplace](https://about.gitlab.com/resources/downloads/esg_instagraphic-aws_marketplace_may_2020_.pdf)
* [WHITEPAPER] - [How to deploy on AWS from GitLab](https://about.gitlab.com/resources/whitepaper-deploy-aws-gitlab/)
* [INFOGRAPHIC] - [How GitLab accelerates workload deployments on AWS](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/uploads/59d1390557bb304e7401443c4e710c0f/gitlab-aws-ci_t-campaign-infographic-01.pdf)
* [Deploy your software to AWS you get the best DevOps tooling running on the best cloud infrastructure](https://about.gitlab.com/resources/downloads/GitLab_AWS_Solution_Brief.pdf)
* [Northwestern Mutual deployed GitLab Runners in AWS to manage demand for capacity and cost](https://www.youtube.com/watch?v=K6OS8WodRBQ)
* [WagLabs is achieving the impossible by using self-hosted GitLab on AWS to power its CI/CD](https://www.youtube.com/watch?v=HfEl9GXZC0s)
* [WEBCAST] - [Ask Media Group made the shift from on-prem data centers to the AWS cloud by leveraging GitLab Runners](https://about.gitlab.com/webcast/cloud-native-transformation/)
{:.list-resources}

***
