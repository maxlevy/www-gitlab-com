---
layout: markdown_page
title: "Category Direction - Cluster Cost Optimization"
---

- TOC
{:toc}

## Introduction and how you can help
Thanks for visiting this category page on Cluster Cost Optimization in GitLab. 
This vision is a work in progress and everyone can contribute. Sharing your feedback directly on [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Cluster%20Cost%20Optimization) and our public [epic](https://gitlab.com/groups/gitlab-org/-/epics/503) at GitLab.com is the best way to contribute to our vision. 
If you’re a GitLab user and have direct knowledge of your need for cluster cost optimization, we’d especially love to hear from you.

## Cluster Cost Optimization

Cloud cost management and optimization (CCMO) is an important tool in any CIO's toolkit for the following:

* Visibility & control - Identify and control which service or team is consuming the most resources for cost accounting and spend accountability
* Analysis - Enable decisions and teams to make the best decisions
* Reduce spend - Optimize spending in the cloud

While these considerations also apply for organizations operating Kubernetes, Cluster cost optimization are a bit different from traditional CCMO tools. Kubernetes is by default, a multi-cloud enabler and enables auto-scaling out-of-the-box, both of which are value propositions for traditional CCMO vendors that is less applicable for companies running Kubernetes.

GitLab is well positioned to provide a comprehensive solution for cluster cost optimization because GitLab's integration with the project or group's Kubernetes cluster.

Potential iterations to a viable solution are listed below:

- Flag deployments/projects that [overprovisioned resource](https://gitlab.com/gitlab-org/gitlab-ee/issues/9049) requests, wasting resources
- Recommend node changes to increase efficiency (use larger nodes, nodes with more RAM, etc.)
- Estimate costs utilized by each project
- Automatically implement the changes to right-size pod resource requests

## What's next & why

We are looking at [kubecost.com](https://www.kubecost.com) and determining how we might integrate. Kubecost can be a quick way to get this category to minimal maturity.

Otherwise, we are not actively prioritizing development work in this category. We would welcome your contribution in this space. 

## Competitive landscape
There's a long tail of competitors and solutions for CIOs in this space with no clear winner. Competition have varying perspectives from different vantage points on how to optimize spend. [kubecost.com](https://www.kubecost.com) is the solution most in line with GitLab's desire to address cluster cost optimization.

The changing landscape has led to some recent consolidation in the Cloud cost management and optimization (CCMO) space with the acquisition of [CloudHealth by VMWare](https://cloud.vmware.com/community/2018/10/04/cloud-health-acquisition/) and [Cloudability by Apptio](https://www.apptio.com/company/news/press-releases/apptio-inc-completes-acquisition-of-cloudability/).
The aquisition enabled VMWare to have a more complete multi-cloud offering, while Apptio now has a more complete on-premise+cloud offering, enabling them to target enterprises going through cloud migration with one integrated solution for optimizing spend.

* [kubecost.com](https://www.kubecost.com)
* [cloudhealthtech.com/blog/kubernetes-cost-allocation-made-easy](https://www.cloudhealthtech.com/blog/kubernetes-cost-allocation-made-easy)
* [replex.io/blog/kubernetes-cost-allocation-in-a-nutshell](https://www.replex.io/blog/kubernetes-cost-allocation-in-a-nutshell)
* [supergiant.io/blog/supergiant-packing-algorithm-unique-save-money](https://supergiant.io/blog/supergiant-packing-algorithm-unique-save-money)
* [mist.io](https://mist.io/)
* [cloudability.com](https://www.cloudability.com/)
* [Flexera](https://www.flexera.com/)
* [Turbonomic](https://www.turbonomic.com/)

## Analyst landscape

TBD

## Top Customer Success/Sales issue(s)

TBD

## Top user issue(s)

TBD

## Top internal customer issue(s)

TBD

## Top Vision Item(s)

[Flag over-provisioned kubernetes deployments](https://gitlab.com/gitlab-org/gitlab-ee/issues/9049)
