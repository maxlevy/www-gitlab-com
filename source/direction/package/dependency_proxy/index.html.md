---
layout: markdown_page
title: "Category Direction - Dependency Proxy"
---

- TOC
{:toc}

## Dependency Proxy

Many projects depend on a growing number of packages that must be fetched from external sources with each build. This slows down build times and introduces availability issues into the supply chain. ​​For organizations, this presents a critical problem. By providing a mechanism for storing and accessing external packages, we enable faster and more reliable builds.

Our vision for the Dependency Proxy is to provide a product that will provide fast, reliable access to all of your dependencies, whether they are hosted on GitLab or any other vendor. In addition, the Dependency Proxy will work hand-in-hand with the planned [Dependency Firewall](/direction/package/dependency_firewall/), which will help to prevent any unknown or unverified providers from introducing potential security vulnerabilities.  ```

- [Maturity Plan](#maturity-plan)
- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=Category%3ADependency+Proxy)
- [Overall Vision](/direction/ops/#package)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

This page is maintained by the Product Manager for Package, Tim Rizzi ([E-mail](mailto:trizzi@gitlab.com))

## Usecases listed

1. Provide a single method of reaching upstream package management utilities, in the event they are not otherwise reachable. 
1. Cache images and packages for faster build times.
1. Track which dependencies are utilized by which projects when pulled through the proxy.
1. Audit logs in order to find out exactly what happened and with what code.
1. Operate when fully cut off from the internet with local dependencies.

## What's next & why

Next, we have [gitlab-#208080](https://gitlab.com/gitlab-org/gitlab/-/issues/208080) which will resolve a bug in which images are not being successfully pulled from the cache. 

Then, [gitlab-#11582](https://gitlab.com/gitlab-org/gitlab/-/issues/11582) will enable the Dependency Proxy for private projects. 

## Maturity Plan

This category is currently at the "Viable" maturity level, and
our next maturity target is Complete (see our [definitions of maturity levels](/direction/maturity/)).

For a list of key deliverables and expected outcomes, check out the epic, [gitlab-#2920](https://gitlab.com/groups/gitlab-org/-/epics/2920), which includes links and expected timing for each issue.

## Competitive landscape

* [Artifactory](https://www.jfrog.com/confluence/display/RTF/Docker+Registry#DockerRegistry-RemoteDockerRepositories)
* [Nexus](https://help.sonatype.com/repomanager3/configuration/repository-management#RepositoryManagement-ProxyRepository)

Artifactory is the leader in this category. They offer 'remote repositories' which serve as a caching repository for various package manager integrations. Utilizing the command line, API or a user interface, a user may create policies and control caching and proxying behavior. A Docker image may be requested from a remote repository on demand and if no content is available it will be fetched and cached according to the user's policies. In addition, they offer support for many of major packaging formats in use today. For storage optimization, they offer check-sum based storage, deduplication, copying, moving and deletion of files.

​​However, since they have focused on solving all possible usecases, there is room for simplification and design improvements. We believe this will allow GitLab to provide a more accessible and easier-to-navigate solution. In addition, we provide added value by combining this with our own CI/CD services, improving speed and having everything on-premise.
​​
## Top Customer Success/Sales issue(s)

The top customer success issue is [gitlab-#11582](https://gitlab.com/gitlab-org/gitlab/issues/11582), which will introduce authentication and allow users to leverage the Dependency Proxy with private projects.

## Top internal customer issue(s)

The top internal customer issue is [gitlab-#496](https://gitlab.com/gitlab-org/distribution/team-tasks/-/issues/496) which will update Gitlab.com to use the Dependency Proxy for caching frequently used images from DockerHub.

## Top Vision Item(s)

Our top vision item is the epic [gitlab-#2614](https://gitlab.com/groups/gitlab-org/-/epics/2614), which will expand the Dependency Proxy to work with the GitLab Package Registry. Once implemented, Admin will be able to create remote and virtual registries, so that they can resolve all of their Group's dependencies with a single, logical URL. 

Our plan is to start with NPM, then move on to Maven, NuGet, and PyPI. 
