---
layout: markdown_page
title: "Category Direction - Design Management"
---

- TOC
{:toc}

## Design Management

| | |
| --- | --- |
| Stage | [Create](/direction/create/) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2020-06-30` |

## Introduction

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas,
and user journeys into this section. -->

Design Management at GitLab encompasses two broad areas of features: **1) Transient "Communication-based" features** which have a beginning and end similar to Issues and **2) Evergreen "Project-based"** features which live as a project or asset on their own. GitLab is in a unique position to offer both types of Design features because our platform supports Issues (collaboration with engineering) and Repositories (where the actual code lives).

**Examples of Transient "Communication-based" Design Management features:**
- Attaching Designs to Issues
- Visual communication in comments on designs
- Approvals of Designs
- Links to prototypes in description of issue
- Developer handoff with specifications
- Production Design Inspect to compare the code to the design
- Issues are closed and Designs are "done"

**Examples of Evergreen "Project-based" Design Management features**
- Customized Design repositories for backing up native design files
- Customized Design repositories for connecting local design system files to their live frontend assets
- Deep linking of assets to original repo files


**Our goal with Design Management is to treat Designers as first-class users within GitLab and support their workflows as good as any product can. Design management will consider the design life cycle from generating ideas, design reviews, design systems, and more.**

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADesign%20Management)
- [Epic List](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3ADesign%20Management)
- [Overall Vision](/direction/create/)

Please reach out to PM Christen Dybenko ([E-Mail](mailto:cdybenko@gitlab.com)) if you'd like to provide feedback or ask
questions about what's coming.

## Long Term Strategy

Long term, we'll need we need to adapt GitLab to incorporate Design users as first class users as this design workflows are often different from the typical DevOps flow. Many companies that employ "Design First" workflows are often working so far ahead of the DevOps process we need to consider this as its own ecosystem and key in on. We recognize that this workflow can be quite iterative and often done and presented/approved ahead well ahead of an issue ever being written.

Ideally, GitLab will support the flexiblity of any design workflow, while featuring a simplified way to share of Design work. It would allow for independent design deadlines or separate milestones from engineering, and empower the Designer to easily track/follow the issue through to production.

### Typical designer workflow across tools

![Typical designer workflow across tools](images/Typical-designer-workflow.png)

### GitLab vision for a new designer workflow

![Typical designer workflow across tools](images/GitLab-designer-workflow.png)


## Target Audience and Experience
<!-- An overview of the personas involved in this category. An overview
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

**In 13.0, we  [moved Design Managment to Core/Foss](https://gitlab.com/gitlab-org/gitlab/-/issues/212566) so it can but used by everyone for free.**  Read more about in the blog: https://about.gitlab.com/blog/2020/03/30/new-features-to-core/

Design Management is targeted at [product designers](/handbook/marketing/product-marketing/roles-personas/#presley-product-designer) who collaborate with [product managers](/handbook/marketing/product-marketing/roles-personas/#parker-product-manager) and [software developers](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer).

The **minimal** user journey will provide designers with the ability to upload mockups to an issue, and for point of interest discussions to happen on each image. Over time these mockups can be updated to resolve the discussions. As the mockups are changed, new versions will be created so that process can iterate.

## What's Next & Why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](/handbook/product/product-management/process/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

**MVC Done in 12.9: [Versioned designs and point of interest discussions](https://gitlab.com/groups/gitlab-org/-/epics/660)** - Currently, collaboration between designers, developers and product managers on issues is hard and unstructured. This first iteration will make it possible to upload discuss designs far more efficiently. Point of interest comments are the key feature, that allows much precise discussion rather than trying to verbally discuss multiple items of feedback on a single image in a single thread. We plan to [move this feature down to Core](https://gitlab.com/gitlab-org/gitlab/-/issues/212566) in the next few releases.

**Next: Improve the useability of the MVC for Versioned designs and point of interest discussions by Dogfooding** - We're working on an [Internal dogfooding epic](https://gitlab.com/groups/gitlab-org/-/epics/2582) which contains the issues that gets us through the major pain points that our internal GitLab team has surfaced. This will take us a long way towards viable as these are the biggest useability concerns.

**Next: [Integrate with major design tools to support current workflows and increase users](https://gitlab.com/groups/gitlab-org/-/epics/2405)** - We started official [Figma](https://gitlab.com/gitlab-org/gitlab-figma-plugin) and [Sketch](https://gitlab.com/gitlab-org/gitlab-sketch-plugin) projects to work on our plgugins. The Figma MVC will ship in 13.2 - track progress in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/3449). Next: Collaborate with the other design tool companies as partners and build out API support so Product Designers can post to GitLab Issues from their preferred workflow. While working on this, upgrade our Issue Descriptions to support unfurled links (with thumbnail preview) from our own Designs as well as the partner design tools. Move engineering heavy lifting to the partner integration teams so we can power through.

**Next: [Empower Product Designers and Leaders to easily stay on top of design progress at GitLab](https://gitlab.com/gitlab-org/gitlab/issues/196240)** - Move "Designs" to its own space on the left side menu above Issues. Here we'll see all design updates in real time and map design images to issues in a more seamless and connected flow. Also to consider here would be to allow designs posted without an issue to encourage rapid feedback and turn around on ideas.

**Next: [Developer Handoff in Review Apps](https://gitlab.com/groups/gitlab-org/-/epics/2406)** - Upgrade the existing Visual Review feature to have original Epic or Issue Designs move forward in the GitLab workflow for visual inspection in the merge request. The Product Designer would be notified and invited to also inspect which would cut down on a lot of back and forth communication that currently happens offline. Empower the Engineer and the Product Designer to compare the original design with live app. Allow them to give visual feedback by attaching comments to dom elements or on an x,y access. For example: Move a transparent overlay of the original design around to ensure check measurements and other visual aspects.

**Later: [Design reviews and approvals](https://gitlab.com/groups/gitlab-org/-/epics/990)** - Once we have more usage of our designs throughout the DevOps flow, add approvals or other checks and balances that may be needed to lock down a design. The first step is to understand how current review and approval processes work outside of GitLab and how we could adapt those to GitLab.

**Later: [Design Management prototyping support](https://gitlab.com/groups/gitlab-org/-/epics/1728)** - currently Design Management only supports static images which are a valuable part of the design process, but may not be able to fully communicate the user experience. Part of enabling communication around that process is to allowing users to create simple click-through prototypes for designs.

**Later: [Adding drawing and diagram files to the Design Tab](https://gitlab.com/gitlab-org/gitlab/issues/199694)** - Allow the creation of a diagram or drawing directly inside the browser via an integration. We'll investigate the right ways to "open up the drawing board" to anyone working on an Issue.

**Later: [Git backup of design files](https://gitlab.com/gitlab-org/gitlab/issues/200088)** - When teams share source files such as the Pajamas Sketch file there is a lot of overhead with checkign that file in and out of a repo and having a design team share the file. We also would consider LFS (Large file system) and design agencies who may have huge repositories and would only be able to check out just a part of the entire repo for use.

## Future considerations

**Live Sketching in Issues** Allow users to draw freehand on a digital whiteboard and see the real time cursor and sketching of other users who are live with them. We think this will enable more "off-the-cuff" brainstorming and foster an in-person experience with colleagues, even if working remote.

**Later: Deep linking of assets** - Imagine seeing an image asset in your repo or within a wiki and being able to click a button to pull down its original artwork from Sketch/Photoshop or beyond. If we achieve git backup of design files, we should be able to create deep links to their source artboards.

**Future: AI to compare Designs to Production**  With the emergence of AI technologies, eventually Design AI could be a part of the CI pipeline to scan for visual changes against the original design, report differences, and recommend changes.



## Competitive Landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](//handbook/product/product-management/process/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Within the Design Tools market, each product broadly solves one or more of these problems:

- Approvals and lock down of designs
- Generate developer specifications (Developer handoff)
- Version control
- Visual communication via comments
- UX Research and UI Testing
- Designing and prototyping
- Design System creation and management

The most full featured Design Tools, that are attempting to solve all these problems are:

- [Invision Studio](https://www.invisionapp.com/studio)
- [Figma](https://figma.com)
- [UX Pin](https://www.uxpin.com/)

What is missed in the current market:

- "Production Design Inspect": Visual inspection of frontend code against the original designs on the issue or original prototype with a focus on the fidelity review of the code a developer actually implemented vs. the design they meant to implement
- Tracking and conversion of local design elements to their live Design System counterpart in the repo

Given GitLab's unique strength as the single source of truth for planning and source code, we are well positioned to work with Product Design teams and encourage Product Designers to participate from design iteration through to production:

- Review and collaboration, bringing designers deeper into the tool that they already use to understand what needs to be designed and built, and integrating the design workflow into the planning and development workflow.
- Version control for source code is a core competency of GitLab, with great support for LFS and upcoming native support for large files in Git. Building on this to support versioning of designs and automation with CI is a natural fit.

## Market Research
<!-- This section should link or highlight any relevant market research you've done that justifies our
entry into the market for the particular category. -->

A [competitive analysis](https://docs.google.com/document/d/12o6h6Fm7bAjhW5AK1r-PNhvn0QrQwZncorYNia12e3Q/edit) was also conducted to further understand existing players in the market.

## Business Opportunity
<!-- This section should highlight the business opportunity highlighted by the particular category. -->

The total market potential of Design Tools is over US $4 billion and growing. There is a significant opportunity for an application that can successfully engage developers and design teams before and during the DevOps lifecycle. If GitLab integrates with the major design tools such as InVision (5,000,000 users), Sketch (1,000,000 users,) and Figma (1,000,000 users) and adds value in the form of visual developer handoffs, we believe product quality will increase.

## Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

In the past month, we've had briefings with Redmonk, Forrester and Gartner. We've validated that we are solving a big problem with our proposal to bring Designers into Devops. 

We learned that a new potential term for this type of software is: "Digital product design platforms" and we are also considering the term "DesignOps" which might play well with DevOps at GitLab.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

No customer issues yet, because the feature is so new

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

*  [Design Activity should be supported on Project, Group and User Activity Pages](https://gitlab.com/gitlab-org/gitlab/-/issues/33051) - 9
*  [Adoption: GitLab <> Figma plugin prototype MVC](https://gitlab.com/gitlab-org/gitlab/-/issues/207433) - 7
*  [Rethinking information architecture of Design Management](https://gitlab.com/gitlab-org/gitlab/-/issues/204743) - 5
*  [Design Comment: Edit Comment text](https://gitlab.com/gitlab-org/gitlab/-/issues/118609) - 5

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](/handbook/values/#dogfooding)
the product.-->

*  [Internal dogfooding epic](https://gitlab.com/groups/gitlab-org/-/epics/2582) - this gets us through the major pain points that our internal GitLab team has surfaced
*  [Versioned designs and point of interest discussions](https://gitlab.com/groups/gitlab-org/-/epics/660) - the MVC addresses the most frequent frustrations with the current workflow which involves uploading images in a markdown table.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

*  [Design process automation](https://gitlab.com/groups/gitlab-org/-/epics/991) - version control the source files in a git repo, and push to update the designs attached to issues.
*  [Design Management prototyping support](https://gitlab.com/groups/gitlab-org/-/epics/1728) - static designs don't fully communicate the user experience; adding support for prototypes can further expand collaboration
*  [Design reviews and approvals](https://gitlab.com/groups/gitlab-org/-/epics/990) - like merge requests, designs need reviews and approvals before being approved to proceed.
