---
layout: markdown_page
title: Product Stage Direction - Manage
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Stage Overview
 
The Manage stage in GitLab **delights business stakeholders and enables organizations to work more efficiently**. Managing a piece of software is more than maintaining infrastructure; tools like GitLab need to be adoptable by companies of all sizes and be easy to operate. Setting up your processes shouldn’t be a struggle, and administrators shouldn’t have to compromise on security or compliance to prevent tools from hindering their velocity.

* **Delighting the business**: to make GitLab adoptable by organizations of any size, it must excel at meeting table stakes that are set by the business. A skyscraper with many people in it can only be enabled by a solid, secure foundation - an application serving a similar scale isn't any different. GitLab needs to support the access control, onboarding, security, and auditing needs that enables enterprise-level scale. We also need to make the foundation easy to lay; constructing a building is slow and arduous when it's done brick-by-brick. Adopting GitLab should be fast and reliable and show a quick trail to getting an amazing return on your investment in GitLab.
 
* **Working more efficiently**: while we want to fulfill foundational needs, GitLab strives to give you the ability to work in new and powerful ways. We aspire to answer valuable questions for users and to automate away the mundane. It’s not enough to give instances the ability to meet their most basic needs; as a single application for the DevOps lifecycle, GitLab can exceed the standard and enable you to work in ways you previously couldn’t.

### Groups in Manage

Manage is composed of 4 distinct groups that support our mission of **delighting business stakeholders and enabling organizations to work more efficiently**:

* Access: leads on our foundation of configuration and access control that enables secure and enterprise-level scale.
* Import: leads on adoption, making it easy for organizations of all sizes to start flying in GitLab quickly and with ease. 
* Compliance: leads on finding more efficient, enjoyable ways for organizations to stay compliant in GitLab - and prove it.
* Analytics: leads on helping users understand the end-to-end process of how they work, where bottlenecks and waste are, and how they can find efficiencies to work more quickly and eliminate risk.

#### Compliance

Historically, compliance as an organizational function has been complex and unfriendly. GitLab, as a complete DevOps platform, is well-positioned to take the friction out of managing your compliance programs. The data you need is already unified within GitLab and not aggregated from disparate data sources, which makes the compliance management process simple and friendly.  Towards this end, the Compliance group at GitLab is focused on three key areas:

* Adding features to enable an organization to enforce compliance controls inside of GitLab (e.g. Define MR approvals at a global level and only authorized users may change those settings);
* Aggregating evidence and other audit information in a way that's easy to obtain and read; and
* Ensuring a comprehensive level of traceability and auditability of GitLab using Audit Events

##### Collaboration

Given the inherent nature of compliance and its relationship to all areas of an organization, the Compliance group will take the lead on [validating](https://about.gitlab.com/handbook/product-development-flow/#validation-track) compliance problems. 

In the majority of cases, the Compliance group will function autonomously as other [product groups](/company/team/structure/#product-groups) do to [validate and build solutions](/handbook/product-development-flow/) to these customer challenges. However, the best solution for our users may involve specialized knowledge or significant engineering work in other areas of the product more closely maintained by other groups. In these cases, the Compliance group will recommend those groups (such as Manage:Access, Verify:CI, Release:Release Management) lead on the engineering effort. In order to be crisp on scope and expectations - all Compliance improvements described here will be validated and built by the compliance team - except those mentioned [here](#separation-of-duties).

To ensure we build a cohesive Compliance experience at GitLab, the following points of view are a baseline for other GitLab product groups to consider.

##### Separation of Duties

The concept of separation of duties is the idea of requiring more than a single individual to complete a task; ensuring there is oversight and review by multiple individuals to catch errors or prevent fraud. This separation can occur at various points throughout an organization. At GitLab, this separation may need to occur in the following places (not an exhaustive list):

* At the merge request to ensure an author or committor cannot approve their own changes
* Within CI/CD pipelines to separate approvers of specific jobs, from build to deploy
* Managing roles for individuals who can make changes to production or non-production environments  
* Accessing or modifying secrets to prevent unauthorized users from taking elevated actions

**Current work underway at GitLab for separation of duties**

| Initiative | DRI & Implementation Group |
| ------ | ------ |
| [Separation of Duties in Deployment](https://gitlab.com/gitlab-org/gitlab/-/issues/201898) | Release:Release Management  |
| [Separation of Duties in CI: Include Child pipelines in DAG view](https://gitlab.com/gitlab-org/gitlab/-/issues/217780) | Verify:CI |
| [Separation of Duties in CI: Parent of Child pipelines missing Child artifacts](https://gitlab.com/gitlab-org/gitlab/-/issues/215725) | Verify:CI |
| [Separation of Duties with Compliance Templates](https://gitlab.com/groups/gitlab-org/-/epics/3156) | Manage:Compliance |

### Analytics

The Analytics group (part of the Manage stage) in GitLab helps organizations more quickly recognize the value of their innovations. We do this in two ways. First by providing *visibility into valuestreams and highlighting wastes*, GitLab create awareness of process improvement opportunities and allow organizations and teams to track their progress. Second, GitLab *increases the effectiveness and effiency of knowledge workers in daily work*, prompting decisionmaking that maximizes benefit across the whole value stream. Counter-productive local optimizations are a natural result of a limited field-of-view. By creating situtional awareness informed by analytics and value-stream thinking, we give every GitLab user the superpower of extrodinarly insight and efficiency.
 
#### Focus Areas in 2020.

1. **Basic value stream measures** help teams to accurately characterize the overall performance of their value streams. 
2. **Open and flexible dashboards and APIs** help other GitLab development teams, solution architects and service teams, as well as customers to contribute measures and visualize them in a single view.
3. **Optimize the Value Stream in everyday work.** We will be focussing more on analytics experiences that provide value in daily use.  For example:
   * personal work queues
   * team visual management and planning scenarios (in collaboration with the Plan stage)

#### Analytics Categories

##### DevOps Reports

The DevOps Reports feature allows teams to build dashboards from the data available in GitLab and to drill-in for additional details. With a single pane of glass displaying metrics from various parts of the organization, teams have at-a-glance access to key performance indicators from every part of the development lifecycle.

* Create information-rich dashboards on a single platform; without the need for external dashboard systems, third party add-ons or integrations.
* Out-of-the-box dashboards provide immediate value to teams newly adopting GitLab.
* Mature GitLab teams can build dashboards appropriate for their specific audiences and KPIs.
* A currated library of metrics from every part of the DevOps library are available for exploration and analysis.
* Record level detail is available on drill-in.
* Export data into CSV format.
* Embed charts in wiki pages or external tools.

This is an important area of investment for GitLab since it allows us to:
* Create a consistent user experience not only from out-of-box and user-defined dashboards themselves, but when drilling-in from other areas of the product where analytics are embedded. 
* Reduce the number of seperate analytics pages in the product while providing more flexibility and value. This will make Analytics easier to find and consume.
* Broaden the range of measures we can offer by opening contribution from other GitLab engineering teams, solutions archtiects, services professionals, partners and customers.
* Focus future investment on signature experiences aligned to specific daily interactions where GitLab analytics and value stream thinking can give GitLab users superpowers. 
* Get more value out of analytics product enhancements by having them apply to every metric offered (reuse).

Specific near term steps in this area include:
* Building APIs and patterns that create a metrics library for exploration.
* Creating a single report page that can display a wide range of metrics in manner appropriate to each without new front-end coding.
* Build out a metrics library from metrics already available in GitLab.
* Consolidate existing analytics pages to leverage these new capabilities.

Later we will add:
* Configurable dashboards
* Additional chart types, display options, and options for data analysis

#### Dogfooding

As part of our effort to [dogfood everything](https://about.gitlab.com/handbook/product/product-management/process/#dogfood-everything), the Analytics group maintains a [dogfooding epic](https://gitlab.com/groups/gitlab-org/-/epics/3049) to serve our internal customers.

## 3 Year Strategy

3 years from now, software will be eating the world faster than ever. As Satya Nadella said, "every company is a software company", reinforcing a trend that's had decades to mature. It's a trend that's only accelerating: exogenous events like COVID-19 are putting even greater emphasis on automation and collaboration, even in [more traditional industries](https://fortune.com/2020/05/11/permanent-work-from-home-coronavirus-nationwide-fortune-100/). Knowledge workers across geographies and industries will thrive, with work becoming more distributed and asynchronous than ever.

While the pie grows, the increasing demand for software increases the spectrum of customer needs in tools like GitLab. New types of customers lead to new requirements - security and compliance, for example - and GitLab will be challenged to continue to expand the needs of these industries and new use cases. DevOps spending is predicted to grow at 23.5% CAGR between 2018 - 2023 (IDC 2019), and a rapidly expanding pie means both catering to these new customers and deepening our relationship with existing personas.

### All-in on SaaS
The growth in DevOps spending is predicted to be led by cloud deployment, and for good reason. All things equal, few organizations want to maintain their own tooling infrastructure. A need for control, compliance, and security compels organizations to self-hosted deployments; over the next 3 years, we'll make progress against each of these needs by:
* Improving isolation and administrative control on GitLab.com to match self-managed deployments.
* Bringing many capabilities currently exclusive to self-managed to GitLab.com, like [LDAP group sync](https://docs.gitlab.com/ee/administration/auth/ldap-ee.html#group-sync).
* Supporting federated architecture patterns, allowing users to work across multiple instances and deployments.

Other stages will support other dimensions of this theme, such as [SaaS reliability](https://about.gitlab.com/direction/enablement/#saas)
and [multi-platform support](https://about.gitlab.com/direction/ops/#multi-platform-support).

### GitLab as a business hub
Ultimately, tools that engineers build serve an organization's goal. Whether you're part of a non-profit, a public sector organization, or a for-profit corporation, software is built for a purpose. GitLab's aspiration is to help you measure your progress against that goal better than any other tool. The [DevOps toolchain crisis](https://about.gitlab.com/devops-tools/) is real, and it doesn't stop at software development - it extends to the many tools companies use to accomplish their goals. While our 3-year goal may not to be displace specific tools well beyond the development workflow, our aspiration is to delight a continually broader swathe of personas in our tool. Delighting business-minded personas are next on the list, by:
* Connecting business objectives to your development process, allowing organizations to track initiatives that are moving the needle in a single application.
* Introducing ML-powered insights that identify areas of waste in your process and surfacing anomalous events for security and compliance teams.
* Making GitLab's ROI apparent to executives through great dashboards and reporting.
* Delighting FP&A teams by making finance and accounting valued personas in GitLab, making R&D capitalization and expense reporting easy.

### Fully managed compliance
According to a 2019 IDC report, only 11% of survey respondents had security and compliance embedded into their DevOps processes. Most see these steps as frustrating, time-consuming bottlenecks that take many people-hours to resolve. Like [security](https://about.gitlab.com/direction/secure/#security-is-a-team-effort), compliance is a team effort - and when shifted left, becomes significantly more painless and cost-effective. We'll build on our compliance roadmap by:
* Extending our compliance posture on GitLab.com for the public sector with FedRAMP authorization and supporting European data residency.
* Providing a lovable permissions and access model for users to keep your GitLab deployment secure and in compliance.
* Preventing configuration drift jeopardizing your compliance posture with alerts and evidence reports that delight auditors.

### Shortening time-to-value
On theme with a wide variety of industries adopting DevOps, our goal is getting customers into the product, getting them started, and getting out of the way. Our challenge is to make GitLab intuitive and easy to use without a steep learning curve; we've built our application on a foundation of [small primitives](https://about.gitlab.com/handbook/product/product-principles/#prefer-small-primitives), and our goal is to reduce the amount of configuration and setup you need to get your team productive. We'll get users and organizations to their "ah-ha" moment faster by:
* Allowing instances to import from and integrate a wide variety of tools that customers use and love.
* Adopting lovable templates for common use cases throughout the product, teaching our users best practices from industry leaders.
* Making user onboarding lovable and intuitive across a variety of personas.

## Plan for 2020

In 2020, the Manage stage will provide such compelling, must-have value to large customers that we will be able to **attribute over $100M in ARR to Manage capabilities by end of year**. This means that Manage is a must-have part of the feature set that supports that customer, or Manage was a key part of their adoption journey.
 
**3 themes will guide Manage in 2020**:

### Enterprise readiness
We're going to focus on increasing and retaining the number of customers with enterprise-grade needs by solving their most compelling problems. We're doing this by focusing on:
* Enterprise-grade authentication and authorization. We'll focus on SAML and build excellent compatibility and documentation with large identity providers. This should work on both GitLab.com and self-managed.
* Comprehensive audit events for everything that’s done within GitLab and allowing those events to be accessible via the API and UI.
* Isolation and control, especially for GitLab.com. For some organizations, there must be safeguards in place to prevent users from viewing or accessing other groups and projects. Providing isolation of group managed accounts will help organizations better manage their GitLab usage by providing a more "instance-like" experience at the group level.
 
Success in this theme looks like:
* Increased adoption by self-managed and GitLab.com organizations over 20 members.
* Increased engagement with enterprise-typical features, like SAML SSO.
* Increased NPS from large organizations.
 
You can track progress against this theme [here](https://gitlab.com/groups/gitlab-org/-/epics/3242).

### Value in GitLab Ultimate
We're going to drive an Ultimate story that creates obvious, compelling value. Currently, the majority of Ultimate's value lies in application security. We will strive to improve the breadth of this tier's value proposition by driving more value in Ultimate, such as:
* Improving tools that help compliance-minded organizations thrive. GitLab makes it easy to contribute, but administrators should have comprehensive control to establish, enforce, and provide evidence of organizational policies that are part of a compliance program or framework. Our compliance vision will evolve to introduce features that enable organizations to rely on GitLab for the enforcement and documentation of policies they set.
* More customizable and fine-grained permissions. GitLab's RBAC permissions system works well for most, but we should offer more powerful customization for customers to leverage. 
* Powerful analytical insights. Provide dashboarding and analytics for project and portfolio management, allowing business to track and communicate progress on work in flight, capacity of teams and projects, and overall efficiency across their full portfolio.
 
Success in this theme looks like:
* Increased share of IACV in Ultimate/Gold.
* Increased adoption and engagement with Ultimate-level features.
 
You can track progress against this theme [here](https://gitlab.com/groups/gitlab-org/-/epics/3243).

### Easy adoption
Manage will create easy paths to support our land-and-expand strategy. There's a starting point for any organization with an expansive new tool, and Manage will make this transition easy by supporting natural starting points - ideally in Core, for all groups - that get our customers started and hooked on GitLab:
* Easier import at any scale. Large-scale moves to GitLab should be significantly easier. We'll particularly focus on the user experience migrating from 2-3 key competitors, including gracefully recovering from failures.
* Drive entry-level enterprise table stakes into Core. Each group will focus on a Core value proposition that allows every user to get value - and encourages enterprises testing the water to land (and later expand) in GitLab.
 
You can track progress against this theme [here](https://gitlab.com/groups/gitlab-org/-/epics/3244).

## Metrics

Manage uses [Stage MAU](https://about.gitlab.com/handbook/product/metrics/#stage-monthly-active-users-smau) as a primary measure of success. This represents the unique number of users getting value from the stage; all groups should be able to contribute to improving this number. 

Manage's Stage MAU is currently being improved. Please see [this issue](https://gitlab.com/gitlab-org/manage/general-discussion/-/issues/17231) to track progress.

Individual groups track progress against a number of group-specific [performance indicators](https://about.gitlab.com/handbook/product/metrics/):

| Group | Dashboard URL |
| ------ | ------ |
| Access | https://app.periscopedata.com/app/gitlab/636494/ |
| Analytics | TBC |
| Compliance | https://app.periscopedata.com/app/gitlab/663045/ |
| Import | https://app.periscopedata.com/app/gitlab/661967/ | 

## How we operate
Manage operates under GitLab's values, but is a stage that seeks to particularly excel in certain areas that support our goals above. We seek to be leaders at GitLab by:

### Iterate on the essential
* Leading the way on iteration, regularly shooting for small but ambitious MVCs.
* Supporting iteration with a great planning and development process, giving us checkpoints to keep issues small and incremental. As a result, our throughput is high. 
* Valuing the 1-year themes above, and deliberately deciding to not pursue initiatives that don’t support our 2020 goals. We'd rather do a few things well than a bunch of things poorly.
* Prioritizing depth over breadth. For the most part, we’re biased toward doubling down and investing on what’s working rather than extending the breadth of our stage. 

### Measure what matters
* Prioritizing instrumentation through our North Star dashboards, which we regularly monitor to keep our priorities in check.
* Measuring business value by tying customer delight and revenue to our priorities.

### Great team
* Aspiring to be the happiest team at GitLab, with high individual job satisfaction.
* Having great work-life balance, ensuring that we [value friends and family above work](https://about.gitlab.com/handbook/values/#family-and-friends-first-work-second) and avoid individual burnout.

You can track our operational goals [here](https://gitlab.com/groups/gitlab-org/-/epics/3245).

<%= partial("direction/categories", :locals => { :stageKey => "manage" }) %>

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "manage" }) %>
