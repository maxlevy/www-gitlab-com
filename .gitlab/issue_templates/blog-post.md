<!-- PLEASE READ: See https://about.gitlab.com/handbook/marketing/blog/ for details on the blog process. Unless you are just suggesting a blog post idea, you will need to create the formatted merge request for your blog post in order for it to be reviewed and published. -->

### Proposal

<!-- What do you want to blog about? Add your description here. If you are making an announcement, please open an `announcement` request issue in the Corporate Marketing project instead: http://about.gitlab.com/handbook/marketing/corporate-marketing/#requests-for-announcements -->

### Checklist

- [ ] If you have a specific publish date in mind (please allow 3 weeks' lead time)
  - [ ] Include it in the issue title
  - [ ] Give the issue a due date of a minimum of 2 working days prior
  - [ ] If your post is likely to be >2,000 words, give a due date of a minimum of 4 working days prior
- [ ] If [time sensitive](https://about.gitlab.com/handbook/marketing/blog/#process-for-time-sensitive-posts)
  - [ ] Add ~"Blog: Priority" label and supplied rationale in description
  - [ ] Mention `@rebecca` to give her a heads up ASAP
- [ ] If wide-spread customer impacting or sensitive
  - [ ] Add ~"sensitive" label
  - [ ] Mention `@nwoods` to give her a heads up ASAP
- [ ] If the post is about one of GitLab's Technology Partners, including integration partners, mention `@TinaS`,  apply the ~"Partner Marketing" label, and see the [blog handbook for more on third-party posts](https://about.gitlab.com/handbook/marketing/blog/index.html#third-party-posts)
- [ ] If the post is about one of GitLab's customers, mention `@KimLock` and `@FionaOKeeffe`, apply the ~"Customer Reference Program" label, and see the [blog handbook for more on third-party posts](https://about.gitlab.com/handbook/marketing/blog/index.html#third-party-posts)
- [ ] Indicate if supporting an event or campaign
- [ ] Indicate if this post requires additional approval from internal or external parties before publishing (please provide details in a comment)

/label ~"blog post" ~"Blog::Pitch"
