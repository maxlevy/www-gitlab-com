<!-- PLEASE READ: See https://about.gitlab.com/handbook/marketing/blog/ for details on the blog process. -->

<!-- All blog posts should have a corresponding issue. Create an issue now if you haven't already, and add the link in place of the placeholder link below -->
Closes https://gitlab.com/gitlab-com/www-gitlab-com/issues/XXXX

<!-- Pro tip: If you begin your MR title with "Blog post:" your review app will be ready much faster. -->

### Checklist for writer

- [ ] Link to issue added, and set to close when this MR is merged
- [ ] Due date and milestone (if applicable) added for the desired publish date
- [ ] If [time sensitive](https://about.gitlab.com/handbook/marketing/blog/#process-for-time-sensitive-posts)
  - [ ] Added ~"priority" label
  - [ ] Mentioned `@rebecca` to give her a heads up ASAP
- [ ] [Blog post file formatted correctly](http://about.gitlab.com/handbook/marketing/blog/#formatting-guidelines), including any accompanying images
- [ ] All relevant [frontmatter](https://about.gitlab.com/handbook/marketing/blog/#frontmatter) included
- [ ] Review app checked for any formatting issues
- [ ] Reviewed by fellow team member
- [ ] If approval before publishing is required
  - [ ]  Any required internal or external approval for this blog post has been granted (please leave a comment with details)  
- [ ] Assign to `@rebecca` for review

/label ~"blog post"
