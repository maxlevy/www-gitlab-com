Process Improvements? Have suggestions for improving the release post process as we go?
Capture them in the [Retrospective issue](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/TODO).

- **Preview page** (shows latest merged content blocks for reference till the 17th): https://about.gitlab.com/releases/gitlab-com/
- **View App** (shows introduction, MVP and latest merged content blocks for reference 18th - 21st: https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/index.html


_Release post:_

- **Intro**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/releases/posts/YYYY-MM-22-gitlab-X-Y-released.html.md
- **Items**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/release_posts/X_Y
- **Images**: https://gitlab.com/gitlab-com/www-gitlab-com/tree/release-X-Y/source/images/X_Y

_Related files:_

- **Features YAML** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/features.yml
- **Features YAML Images** link: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/images/feature_page/screenshots
- **Homepage card**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/source/includes/home/ten-oh-announcement.html.haml
- **MVPs**: https://gitlab.com/gitlab-com/www-gitlab-com/blob/release-X-Y/data/mvps.yml

_Handbook references:_

- Blog handbook: https://about.gitlab.com/handbook/marketing/blog/
- Release post handbook: https://about.gitlab.com/handbook/marketing/blog/release-posts/
- Markdown guide: https://about.gitlab.com/handbook/engineering/technical-writing/markdown-guide/

_People:_

- Release Post Managers: https://about.gitlab.com/handbook/marketing/blog/release-posts/managers/
- Release Managers: https://about.gitlab.com/community/release-managers/

| Release post manager | Tech writer | Messaging | Social |
| --- | --- | --- | --- |
| `@release_post_manager` | `@tw_lead` |`@pmm_lead` | DRI: `@wspillane` & `@social` for Slack Checklist item|

### Release post kickoff (`@release_post_manager`)

**Due date: YYYY-MM-DD** (By the 7th)

**Before starting on this checklist, you should have created the release post branch and required fields [as explained in the Handbook](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-your-release-post-branch-and-required-files)**

- [ ] Verify this MR is labeled ~"blog post" ~release ~"release post" ~P1 and assigned to you (the Release Post Manager)
- [ ] Add the current milestone to this MR
- [ ] Create a release post retrospective issue ([example](https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7918)) and update the link at the top of this MR description
- [ ] Replace each `@mention` in this MR description with the names of the Release Post Manager, Tech Writer, Messaging Lead, and Social Lead for this release

- [ ] Update the links in this MR description
- [ ] Update all due dates in this MR description
- [ ] Make sure the release post branch has all initial files: `source/releases/posts/YYYY-MM-DD-gitlab-X-Y-released.html.md`, `data/release_posts/X_Y/mvp.yml` and `data/release_posts/X_Y/cta.yml`
- [ ] Add the release number and your name as the author to `source/releases/posts/YYYY-MM-22-gitlab-X-Y-release.html.md`
- [ ] Per guidance on [communication](https://about.gitlab.com/handbook/marketing/blog/release-posts/#communication) for the release, create X-Y-release-post-prep channel in Slack and invite `@tw_lead` and `@pmm_lead`
- [ ] Set up a 15 minute weekly standup with the TW lead and Messaging lead to touch base and troubleshoot. If times zones conflict, this is not mandatory.
- [ ] In the #release-post Slack channel, remind Product Managers that all [content blocks (features, recurring, bugs, etc.)](#content-blocks) should be drafted, and under review by the 10th. All direction items and notable community contributions should be included in the release post.


### Recurring items starting on the 12th: `@release_post_manager`

- [ ] Pull `master` into the `release-X-Y` branch, resolve any conflicts. Do this every couple of days so that new release post items are added to the review app.

      1.  `git checkout release-X-Y`
      1.  `git pull release-X-Y`
      1.  `git pull origin master`
      1.  `bin/release-post-assemble`
      1.  `git push`
      1.  Do a visual check of the release-X-Y content block and image folders to make sure paths and names are correct
      1.  Make sure the review app generates as expected and notify the team there's an updated review app available

- [ ] Alert DRIs one working day before each due date (post a comment to #release-post Slack channel)

### General contributions `@release_post_manager`

The release post is authored following a changelog style system.
Each item should be in an individual YAML file.

#### Contribution instructions

See [Handbook: Contributing to the release post](https://about.gitlab.com/handbook/marketing/blog/release-posts/#general-contributions).

#### Content blocks

**Due date: YYYY-MM-DD** (10th)

Product Managers are responsible for [raising MRs for their content blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#pm-contributors) and ensuring they are reviewed by necessary contributors by the due date. Content blocks should also be added for any epics or notable community contributions that were delivered.

Product Managers are also responsible for making sure all [required (Tech Writing) and recommended (PM Director and PMM) reviews ](https://about.gitlab.com/handbook/marketing/blog/release-posts/#reviews) get done for their content blocks. To help reviewers prioritize what to review, PMs should communicate which content blocks are most important for review by applying the proper labels to the release post item MR prior to assigning the MR to reviewers. (ex: Tech Writing, Direction, Deliverable, etc). PMs can also [follow these guidelines](https://about.gitlab.com/handbook/marketing/blog/release-posts/#recommendations-for-optional-director-and-pmm-reviews) to help decide which content blocks should get PM Director and PMM reviews.

To enable Engineering Managers [to merge the content blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#merging-content-block-mrs) as soon as an issue has closed, PMs should ensure all scheduled items have MRs created for them and have the Ready label applied when content contribution and reviews are completed.

Product Managers should only check their box below when **all** their content blocks (features, deprecations, etc.) are complete (documentation links, images, etc.). Please don't check the box if there are still things missing.

_Reminder: be sure to reference your Direction items and Release features._ All items which appear
in our [Upcoming Releases page](https://about.gitlab.com/upcoming-releases/) should be included in the relevant release post.
For more guidance about what to include in the release post please reference the [Product Handbook](https://about.gitlab.com/handbook/product/communication/#release-posts).

- Manage
  - [ ] Access (`@mushakov`)
  - [ ] Analytics (`@jshackelford`)
  - [ ] Compliance (`@mattgonzales`)
  - [ ] Import (`@hdelalic`)
- Create
  - [ ] Source Code (`@danielgruesso`)
  - [ ] Knowledge (`@cdybenko`)
  - [ ] Static Site Editor (`@ericschurter`)
  - [ ] Editor (`@phikai`)
  - [ ] Gitaly (`@jramsay`)
  - [ ] Gitter (`@ericschurter`)
  - [ ] Ecosystem (`@deuley`)
- Plan
  - [ ] Project Management (`@gweaver`)
  - [ ] Portfolio Management (`@kokeefe`)
  - [ ] Certify (`@mjwood`)
- Verify
  - [ ] Continuous Integration (`@thaoyeager`)
  - [ ] Runner (`@DarrenEastman`)
  - [ ] Testing (`@jheimbuck_gl`)
- Package
  - [ ] Package (`@trizzi`)
- Release
  - [ ] Progressive Delivery (`@ogolowinski`)
  - [ ] Release Management (`@jmeshell`)
- Configure
  - [ ] Configure (`@nagyv-gitlab`)
- Monitor
  - [ ] APM (`@dhershkovitch`)
  - [ ] Health (`@sarahwaldner`)
- Secure
  - [ ] Static Analysis (`@tmccaslin`)
  - [ ] Dynamic Analysis (`@derekferguson`)
  - [ ] Composition Analysis (`@NicoleSchwartz`)
  - [ ] Fuzz Testing (`@stkerr`)
- Defend
  - [ ] Container Security (`@sam.white`)
  - [ ] Threat Insights (`@matt_wilson`)
- Growth
  - [ ] Fulfillment (`@mkarampalas`)
  - [ ] Retention (`@mkarampalas`)
  - [ ] Telemetry (`@sid_reddy`)
  - [ ] Expansion (`@timhey`)
  - [ ] Conversion (`@s_awezec`)
  - [ ] Acquisition (`@jstava`)
- Enablement
  - [ ] Distribution (`@ljlane`)
  - [ ] Geo (`@fzimmer`)
  - [ ] Memory (`@joshlambert`)
  - [ ] Global Search (`@JohnMcGuire`)
  - [ ] Database (`@joshlambert`)

#### Recurring content blocks

**Due date: YYYY-MM-DD** (10th)

The following sections are always present and managed by the PM or Eng lead
owning the related area.

- [ ] Add GitLab Runner improvements: `@DarrenEastman`
- [ ] Add Omnibus improvements: `@ljlane`
- [ ] Add Mattermost update to the Omnibus improvements section: `@ljlane`

**Due date: YYYY-MM-DD** (12th)
- [ ] Add Performance Improvements section: `@release post manager`
- [ ] Add Bug Fixes section: `@release post manager`

#### Final Merge

**Due date: YYYY-MM-DD** (17th)

Engineering managers listed in the MRs are responsible for merging as soon as the implementing issue(s) are officially part of the release. All release post items must be merged on or before the 17th of the month. Earlier merges are preferred whenever possible. If a feature is not ready and won't be included in the release, the EM should push the release post item to the next milestone.

To assist managers in determining whether a release contains a feature. The following procedure [documented here](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/34519) is encouraged to be followed. In the coming releases, Product Management and Development will prioritize automating this process both so it's less error prone and to make the notes more accurate to release cut.

### Content assembly and initial review (`@release_post_manager`)

**Due date: YYYY-MM-DD** (12th or earlier)

- [ ] [Create MRs for the Performance Improvements and Bug Fix content blocks](https://about.gitlab.com/handbook/marketing/blog/release-posts/#create-mrs-for-bugs-and-performance-improvements)
- [ ] In the comments in this MR, mention `@gitlab-com/backend-managers` and `@gitlab-org/frontend/frontend-managers` and ask them to add their team Performance Improvements and Bugs Fixes to the MRs you've created, referencing links to the specific MRs
- [ ] In Slack #release-post, share the link to the Bug Fixes MR and ask PMs to work with their EM to add high impact bugs to the MR

**Due date: YYYY-MM-DD** (15th or earlier)

- [ ] Select a [cover image](https://about.gitlab.com/handbook/marketing/blog/release-posts#cover-image). On the `release-X-Y` branch, add the cover image to `source/images/X_Y/X_Y-cover-image.jpg`. In `source/releases/posts/YYYY-MM-DD-gitlab-X-Y-released.html.md`, [add details about the source image](https://about.gitlab.com/handbook/marketing/blog/release-posts/#cover-image-license).
- [ ] In the #release-post Slack channel, [request MVP nominations](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp)
- [ ] Remind `@gitlab-com/backend-managers` and `@gitlab-org/frontend/frontend-managers` to add their team Performance Improvements and Bug Fixes to the MRs you've created, referencing links to the specific MRs

**Due date: YYYY-MM-DD** (17th or earlier)

- [ ] Mention the [Distribution PM](https://about.gitlab.com/handbook/product/categories/#distribution-group) reminding them to add any relevant [upgrade warning](https://about.gitlab.com/handbook/marketing/blog/release-posts/#important-notes-on-upgrading) by doing an [upgrade MR](https://about.gitlab.com/handbook/marketing/blog/release-posts/#upgrades)
- [ ] Ask the [Messaging Lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead) to finalize the introduction by the 18th
- [ ] Check with the [Messaging Lead](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead) to see if they need help pinning down the final themes/features for the intro
- [ ] If there are no deprecation MRs, ask in the #release-post Slack channel if there are any deprecations to be included
- [ ] Decide on the [MVP](https://about.gitlab.com/handbook/marketing/blog/release-posts/#mvp) and work with the nominator of the MVP to write the MVP section in `data/release_posts/X_Y/mvp.yml` on the `release-X-Y` branch
- [ ] Make sure the PM for the MVP feature adds a corresponding content block if applicable, linking from the MVP section
- [ ] On the `release-X-Y` branch, add the MVP's name and other profile info to `data/mvps.yml`

**Due date: YYYY-MM-DD** (18th or earlier)
- [ ] Remind PMs to check off their [content and recurring blocks](#content-blocks) by tagging them into this MR
- [ ] Label this MR: ~"blog post" ~release ~"review-in-progress
- [ ] Run the content through an automated spelling and grammar check
- [ ] Check if there are no broken links in the review app (use a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf))
- [ ] Check all comments in the MR thread (make sure no contribution was left behind)
- [ ] Make sure all discussions in the thread are resolved
- [ ] Assign the MR to the next reviewer (structural check)

### Other reviews

Ideally, complete the reviews by the 19th of the month,
so that the 2 days before the release
can be left for fixes and small improvements.

#### [Structural check](https://about.gitlab.com/handbook/marketing/blog/release-posts/#structural-check) (Technical Writing Lead)

**Due date: YYYY-MM-DD** (18th)

The structural check is performed by the technical writing lead: `@tw_lead`

For suggestions that you are confident don't need to be reviewed, change them locally
and push a commit directly to save the PMs from unneeded reviews. For example:

- clear typos, like `this is a typpo`
- minor front matter issues, like single quotes instead of double quotes, or vice versa
- extra whitespace

For any changes to the content itself, make suggestions directly on the release post
diff, and be sure to ping the reporter for that block in the suggestion comment, so
that they can find it easily.

- [ ] Add the label ~review-structure.
- [ ] Pull `master` into the release post branch and resolve any conflicts.
- [ ] Check [frontmatter](https://about.gitlab.com/handbook/marketing/blog/release-posts/#frontmatter) entries and syntax.
- [ ] Check that images match the context in which they are used, and are clear.
- [ ] Check all images (png, jpg, and gifs) are smaller than 150 KB each.
- [ ] Check for duplicate entries.
- [ ] Check all dates mentioned in entries, ensuring they refer to the correct year.
- [ ] Remove any `.gitkeep` files accidentally included.
- [ ] Add or check `cover_img:` license block (at the end of the post). Should include `image_url:`, `licence:`, `licence_url:`.
- [ ] Check the anchor links in the intro. All links in the release post markdown file should point to something in the release post Yaml file.
- [ ] Run a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf) and ping reporters directly on Slack asking them to fix broken links.
- [ ] Run a spelling check against the Release Post's review app. For example, using [Webpage Spell-Check](https://chrome.google.com/webstore/detail/webpage-spell-check/mgdhaoimpabdhmacaclbbjddhngchjik?hl=en) for Google Chrome.
- [ ] Ensure content is balanced between the columns (both columns are even).
- [ ] Pull `master` into the release post branch and resolve any conflicts (again).
- [ ] Report any problems from structural check in the `#release-post` channel by pinging the reporters directly for each problem. Do NOT ping `@all` or `@channel` nor leave a general message to which no one will pay attention. If possible, ensure open discussions in the merge request track any issues.
- [ ] Post a comment in the `#whats-happening-at-gitlab` channel linking to the review app + merge request reminding the team to take a look at the release post and to report problems in `#release-post`. CC the release post manager and messaging lead. Template to use (replace links):
  ```md
  Hey all! This month's release post is almost ready! Take a look at it and report any problems in #release-post.
  MR: https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/1234
  Review app: https://release-X-Y.about.gitlab-review.app/releases/YYYY/MM/DD/gitlab-X-Y-released/index.html
  ```
- [ ] Remove the label ~review-structure.
- [ ] Assign the MR to the next reviewer (Marketing content and reviews).
- [ ] Within 1 week, update the release post templates and release post handbook with anything that comes up during the process.


#### [Marketing content and positioning](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead) (Messaging Lead)

**Due date: YYYY-MM-DD** (18th - 20th)

The Marketing content, positioning and reviews are done by the Messaging lead: `@pmm_lead`
  - [ ] Starting any time after the 22nd of the previous month and until final merge on the 17th of the next month, you can look at [X-Y Preview page](https://about.gitlab.com/releases/gitlab-com/) for the latest features available on .com that will likely get packaged for the current release cycle on the 22nd
  - [ ] Before 13th, shortlist top release themes / features - process listed in [release post handbook page](https://about.gitlab.com/handbook/marketing/blog/release-posts/#messaging-lead)
  - [ ] On 13th, ping on #release-post slack channel for feedback from the product team on top features (give a list of 4-5 themes or 8-10 features)
  - [ ] Before 14th, commit the first (rough) draft of the release post introduction with the various themes/features selected above in YYYY-MM-DD-gitlab-X-Y-released.html.md in the release X-Y branch. Notify the TW lead that the draft introduction is available
  - [ ] After EOD 17th, you can look for  merged release post items with label 'Ready' which indicate features making it in the release, to update your draft introduction based on any changes in the release. At this time you should also be able to reference the latest review app the release post manager publishes on the 18th.
  - [ ] Before 18th, check/copyedit feature blocks (optional as it is reviewed by PMM leads for the stages)
  - [ ] Before 18th, check/copyedit `features.yml` (optional as it is reviewed by PMM leads for the stages)
  - [ ] By the 18th, [reorder primary and secondary features](https://about.gitlab.com/handbook/marketing/blog/release-posts/#feature-order) by stage and/or according to their relevance per the theme of your release post intro (items are sorted by adding 0_1, 0_2, etc. in ascending order to beginning of content block filename). Reorderng primary features is an option but not required.
  - [ ] By the 18th, ping on #release-post slack channel with your top 3 themes/features. Make sure to cc the Head of Products and Directors
  - [ ] By the 18th, after receiving feedback from Heads of Product and Directors, ping on the Slack #ceo channel with your top 3 themes/features and draft introduction, including the Release Post Manager as an FYI. Revise as required and let the Release Post Manager know when updates are complete so they can begin [Final content review](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/44776#final-content-review)
  - [ ] Before 20th, on the `release-x-y` branch update the release blurb for the homepage (check /source/includes/home/ten-oh-announcement.html.haml - detailed instructions in the handbook)
  - [ ] Before 20th, ensure that the social sharing text for the tweet announcement is available in the introduction
  - [ ] Before the 20th, on the `release-X-Y` branch, add [the social sharing image](https://about.gitlab.com/handbook/marketing/blog/release-posts#social-sharing-image) to `source/images/tweets/gitlab-X-Y-released.png`.
  - [ ] Before the 20th, on the `release-X-Y` branch, in the Intro, update the '/images/tweets/gitlab-X-Y-released.png' to reference the social sharing image filename
  - [ ] On 20th, Scott W. (`@sfwgitlab`) for final check
  - [ ] On 20th, remove the label ~review-in-progress after Scott's review
  - [ ] On 20th, assign the MR back to the Release Post Manager

#### Final content review (`@release_post_manager`)

**Due date: YYYY-MM-DD** (18th - 19th)

- [ ] Check to be sure there are no broken links in the review app (use a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf))
- [ ] Mention `@sfwgitlab`, `@adawar`, `@kencjohnston`, `@jyavorska`, `@ebrinkman`, `@joshlambert` `@David` and `@hilaqu`  to remind them to review (directors do not need to re-review individual release post item as that should have been addressed prior to the 17th)
- [ ] Mention `@sytse` in #release-post on Slack when the post has been generated for his review

**Due date: YYYY-MM-DD** (by the 20th)

- [ ] Make sure all feedback from CEO and Product team reviews have been addressed by working with DRIs of those areas as needed
- [ ] Make sure there are no open feedback items in this MR or in Slack #release-post channel
- [ ] Make sure PM DRIs have all checked off their content blocks as needed in [General contributions](#general-contributions)

### Preparing to merge to master (`@release_post_manager`)
#### On the 21st

- [ ] Make sure the [homepage card](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/source/includes/home/ten-oh-announcement.html.haml) was updated by the Messaging lead on the release X-Y branch
- [ ] Check with the Messaging lead to confirm the social sharing copy is finalized (should be visible in review app at this point)
- [ ] Lock [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml) with File Locking **on the 21st**
- [ ] Mention `@sumenkovic` on Slack #release-post to remind him to send the swag pack to the MVP
- [ ] Check if all the anchor links in the intro are working
- [ ] Confirm there are no broken links in the review app (use a dead link checker, e.g., [Check my Links](https://chrome.google.com/webstore/detail/check-my-links/ojkcdipcgfaekbeaelaapakgnjflfglf))
- [ ] Do a visual check of the blog post and ordering of content blocks in their relevance to the intro

#### On the 22nd (`@release_post_manager`)

#### At 12:30 UTC

- [ ] Read the [important notes](#important-notes) below
- [ ] Ping the release managers on the `#releases` Slack
channel asking if everything is on schedule, and to coordinate timing with them:
  - Ask the release managers to ping you when the packs are publicly available (and GitLab.com is up and running on the release version)
  - If anything is wrong with the release, or if it's delayed, you must ping
  the messaging lead on `#release-post` so that they coordinate anything scheduled
  on their side (e.g., press releases, other posts, etc).
  - If everything is okay, the packages should be published at [13:30 UTC](https://gitlab.com/gitlab-org/release-tools/-/blob/fac347e5fc4e1f31cffb018d90061ef4f25747f3/templates/monthly.md.erb#L104-125), and available
  publicly around 14:10 UTC.
- [ ] Pull `master` into the `release-X-Y` branch, resolve any conflicts
- [ ] Check to make sure there aren't any alerts on Slack `#release-post` and `#whats-happening-at-gitlab` channels
- [ ] Check to make sure there aren't any alert on this MR thread

### Merging to master (`@release_post_manager`)
#### At 13:50 UTC

- [ ] Check MR pipleines to see if there are any failures. Pull `master` into the `release-X-Y` branch to check for conflicts and resolve as needed

Once the release manager confirmed that the packages are publicly available by pinging you in Slack:

- [ ] Unlock [`features.yml`](https://gitlab.com/gitlab-com/www-gitlab-com/blob/master/data/features.yml) just before merging.
- [ ] Merge the MR at 14:10-14:20 UTC.
- [ ] Wait for the pipeline. It should take ~45min to complete.
- [ ] Check the live URL on social media with [Twitter Card Validator](https://cards-dev.twitter.com/validator) and [Facebook Debugger](https://developers.facebook.com/tools/debug/).
- [ ] Check for broken links again once the post is live.
- [ ] Handoff social posts to the social team and confirm that it's ready to publish: Mention @social in the `#release-post` Slack channel; be sure to include the live URL link and social media copy (you can copy/paste the final copy from the review app).
     - A member of the social team will schedule the posts at the next available best time on the same day. The social team will mark the Slack message with a ⏳ once scheduled and add cheduled times to the post thread for team awareness. Further details are listed below in the Important Notes Section.
- [ ] Share the links to the post on the `#release-posts` and
`#whats-happening-at-gitlab` channels on Slack.

#### What to do if your pipeline fails

For assistance related to failed pipelines or eleventh-hour issues merging the release post, follow the [Handbook on-call](https://about.gitlab.com/handbook/about/on-call/) process by asking for assistance in the `#handbook-escalation` Slack channel. Cross post the thread from #handbook-escalation in #release-post so all Product Managers and release post stakeholders are aware of status and delays.

#### Important notes

- The post is to be live on the **22nd** at **15:00 UTC**. It should
be merged and as soon as GitLab.com is up and running on the new
release version (or the latest RC that has the same features as the release),
and once all packages are publicly available. Not before. Ideally,
merge it around 14:20 UTC as the pipeline takes about 40 min to run.
- The usual release time is **15:00 UTC** but it varies according to
the deployment. If something comes up and delays the release, the
release post will be delayed with the release.
- Coordinate the timing with the [release managers (https://about.gitlab.com/community/release-managers/). Ask them to
keep you in the loop. Ideally, the packages should be published around
13:30-13:40, so they will be publicly available around 14:10 and you'll
be able to merge the post at 14:20ish.
- Once the release post is live, wait a few minutes to see if no one spots an error (usually posted in #whats-happening-at-gitlab or #company-fyi), then follow the `handoff to social team` checklist item above.
- The tweet to share on Slack will not be live, it will be scheduled during a peak awareness time on the 22nd. Once the tweet is live, the social team will share the tweet link in the `#release-post` and in the `#whats-happening-at-gitlab` Slack channels.
- Keep an eye on Slack and in the blog post comments for a few hours
to make sure no one found anything that needs fixing.



/label ~"blog post" ~release ~"release post" ~P1
/assign me
