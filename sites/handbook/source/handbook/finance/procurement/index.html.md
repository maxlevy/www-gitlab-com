---
layout: handbook-page-toc
title: The Procurement Team
---

## On this page
{:.no_toc}

- TOC
{:toc}


Team members can purchase goods and services on behalf of the company in accordance with the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) and guide to [Spending Company Money](/handbook/spending-company-money/). However, all purchases made on behalf of GitLab that are not a personal expense, must first be reviewed by procurement, then signed off by a member of the executive team. 

Finance vendor contract request issues are required for all third party purchases being made on behalf of GitLab that aren't considered a [personal reimbursable expense](/handbook/spending-company-money/#expense-policy). This ensures the organization can appropriately plan for spend and assess supplier risk. Exceptions to this are:

1.  Last minute un-planned onsite event needs such as food and rental transactions needed for event.
2.  One-time field marketing and event purchases less than $10K such as booth rentals, AV equipment, catering, etc.
     - In this instance, the vendor can invoice GitLab in Tipalti, and AP will route approvals based on the matrix


## When should I contact procurement?
1. Contact procurement BEFORE you have a contract. 
1. **Create an issue 60-90 days before you need the service/product** to allow for best negotiations and terms.
1. The sooner procurement is engaged, the better.

## How do I contact procurement?
1. If you have a general sense for your business needs that you need to purchase, open a vendor contract request issue as identified under Requesting Procurement Services. You do NOT need a contract to open a contract request issue.
1. If you have a general question or are looking for direction, use the #procurement slack channel.

## Prior to Contacting Procurement
Prior to engaging Procurement, please review the below guidelines:
1. Review the market capabilities defined by your overall spend *before* selecting your vendor.
1. Before sharing details and/or confidential information regarding GitLab business needs, obtain a [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing) from the potential vendor(s). Refer to the [Signature Authorization Matrix](/handbook/finance/authorization-matrix/) for signing authority. 
   - Create a vendor contract request issue to facilitate this process.
   - The issue creator is responsible for obtaining signature and uploading the signed NDA to Contract Works
1. All vendors must adhere to the [GitLab Partner Code of Ethics](/handbook/people-group/code-of-conduct/#partner-code-of-ethics). It is mandatory all vendors contractually adhere to this if they would like to do business with us. (Note these are typically not required in event related agreements unless the vendor is providing services).
1. Identify your bid requirements based on your estimated spend:
     >$0-$100K: No bid

     >$101K - $250: 2-3 Bids

     >Greater than $250K: RFP

## Requesting Procurement Services

Start working with the Procurement team by opening a vendor contract approval issue based on the type of purchase below. Procurement will not approve the request if the request is incomplete and/or missing information.

Contact Procurement directly in Slack via #procurement if you have questions.

**Note: Before sharing details and/or confidential information regarding our business needs, please obtain a [Mutual Non-Disclosure Agreement](https://drive.google.com/a/gitlab.com/file/d/1hRAMBYrYcd9yG8FOItsfN0XYgdp32ajt/view?usp=sharing) from the potential vendor(s).**

### 1: Purchase Type: Software/SaaS

1. Open a Vendor Contract Request issue with [this template](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=software_vendor_contract_request) to begin the process.
1. Create this issue **BEFORE** agreeing to business terms and/or pricing. 
1. It is preferred we negotiate the best pricing up front to keep our ongoing costs to a minimum across our long-term relationships with vendors. We also continue to evaluate supplier pricing at the time of renewal to minimize our ongoing costs across our long-term relationships with vendors.

A video tutorial of the issue creation process can be found [**HERE**](https://gitlab.zoom.us/rec/share/z-4pcYnpxDJIac-d70_hRaIwFYj8eaa8g3VN8vJbnkypGYJSQZwIUL6R3gGWbxjb?startTime=1587585669000)

### 2: Purchase Type: Professional Services and all other contract types

1. Open a Vendor Contract Request issue with [this template](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=general_vendor_contract_request) 
1. Create this issue **BEFORE** agreeing to business terms and/or pricing. 
1. This template can be used for addendums that either do or do not change pricing.

A video tutorial of the issue creation process can be found [**HERE**](https://gitlab.zoom.us/rec/share/6-h-N5zR71tIS5Xdsn-Pf7NiD53Eeaa80XMZ-fUNxEfuCbA_5yfQNyOY4AUZsmwh?startTime=1584653128000)

### 3: Purchase Type: Field Marketing and Events withOUT Confidential Data

1. Open a Vendor Contract Marketing & Events Request [issue with this template](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=vendor_contracts_marketing_events)
1. Examples for this template type include marketing events, programs, sponsorships, catering, hotels, swag and services that do NOT involve the processing or sharing of data.
1. Due to the rapid nature of these types of requests, procurement will only negotiate if spend is greater then $100K and there is business justification and alignment to do so. 
1. If you will be sharing confidential data with the vendor, please use the template under Purchase Type #2 above.

A video tutorial of the field marketing and events issue creation process can be found [**HERE**](https://gitlab.zoom.us/rec/share/vdJ0d5jS00RJTtKVxRj5fKkfMqXPaaa80XMY-fIFzx7GHZWxe_p688iPeZ_qU85O?startTime=1584654449000)

## What if the Vendor I am working with doesn't require a contract?
1. GitLab requires a Vendor Contract even if your vendor doesn't.
1. The Vendor Contract approval process is designed to protect both you and the GitLab and the vendor. 
1. In this event legal can and will provide contract terms to govern the transaction based on the level of risk.
1. Open the appropriate vendor contract request to initiate the process


## Deep Dive on the Vendor Contract Issue Process
Procurement will not approve a contract issue until all other approvals have been received to validate the appropriate approval process and policies have been followed. In the event procurement approves an issue prior to other approvals in an attempt to avoid being the source of a backlog, procurement will comment in the issue that their approval is "subject to remaining approvals". At this point it is the responsibility of the issue owner to follow the remaining process and secure remaining approvals BEFORE obtaining contract signature.

>### Legal Engagement for Vendor Contracts

1. Legal is responsible for reviewing vendor contracts and will adhere to the legal playbook.
1. A contract cannot be signed until it has been approved by the legal team. Once the legal team approves the contract, legal will upload the contract with the approval stamp. **Contracts will not be signed unless the legal approval stamp is included.**
>#### Rules of Engagement GitLab entity
1. PEO's, Contractors should be engaged with GitLab IT BV
1. Contract to be engaged locally when there is a GitLab entity available (e.g. Netherlands with BV, UK with Ltd et cetera)
1. If there is no GitLab entity available in the country of a vendor use GitLab Inc

>### Security Engagement for Vendor Contracts

1. Security is responsible for reviewing vendor security practicies and will adhere to the [Third Party Vendor Security Review Process](//handbook/engineering/security/security-assurance/security-compliance/third-party-vendor-security-review.html#email-template-for-requesting-security-documentation_). 
1. The Security Compliance team needs 3 business days to complete this review from the time they receive all necessary documentation from the vendor 
1. A contract cannot be signed until it has been approved by the security team. Once the security team approves the vendor and/or identifies gaps in the vendor's security practices for negotiation, security will provide their approval in the issue.
1. Consult the [Data Classification Policy](https://docs.google.com/document/d/15eNKGA3zyZazsJMldqTBFbYMnVUSQSpU14lo22JMZQY/edit#heading=h.a7l25bv5e2pi) to understand whether your contract will need security review. Any contracts that will share RED or ORANGE data will need security approval prior to signing. Additionally, an annual reassessment of vendor's security posture is performed as part of the contract renewal.
1. Complete a [Privacy Review](https://gitlab.com/gitlab-com/legal-and-compliance/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#); please note that this will be done in partnership with GitLab's Privacy Officer and reviewed by Security Compliance during the Security Review.

>### Vendor Contract Approval Process

GitLab team-members must obtain the appropriate approvals from other departments such as the department head of your department, finance (budgetary authorization), procurement, legal, and in some cases security as dictated by the contract issue request.

1. Please consult the [Authorization Matrix](/handbook/finance/authorization-matrix/) to determine who must sign off on Functional Approval and Financial Approval.  

*For events, campaigns, or other program expenses where it makes sense to quantify actuals at the time they are invoiced to accounts payable, please include the Campaign Finance Tag as indicated in the issue template.*

1. If you are purchasing new software/tools, you also need to get approval from IT as identified in the contract issue request.  


>### Vendor Contract Signing Process

1. Do not send the contract to the authorized signatory until all approvals are received. Doing so can put GitLab in a direct financial and/or legal risk which could need to be escalated.
1. Once all approvals in the issue are received, send the contract to the authorized signatory:
     - Upload the contract with the legal stamp to HelloSign
     - If a legal stamp is not included in the issue, please request clarification from legal and/or procurement by tagging them in the contract issue or asking for clarification in the #procurement slack channel.
     - In the description field in HelloSign, paste the link to the vendor contract issue request to avoid delays in signature.
     - Enter the signatory's name and email in HelloSign
 1. Once the contract is signed by GitLab, send the contract to your vendor (if not already signed by the vendor) through HelloSign.
 1. After the contract is signed by both parties, upload the fully executed contract to ContractWorks. You will need to upload the fully signed pdf into the folder labeled **01. To Be Standardized**, which in under the parent folder **01. Uncategorized**. Legal will then organize the contracts using their [instructions and best practices](/handbook/legal/vendor-contract-filing-process)
      - If you need access to ContractWorks, please process an access request [here](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=Single+Person+Access+Request).

- Note: If your vendor has a signature tool they would like to use and all approvals have been received in the contract issue, request that the vendor send the final contract version with the legal stamp for signature and also include the issue link. This will avoid delays in signature.

>### Vendor Contract Access Request

Similar to our Access Request process for team members, we have an access request process for consultants or professional services providers. If the vendor you are working with requires access to systems to complete work, create a Vendor Access Request. Include [**only systems that are necessary**](https://about.gitlab.com/handbook/engineering/security/Access-Management-Policy.html#access-management) to the work that the vendor will be performing. 

Use this [access request template](https://gitlab.com/gitlab-com/contingent-workers/lifecycle/-/issues/new?issuable_template=access-request) and assign it to yourself and the relevant provisioner(s) for the tools that the professional services provider requires access to.

Create an [orientation issue](https://gitlab.com/gitlab-com/contingent-workers/lifecycle/-/issues/new?issuable_template=orientation-issue) if the professional services provider wants support through the set up of the most common tools used at GitLab. Assign to yourself and the professional services provider if they have a GitLab account with the required access.


>### Payment
1. Vendors will be required to create an account within Tipalti in order to receive payment
1. For complete details on how to obtain payment, please visit Accounting's [Procure to Pay](/handbook/finance/accounting/#procure-to-pay) page.
1. If your annual contract value is equal to or greater than $100K, a Purchase Order must be created to pay the vendor. See Creating a Purchase Order for steps to do so.

## Creating a Purchase Order
If your contract has an annual value of $100K or greater, a Purchase Order (PO) must be created to pay your vendor. If an invoice is received that is at or above $100K and there is no PO, the invoice will not be paid until a PO is created which could cause delays to your vendor's receipt of payment.

It is the responsibility of the business owner/issue creator to request a Purchase Order be generated. To do this, complete the steps below within the vendor contract request issue. Once these steps are completed, a PO will be generated by the procurement team:
1. After the contract has been through the approvals process and signed by **both** parties, attach it to the issue in the appropriate step.
     - A PO will not be created until the signed contract is uploaded to the issue in the appropriate step. The request will be rejected and delayed until the contract is uploaded.
     - A PO will not be created unless all approvals have been received in the contract request issue and your vendor will not be paid.
1. Enter the contract start and end date
     - A PO will not be created until these dates are added to the issue in the appropriate step. The request will be rejected and delayed until this is entered.
1. Add the `PO To-Do` label to the issue. This prompts the PO creation process. 
     - A PO will not be created until this label is added to the issue. The request will not be seen and the PO will not be created until this tag is added.
1. Enter the Invoice Approver name. This is the person who will approve and be contacted in the event there are any invoice questions.
     - If no invoice approver name is entered, the PO will default to the person who opened the issue. In the event this is the incorrect person, payment to the vendor may be delayed.

>### Exceptions to the Purchase Order requirement:
1. Confidential Legal Fees 
     - AP will route approvals in Tipalti based on matrix
1. Audit and Tax Fees 
     - AP will route approvals in Tipalti based on matrix
1. Benefits & Payroll 
     - Includes PEOs and benefit related expenses

- [Mutual Non-Disclosure Agreement](https://drive.google.com/a/gitlab.com/file/d/1hRAMBYrYcd9yG8FOItsfN0XYgdp32ajt/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)
- [Media Consent and Release Form](https://drive.google.com/file/d/10pplnb9HMK0J0E8kwERi8rRHvAs_rKoH/view?usp=sharing)

>### Step by step guide on the PO creation process the procurement team is responsible for within Netsuite can be found [here](/handbook/finance/procurement/purchase-order-process/)


### Contract Request Capacity & Back Log

All open contract issue requests, are located at the [Procurement Issue Board](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/boards/1844091)

In the event you have an issue that hasn't received a prompt response from procurement, requestor should mention Aleshia Hansen in the #procurement Slack channel and provide:

1. Link to the Vendor Contract Approval issue; and 
1. Timeline

In the event the procurement team is out of office (as highlighted in PTO calendar or Slack), and the matter is time sensitive, requestor should
contact #legal channel in Slack and provide:

1.  Link to Vendor Contract Approval Issue; and
2.  Reason for escalation, with timeline for requirement(s)

Legal will assign a team member to approve the procurement portion of the issue.

## PO Change Approval request

If you receive an email notifying that a bill could not be approved due to exceptions during the matching process, start working with the Procurement team by opening a PO Change Approval issue to request the change of an existing Purchase Order. This can include an increase or decrease in the dollar amount or any changes to the currency, product, etc. Procurement will not approve the request if the request is incomplete and/or missing information.

Contact Procurement directly in Slack via #procurement if you have questions.

###  Opening a PO Change Approval issue

1.  The Business Owner / Requestor has to open a PO Change Approval issue with [this template](https://gitlab.com/gitlab-com/Finance-Division/procurement-team/procurement/-/issues/new?issuable_template=PO_Change_Approval) to begin the process.
1.  Once the change has been approved by all parties, the Procurement team will amend the Purchase Order in NetSuite.
2.  The Business Owner / Requestor has to send the bill back to Accounts Payable (either by logging into Tipalti or directly clicking on the button "Send back to AP" from the email) and notify that the PO has been amended by adding the PO Change Approval issue URL.
    *  **If the bill is not sent back to AP by the Business Owner, the process will not move forward and the invoice won't be paid.**

## Related Docs and Templates

#### Documentation

* [Uploading Third Party Contracts to ContractWorks](/handbook/legal/vendor-contract-filing-process/)
* [Compliance](/handbook/legal/global-compliance/) - general information about compliance issues relevant to the company
* [Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information) - general information about each legal entity of the company
* [Trademark](/handbook/marketing/growth-marketing/brand-and-digital-design/brand-guidelines/#trademark) - information regarding the usage of GitLab's trademark
* [Authorization Matrix](/handbook/finance/authorization-matrix/) - the authority matrix for spending and binding the company and the process for signing legal documents

##### Contract Templates

- [Mutual Non-Disclosure Agreement](https://drive.google.com/file/d/1kQfvcnJ_G-ljZKmBnAFbphl-yFfF7W5U/view?usp=sharing)
- [Logo Authorization Template](https://drive.google.com/file/d/1Vtq3UHc8lMfIbVFJ3Mc-PZZjb6_CKAvm/view?usp=sharing)

## Procurement Main Objectives

Procurement operates with three main goals
1.  Cost Savings
2.  Centralize Spending
3.  Compliance