---
layout: handbook-page-toc
title: Coaching 
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction to coaching

On this page we are going to review the GitLab coaching framework and approach for people leaders and direct reports to apply during coaching conversations with their team. Use this page as a guide to starting and executing a coaching conversation. 

## What is coaching?

Coaching is about helping others help themselves. It is not about giving advice, instruction, or telling someone what to do. **Coaching is about focusing on the future and identifying where the coachee wants to be and what they want to achieve.** As a coach, your role is to clarify the pathway from the current state to the future. Coaches do this by enabling the coachee to make informed choices based on deeper insight.

## Coaching at GitLab

Coaching conversations are fluid, dynamic acts of co-creation where the coach and the coachee are equal partners. The Gitlab coaching framework has both a coach and coachee side, and each side is a reflection of the other. Coaching is an important component of our [360 review process](https://about.gitlab.com/handbook/people-group/360-feedback/) and for [guidance on feedback](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/). Coaching can occur during 
[one-on-one meetings](https://about.gitlab.com/handbook/leadership/1-1/) or at any time. It is an important component of [career development](https://about.gitlab.com/handbook/people-group/leadership-toolkit/career-development-conversations/), [leading teams](https://about.gitlab.com/handbook/people-group/leadership-toolkit/leading-teams-through-adversity-or-challenging-times/), [building an inclusive culture](https://about.gitlab.com/company/culture/inclusion/building-diversity-and-inclusion/#tips-for-managers), [mentoring](https://about.gitlab.com/handbook/engineering/career-development/mentoring/), and much more. 

## Selecting a coach

There are various ways to find a coach at GitLab:
* [Modern Health](https://about.gitlab.com/handbook/total-rewards/benefits/modern-health/), our Employee Assistance Program provides professional coaching to support development areas on the feedback you receive. 
* Consider a senior to you in your larger team or someone outside of your team that have inspired you on the specific areas you are working and developing on. 
* Your current Manager can be a coach that steer you towards future goals. Please ensure that your Manager has capacity to take you on in a coaching capacity. However, coaching can also be a type of interaction you have with your Manager. 

## How coaches coach

Coaches help team members by focusing their attention on the future while recognizing their unique strengths and areas for development. A coach is there to help the coachee tap into their potential and to reflect and learn by identifying desired outcomes to acheive future goals. 

Key attributes of a coach include some of the following: 

*  Ask powerful questions to deepen learning or insight.
*  Help your coachee recognize their own strengths and untap their potential.
*  Encourage your coachee to move to action on their choices.
*  Hold your coachee accountable for the actions they have committed to.
*  Fully focusing on the other person.
*  Asking questions and listening deeply while offering support.

### Different hats for different conversations

When we think about coaching as a mode of conversation, you may also be thinking about all the other modes of conversation you might use as a [leader](https://about.gitlab.com/handbook/leadership/) in multiple types of roles. You may be a team lead running an engineering program. You may be managing [one of our ERG's](https://about.gitlab.com/company/culture/inclusion/erg-guide/). You may [be a mentor](https://about.gitlab.com/handbook/engineering/career-development/mentoring/) or [a buddy](https://about.gitlab.com/handbook/people-group/general-onboarding/onboarding-buddies/). You are also most likely a direct report of someone else. Whatever it is you need to flex your leadership style and adopt different approaches to engage in meaningful conversations. You can think of this as wearing "different hats."

You may wear multiple hats in any given day: 

*  **Boss Hat** - Being directive and telling people what to do and how to do it.
*  **Teacher Hat** - Passing on your knowledge and expertise to grow someone else's skills.
*  **Mentor Hat** - Sharing advice and giving guidance from your own experience.
*  **Coach Hat** - Asking lots of quesitons and listening deeply to help people reach their own solutions.

### GROW Model

The GROW Model is a four step method to holding coaching conversations with a coachee. You can apply it during coaching sessions to guide the coachee through future focused discussions. 

**G - Goals:** Identify the inspirational goal to drive success and keep energy and motivation high. 

**R - Reality:** Discuss the current situation and what barriers currently exist between and achievement of future goals

**O - Options:** Explore the options for moving forward

**W - Way Forward:** Agree on specific actions and timeframe to set accountablity for the coachee


## Essential coaching skills

Effective coaches use a defined set of skills to enable coaching coversations. Those skills include: 

<details>
<summary>Questioning</summary>

  Asking powerful, open ended questions is a core skill. Coaching is about coming from a place of asking, not telling, while empowering team members to create their own solutions. Coaching works to "pull out" insight from an individual. 

  <p>
    <b>How to structure questions</b>
    <ul>
    <li>Ask open ended questions. The best questions start with "what" or "how." As you get to questions around action, "who" and "when" can also be helpful.</li>
    <li>One question at a time. Focus attention on one question that focuses on one element of the conversation.</li>
    <li>Keep the questions brief.</li>
    <li>Stay with the coachee's agenda. Pick a question that reflects where they are, not where you think they should be. </li>
    <li>Ask for permission. If you want to challenge the person, or give them a suggestion or insight they may not otherwise have, ask them for permission. </li>
  </ul>
  </p>

  <p>
    <b>Questioning - What to avoid</b>
    <ul>
    <li>Avoid closed questions - they can sometimes close down thinking. For example, avoid questions that use the following: "Does that...do you...have you...is there?"</li>
    <li>Avoid why questions - these can cause the person to feel defensive and may close down their learning from the coach.</li>
    <li>Avoid leading questions, statements, or opinions in the form of questions - as these point the coachee to a place you think they should go, when they will be more committed to action if they come to their own conclusions.</li>
    <li>Avoid stale questions - avoid asking questions that you thought of at the start of the session, but which may not be relevant anymore (If you find yourself waiting for the right time to ask a question, chances are you may not be listening anymore).</li>
  </ul>
  </p>

</details>

<details>
<summary>Listening</summary>

  Listening is a crtical skill because it enables openness and curiosity. It also signals to the coachee that you are being fully present. To ask great questions, you need to be fully listening.

  <p>
    <b>Listening like a coach</b>
    <ul>
    <li>Be present physically, mentally, emotionally (close down screens and give your coachee your undivided attention).</li>
    <li> Focus on the other person.</li>
    <li>Listen with curiosity and without judgment.</li>
    <li>Allow time for silence when the coachee is processing your question.</li>
    <li>Pay attention to body language through video chat: Make eye contact in the camera, smile, make gestures such as nodding your head to show you are present and lean into the discussion.</li>
  </ul>
  </p>

  <p>
    <b>What to listen for</b>
    <ul>
    <li>The tone of voice</li>
    <li>The pace of speech and shifts in energy</li>
    <li>Virtual body language</li>
    <li>Choice of words</li>
    <li>Overarching themes or what the coachee continues to come back too</li>
    <li>Underlying beliefs and assumptions</li>
    <li>Blind spots</li>
    <li>Personal values and what matters most to them</li>
    <li>The pauses and the silence</li>
  </ul>
  </p>
  
  <p>
    <b>What else to do when you are listening</b>
    <ul>
    <li>Provide focus and try to clarify for the coachee.</li>
    <li>Without judgement, offer an obseration, hold up the mirror to show what you are hearing and to deepen insight for the other person.</li>
    <li> Without judgement, confront or challenge what might be a limiting belief to open up new opportunities for the coachee. </li>
  </ul>
  </p>

</details>

<details>
<summary>Encouragement</summary>

  Encourage and show enthusiasm by identifying strengths in the coachee to build trust. Be open and ground yourself in individual team member strengths. Reflect back on what you noticed and determine the impact on the coachee to see that the [feedback](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/) has landed.

  <p>
    <b>When to Encourage</b>
    <ul>
      <li>At the beginning of a coaching conversation, acknowledge progress made or an accomplishment achieved.</li>
      <li>During a conversation, acknowledge when the coachee makes a connection or gains new insight.</li>
      <li>During brainstorming, acknowledge when somone has sretched beyond their comfort zone.</li>
      <li> During planning for action, acknowledge someone's commitment to change.</li>
    </ul>
    </p>

  <p>
    <b>Strategies to practice encouraging</b>
    <ul>
    <li>Acknowledging: Focuses on the coachee - who they are as a person and what they have done in their life, their inner character. Make the coachee feel unique and that you recognize what sets them apart.</li>
    <li>Appreciating: Focuses on the positive impact and contribution of the deed to others.</li>
    <li>Praising: Focuses on the deed, what people do - <a href="https://about.gitlab.com/handbook/values/#results">results</a>, <a href="https://about.gitlab.com/handbook/values/#transparency">transparency</a>, <a href="https://about.gitlab.com/handbook/values/#efficiency">efficiency</a>, <a href="https://about.gitlab.com/company/culture/inclusion/">inclusion</a>, and performance. </li>
  </ul>
  </p>

</details>

<details>
<summary>Challenge</summary>

  Challenging your coachee helps them raise the bar, stretch, and grow. Challenge the coachee to get out of their comfort zone, raise the bar, and play a bigger role. Your role as a coach is to hold a bigger picture of what's possible.
  
  A great challenge jolts someone into examining the limits of their comfort zone and move beyond them.

  <p>
    <b> When to challenge</b>
    <ul>
      <li>When your coachee has been sitting in their comfort zone or they've been playing it safe for too long.</li>
      <li>A coachee who has been moving fast and thrives on challenges.</li>
      <li>To increase the coachee's self-confidence and to increase their perception of what is possible.</li>
  </ul>
  </p>

</details>

<details>
<summary>Be Present</summary>

  Be in the moment, focus on the other person, free of judgement. Give your coachee your undivided attention. Close out other programs during a virtual coaching session. Be open and curious by identifying what's working and what's not. Be truly present physcially, mentally, and emotionally. 

<p>
  <b> Strategies on how to be present</b>
  <ul>
    <li>Make gestures such as nodding your head to show you are present.</li>
    <li>Empty your own mind of all preoccupations that are diverting your own focus, attention, and energy.</li>
    <li>Ground yourself in an open, curious, and appreciative mindset.</li>
    <li>Look for something to appreciate about your coachee, your self, and the coaching process.</li>
    <li> Ground yourself in your intentions for the coaching conversation.</li>
  </ul>
  </p>
  
</details>

## Attributes of a coachee

As a coach uses their core coaching skills, a coachee can access their own set of skills and action to get the most out of the coaching conversation through the following: 

*  **Be Present**: Just as the coach is being present, a coachee needs to be present in the conversation, attentive, open to possibilities, and fully engaged.
* **Reflect**: A coachee is invited to reflect, wonder, ponder, and contemplate through a coaching conversation through powerful questions. 
*  **Visualize**: The coachee can harness the power of their imagination and paint a picture in their mind of the desired future outcomes to enhance their chances of success. 
*  **Learn**: Coaching conversations revolve around learning by discovering new perpsectivies by coming to a new understanding or realization. 
*  **Transform**: Change is a core attribute of coaching. Sometimes the transformation may be small, when a coachee has increased clarity on appropriate action to move forward. At times the transformation may be more profound, as a coachee is able to shift their mindset in a way that there's no going back.

## Planning for action

The planning for action phase is about enabling the coachee to make a plan with action steps that support their goals. 

**How to plan for action:**

*  The coachee commits to and is accountable for actions that they design for themselves. 
*  The coach may brainstorm, probe, request, challenge, or raise the bar but the coachee is ultimately responsible for designing the action steps. 
*  The coaches actions for the coachee are in service of the coachee's agenda.
*  Coaches may not neceassarily be attached to the results a coachee achieves.
*  Actions are anchored in what is important to the coachee, what changes they want to acheive. Coaches should hold their coachee accountable for performance on action steps and their overall change agenda. 

When you are planning for action while wearing your coaching hat, you are not evaluating or judging or driving your own agenda. The coachee should ultimately decide the action steps, committing to their own path forward. As a coach, you are curious and non-judgemental while living up to [GitLab values](https://about.gitlab.com/handbook/values/). 

### Sample planning for action questions: 

*  What might be three next steps that you can take in order to....?
*  What are you going to do? When are going to do it? How will you know?
*  What do you need/want to find more about...?
*  What kind of support do you need? From whom?
*  My request is that this week you... Will that work for you?
*  I challenge you to...What do you think?
*  In what way do you want me to follow up with you? 

## Ending the coaching conversation

When a coaching session is completed, it is essential to review the goal of the conversation with the outcome by asking: 

*  So you wanted to talk about _____, in what way has this conversation been useful for you?
*  What do you want to take away from this conversation?

The coach might also want to end the conversation with a few statements about what this session has reminded them of and what they truly appreciate in the coachee. Coaching is about empowering. They are helping team members to increase and balance their self-confidence and self-worth:

*  What I truly appreciate from our conversation was...
*  What I admire you for is...
*  What you have helped me realize, learn or reminded me about is...


## Additional coaching skills

* Offering a big picture perspective
* Use of metaphors
* Brainstorming
* Creating a compelling picture of what could be 
* Championing
* Challenging
* Identifying underlying limiting beliefs, assumptions, and mindset

## Coaching Manager Competency

In an all-remote organization, coaching is a critical skill for managers to develop and improve upon as they progress in their careers. Coaching helps to facilitate the career development of team members through regular coaching conversations. Coaching helps team members change behavior, improve performance, and sustain commitment through encouragement, support, collaborative problem-solving, goal setting, and [feedback](https://about.gitlab.com/handbook/people-group/guidance-on-feedback/). 

**Skills and behavior of the [coaching competency](https://about.gitlab.com/handbook/competencies/) for managers: **

- Facilitates job performance growth of team members by providing regular coaching sessions
- Understands team members long-term career goals and acts as a mentor and guide to achieving them. 
- Conducts development and career planning dialogues with team members continuously
- Reflects on their leadership style and impact on the team and team situation
- Seeks new coaching approaches and techniques and exemplifies what it means to continuously develop their skills
- Delivers effective strategies for dealing with cases of [underperformance](https://about.gitlab.com/handbook/underperformance/) and instills that in other leaders across the organization

## Additional coaching resources

* [The Leader as Coach - Harvard Business Review Article](https://hbr.org/2019/11/the-leader-as-coach)
* [What Can Coaches Do for You? - Harvard Business Review Article](https://hbr.org/2009/01/what-can-coaches-do-for-you)
* [Four Tips for Coaching Remote Employees](https://blog.insideoutdev.com/4-tips-for-coaching-remote-employees)
