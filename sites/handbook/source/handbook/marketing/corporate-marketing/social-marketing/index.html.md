---
layout: handbook-page-toc
title: "Social Marketing Handbook"
description: Strategies, Workflows, and Emojis for Social Media at GitLab
twitter_image: "/images/opengraph/social-handbook.png"
twitter_image_alt: "GitLab's Social Media Handbook branded image"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Social Marketing Handbook
*The social marketing team is responsible for the stewardship of the GitLab brand social channels. We’re accountable for the organic editorial calendar and work with partners across the community advocacy, digital marketing, talent brand, and other teams to orchestrate the social media landscape for GitLab.*

**This page is the single source of truth for all strategies and positioning for the GitLab brand social channels.** If you're looking for administrative details (how-to open an issue, using labels, how to add frontmatter to your page for social sharing, etc.) [head to our Social Media Administration Handbook page](/handbook/marketing/corporate-marketing/social-marketing/admin/). 

## Video introduction to Social Media at GitLab

<figure class="video_container">
<iframe src="https://www.youtube.com/embed/XZQ2Egrk7tI" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

## Primary Social Channels, Audiences, and Calendaring <a name="primary social channels audiences and calendaring"></a>
GitLab Branded Social Channels include our company [Twitter](https://twitter.com/gitlab), [LinkedIn](https://www.linkedin.com/company/gitlab-com), and [Facebook](https://www.facebook.com/gitlab) which are managed in a more traditional manner. A potential [Instagram](https://www.instagram.com/gitlab) channel will take more of an editorial look at remote life and work at GitLab, this is currently on hold. A little less “marketing-y” and more about building community. And lastly, our [YouTube](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg) channel is a testament to the core of GitLab where everyone can contribute.

### Twitter <a name="twitter"></a>

##### ✍️Channel Use
Because the wider developer community utilizes Twitter as a platform to ask support questions, make feature suggestions, and share industry ideas, GitLab prioritizes this channel. We post to this platform first, and also more frequently. `@gitlab` broadcasts a variety of content including our blog, releases, important announcements, major integrated marketing campaigns, live event coverage, and All Remote content.

In addition to sharing curated content, the `@gitlab` Twitter handle participates in community management activities (likes, comments, and shares). Through this practice of engagement, we attempt to create a digital space for dev, friends and thought leadership. 

Tweet threads should and are consistently used on the `@gitlab` Twitter handle. Because of GitLab’s value of transparency, utilizing Twitter threads gives our brand the opportunity to provide all important details without the character limit. 

We ultimately use Twitter as an opportunity to provide our community with the most up to date information about gitlab.com, our brand, and also DevOps. We do that with a friendly voice because all devs are friends; no matter the tool they use. And we use emojis whenever we can. The overuse of emojis allows us to participate in a universally used language. GitLab believes that everyone can contribute. That’s why being better communicators is crucial for our audience- no message should go misunderstood. 

##### 🎯 Audience targets
From the United States to Japan, our Twitter audience is a global one. 66% of GitLab’s Twitter audience is between the ages of 18-24, and 16% are between the ages of 25-34. There is an equal breakdown of mobile device use between iOS and Android (both 50%). The nature of a developer’s job however still has 68% accessing Twitter from the web, not a mobile device. Twitter does not have the capability of breaking demographics down by title, however, our particular audience participates in the following conversations: DevOps, code, git, GitHub, remote and remote work, CI, and CD.

##### 📅 Calendaring, Cadence, and Volume
How often GitLab should post depends on the platform and kind of content we are sharing. That said, Twitter does not operate like our other GitLab Social branded Channels. There are no strict frequency limitations on Twitter and we aim to post at least 3 times a day if not more. Content shared should prioritize the blog, releases, the hackathon, culture, the handbook, and All Remote content. These specific pages make up the majority of traffic driven from Twitter posts shared between October 2019-March 2020.
*  If there are 4+ posts they should have at least 2 hours in between. This rule does NOT apply if there are time-sensitive posts including releases, security releases, patch releases,  live event coverage, etc.
*  Post every day, and utilize Sprout’s optimal time feature as we have many followers located across all time zones. 
*  But do not always select the 3-5 stars optimal time. If there are already 4 posts going out that day select the 1-2 stars optimal time. This will help keep the calendar properly spaced out and ensure we have content publishing throughout the day. 
*  In Twitter Media Studio, create Twitter cards when the meta description is irrelevant, not unique, or creating repetition. Media used must be delivered by the Design or Social Media Teams. 
*  Retweet daily mentions that are authored by Team Members, our community, our partners or are press opportunities. Retweets are not limited by frequency meaning there’s no limit at all! But there is an organized way which we do determine if something gets retweeted to the GitLab Branded Twitter. And it is up to the social media team’s discretion if a tweet gets shared.
    - **Milestones**: RT freely, or RT with a comment, emoji, or GIF when users are celebrating. These milestones can include but are not limited to: #MyFirstMRMerged, a number of MR’s, contributions, etc. 
    - **Partner mentions**: Use the RT with comment feature and tag the socially active partner with their proper handle. Provide a short POV (point of view) on the partnership. Example: *@PartnerHandle + GitLab = purple heart*
    - **Press/third party mentions**: RT with comment *always*. Share gratitude with a clear CTA (call to action).

### Facebook
##### ✍️Channel Use

GitLab has a smaller audience on Facebook so its priority does fall below Twitter and LinkedIn. It is still important to maintain our presence. That's why we aim to post to this platform no more than 2 posts per day (if that).  GitLab should prioritize posts around All Remote, third party content, and our culture (or Life at GitLab). We do have an obligation to always post important announcements, releases, and specific campaigns or events. 

##### 🎯 Audience targets
From the United States to India, our Facebook audience is a global one. Although our Facebook audience does see 15% of its fan base from the United States. And 51% of fans between the ages of 25-34 are the leading force of followers. Through the power of organic reach, our content has a high potential to also reach users between the ages of 45-54.

Facebook does not have the capability of breaking demographics down by title, however our particular audience participates in the following conversations: security, code, remote, CI, cloud, and CD. 

##### 📅 Calendaring, Cadence, and Volume
How often GitLab should post depends on the platform and kind of content we are sharing. That said, Facebook does not operate like our other GitLab Social branded Channels. It is utilized less frequently and should prioritize culture culture, handbook, jobs, homepage, and All Remote content. These specific pages make up the majority of traffic driven from Facebook posts shared between October 2019-March 2020. 
* There is no immediate need to post every single day on Facebook. Though, the social team will post once a day when content is available.
* There should not be more than 2 posts per day. And 2 posts per day on Facebook is used only during critical times. Critical times would mean there are timely updates, announcements, and or news that requires more than 2 posts on the GitLab Branded Facebook on a given day.

### LinkedIn <a name="linkedin"></a>
##### ✍️ Channel Use

Our company page on LinkedIn shares a variety of content including third party mentions, releases, our blog, company culture, major integrated marketing campaigns, events (must include geotargeting if applicable), and All Remote content. The repost feature of LinkedIn is then used to broadcast company mentions (Example: partner tags GitLab in their post-we broadcast that to our page). Ultimately we use LinkedIn as an opportunity to expand in more detail on the content that we care about. How do we do that? By using a variety of formatted posts with more text, bullets, emojis, questions, quotes, and/or even stats. 

##### 🎯 Audience targets

From San Francisco to India and in between, our LinkedIn audience views our company page from across the world. Our visitors fall primarily under senior (43.23%) and entry level (30.03%). GitLab’s LinkedIn audience is also lacking the CXO, Owner, VP, Manager and Director level followers. So we are able to make the assumption that it is GitLab end users who follow us on LinkedIn. The following industries make up this audience: IT + Services, Computer Software, Telecommunications, Financial Services, Higher Education, and Program Development. 

##### 📅 Calendaring, Cadence, and Volume
How often GitLab should post depends on the platform and kind of content we are sharing. That said, LinkedIn does not operate like Twitter and follows a 2-3 posts per day limit with the exception of timely announcements that must take place. Content shared should prioritize the home page, culture, the blog, hackathons and Heroes, Remote Work content, and the handbook. These specific pages make up the majority of traffic driven from LinkedIn posts shared between October 2019-March 2020. 
*  Post everyday, no more than a couple times a day- posting any more will result in a drop in engagement and cause GitLab posts to be hidden in followers’ news feeds. 
*  Posts should have a few hours in between to avoid eating up our own impressions and engagements. 
*  Geotargeting through Sprout should **always** be used for events if applicable. For example, GitLab Connect in San Antonio, was limited by location factor=Texas. Using organic targeting like this allows us to provide followers relevant content. 

## Other Social Channels, Audiences, and Calendaring <a name="other social channels audiences and calendaring"></a>
### Instagram
##### ✍️Channel Use
In no way shape or form would our Instagram account mirror our other GitLab Branded Social Channels. In fact, it will take more of an editorial look, will be a little less “marketing-y” and be more about building community. We're rallying around a number of general topics. It does include remote work (All Remote), but more deeply about "Life at GitLab" (talent brand) and stories around living our values.  
For remote specific content we will try and steer clear of remote working stereotypes as a dominant theme. The data and Remote Work Report says that reasons and lifestyles for remote are different for everyone - so really, that's the remote story we want to tell.

Instagram will help the Social Media team tell big stories with small moments. For example, corporate produced events like Commit can use Instagram to provide live experiences and video content. 

##### 🎯 Audience targets
This is essentially being treated as a net new social channel since it had never been posted to. There is no current data available on our audience. However,  we intend for our Instagram content to be consumed by gitlabbers, prospective team members, gitlab.com users, and those interested in remote work. The GitLab Instagram is targeting the #LifeAtGitLab, #AllRemote, #RemoteLife, #OpenSource, #DevOps, and #FutureOfWork conversations. 

##### 📅 Calendaring, Cadence, and Volume

How often GitLab should post depends on the platform and kind of content we are sharing. That said, the Instagram Story feature will not have a frequency limitation and should be utilized as often as content is available. However, we will not over post Instagram wall posts. Instagram is a visual platform where GitLab can showcase the human side of our company and our brand. Each wall post on the GitLab Instagram account should tell its own story- and be a stand alone post. At initial launch, we will aim to post once a month as we gear up content to post 1X per week or biweekly

### Medium <a name="medium"></a>
GitLab has a [Medium publication](https://medium.com/gitlab-magazine), and all GitLab team-members may be added as writers! To be added as a writer to the publication, [import](https://help.medium.com/hc/en-us/articles/214550207-Import-post) a blog post that you authored on about.gitlab.com/blog to your personal Medium account, and submit it to the GitLab publication (by hitting `edit` -> `submit to publication` -> `GitLab Magazine`). the Social Marketing Manager will approve you as a writer and help finalize the post before publishing. 

If you submit original content (i.e., not originally published somewhere else) to the publication for review, she may edit and publish your post. We want to highlight writers wherever possible, so we highly encourage you to import posts to your personal Medium.

**Content/Execution**
* Brand and thought leadership-focused
* Favorite articles about GitLab
* Aim to post one article to Medium every two weeks
* Cross-post our original thought-leadership posts to Medium, linking back to blog
  - Use the [import tool](https://help.medium.com/hc/en-us/articles/214550207-Import-post); do not copy and paste.

### YouTube <a name="youtube"></a>

**Content/Execution**
- Developer/community-focused
- Live events are more tech focused
  - Group Conversations, pick your brain meetings, demos, brainstorms, kickoffs

## 🎟 Social Event Strategies
Coverage for events across GitLab brand social channels varies and is dependent on investment level, on-the-ground activations, and core messaging. Social coverage for an event is not guaranteed.

Sponsoring or creating an event is a way to extend GitLab to new audiences, so promoting these events to our existing audiences on brand social channels does not work in the same direction. In order for an event to be social-able, there needs to be something that social audiences can gain. In most cases, promoting events on organic brand social channels is a way to tell our audiences that we are out in the world, doing big things, and are taken seriously. This means that simply because GitLab is a sponsor of an event does not mean that we will promote the event on organic brand social channels. 

It's critical to fill out a **social-event-request** issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues) as soon as there is enough information available to request organic social coverage. 

Once the social event request issue template is filled out, a member of the social team will determine what level of social coverage, if any, is appropriate. Levels of coverage and what each includes are listed below. Please note, that the descriptors of each tier are suggestions, and a specific event may be identified for a different tier for a number of factors. While most events fall into similar categories, specific coverage may need to be unique to the event.

#### Social Event Tiers and What They May Include

| Tier | Example Event |Equivalent | What's Included |Action|Example |
| ------ | ------ | ------ |------ |------ |------ |
| `1` | Any Integrated Corporate Marketing Event|On-Site Support, Channel Takeovers |  Everything in Tiers 2 and 4. Includes Tier 3 if appropriate. On-Site/At Event Support from a member of the social team. Social Channel "takeovers", e.g. creative focused on the event. |Social Team: Use existing issue to plan. Requester: Be sure to link event campaign epic and other comms/PR issues. It's critical for social + PR to be linked for Tier 1 events. If creative is necessary, open a **design-request-general** issue. |Consider Commit, AWS re:Invent, and others |
| `2` | Any event with a GitLab speaker, presentation or interactive element - *must be more than booth or attendance only* |Broadcasted via Posts on GitLab Brand Channels (Pre, During, Post) | Everything in Tier 4; Includes Tier 3 if appropriate. Organic social posts including custom copy. May include: scheduled posts in advance of the event, live coverage during, and/or post-event wrap-ups. | Social Team: Use existing issue to plan posts. Requester: You may need to include more info. If creative is necessary, open a **design-request-general** issue. |[Ex 1](https://twitter.com/gitlab/status/1228384465891790848?s=20) |
| `3` | Team Member Enablement for any event, when the event team makes this request |Advocacy, Team Members Sharing on Their Channels | Everything in Tier 4; Social team aiding to create copy for team members to share on their own social channels. *This tier is optional, only if the requester wants assistance with advocacy.* | Social Team: Open a **social-team-advocacy** issue in the [corporate marketing project](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues). If creative is necessary, open a **design-request-general** issue.|[Ex 1](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/1437) | 
| `4` | Booth or Attendance Only |Engagement Only | Ad-hoc retweets/engagement with social posts from other users and brands. No outbound broadcasts from brand social channels. The majority of events will fall under Tier 4. |Social Team: Set calendar reminders for event day(s) and open specific columns in Tweetdeck to support. Alert the advocates of live coverage as this impacts Zendesk activity. |[Ex 1](https://twitter.com/gitlab/status/1228340156769411072?s=20)|

#### Field Events + Social Media
Field events present a major challenge and a major opportunity across social channels. While we can geo-target our organic (existing followers) audience on some channels, the number of people represented in this audience may not warrant a social post on GitLab brand social channels.

The single best way to secure social support for field events is to request paid social advertising with the Digital Marketing Programs team. This allows for appropriate geo-targeting on any social channel. This does not need to be a major investment.

You can open a paid social advertising request by [opening a new issue in the Digital Marketing Programs Project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new#) and choosing the **mktg-promotion-template**. The core social team is not the DRI for paid social, however, we can assist you with questions or details along the way.

#### Booth Location-Only Requests for Social Media Coverage
If your only request is 
> "We have a booth at this event and we want people to know where to find us."

Unless the event in question is in an integrated corporate marketing campaign, there are only two appropriate ways to tell social audiences that GitLab has a booth. 
1.  **GitLab Team Members use their personal/professional social channels to share with their audiences.** This could be considered a `Tier 3` or `Tier 4` event, where the social team can aid the event team, however, an issue is required in advance of the event. 
1. **Paid social advertising presents the necessary opportunity** - geo-targeting users in a defined region to tell them where GitLab has a booth.

This means that booth location-only requests are not fulfillable with global organic social media posts.

### FAQ on Social Event Coverage
##### I don't understand why we can't get the support we've asked for.
Organic social media has a number of priorities to mix for success. First, our global audiences need to gain value from our posts or we need to extend GitLab's brand somehow. Every event isn't relevant from a global scale, and in most cases, doesn't provide a brand value. Furthermore, agreeing to publish posts that would not perform well against our performance indicators is a self-sabotage for the social team. It's not in our best interest, as a team or as a company, to publish posts that won't perform well or that do not extend the GitLab brand.

##### I'm worried that my event won't get enough attention on social media.
Organic brand social channels have to maximize fit, content, and distribution in order to meet performance metrics. This yields more attention to the right content from the right kind of audience. An integrated plan for event coverage should never rely solely on social media. If you are deeply concerned about marketing your event, please consider an alternative to organic social, including paid social advertising.

##### When should I ask for paid social advertising for an event?
Generally speaking, there are two points to hit for paid social advertising to work. If the event **has a GitLab-owned landing page** and **needs to be geo-targeted to the right regions** then your event may be a good candidate for paid social advertising. *E.g. Our roadshow is coming to Chicago and we have a landing page for registrations.*

You can open a paid social advertising request by [opening a new issue in the Digital Marketing Programs Project](https://gitlab.com/gitlab-com/marketing/digital-marketing-programs/issues/new#) and choosing the **mktg-promotion-template**. The core social team is not the DRI for paid social, however, we can assist you with questions or details along the way.

##### Help! My event is in *insert short period of time*. Can the social team post?
If your event is in an appropriate tier for broadcasted posts on GitLab brand social channels, the social team will work with you to get these posts scheduled. Please keep in mind that the shorter the amount of time to prepare, the less likely there will be a comprehensive plan including the best copy, creative, and other elements. It's still best to get request issues open as soon as possible.

If last-minute requests become a frequent occurrence or there are other specifics preventing our cooperation, the social team reserves the right to decline the request.

## Social Media's place in an integrated marketing and communications strategy
The social team does not create our own core messaging for blogs or campaigns. We take the approved messaging (whether it's in an issue, a sheet, or from the blog itself) and "socialize" it for our GitLab brand social channels. This is a critical step in making our content genuine, as it's best to take guidance approved upstream (content marketing, MPM, communications, etc.) to best align with our messages. Because of this, it's important to bring the social team into a campaign prior to being activated. It's also critical for our integrated teams to confirm that key messaging has been reviewed, if not approved, by stakeholders for the topic or practice we're promoting.

### Approval of copy
We understand that while social media posts being published is usually the last step in an integrated marketing or communications process, it's usually the first time that all eyes are able to see what came out of the work. Because of this, there are times that stakeholders might not see what messaging or creative made it through the social media creation process. To avoid this situation, we've adapted to a more formal approval process.

During the social media coverage issue request process, the social team adds copy suggestions to the issue for approval. For many integrated campaigns as well as times that just feel best to have an additiona layer of approval, we will request a formal approval prior to scheduling social posts in Sprout. This process does not exist for retweeting/sharing with comment, social-first activations, or blog updates (blogs are vetted through an editorial process that presents a company-approved blog to use copy as desired). 

## Social-First content and campagins
The social media team will often create our own content and mini-campaigns for the sake of having fun and meeting our own objectives. This is also a critical part of a successful content strategy. If our social-first content is to align with a larger topic or talking point from GitLab, we will be sure to run our messaging by the DRI on the correct team. 

## Various Other Details
### Sharing 3rd-party events (onsite or virtual) on organic social where a team member is speaking
The purpose of GitLab team members participating in 3rd-party events is to bring GitLab's message to the 3rd-party's audience and to gain more community. Therefore, it's contradictory for GitLab brand social channels to promote a team member speaker for a 3rd-party event. Periodically, if the speaking engagement is part of a critical campaign or would resonate with the zietgiest of the moment, we may tweet once for folks to register for the event. However, if you're the speaker, we encourage you to post to your own social channels so that your network can join the event. Share your post with the #social_media Slack channel and we'll like, comment, and maybe share your post!

### Sharing blog posts
New blogs are linked in the #content-updates channel, and then our team simply uses that as a notification that the blog is ready to schedule and share on social. Sometimes when it’s connected to a campaign or is particularly important, we’ll also get a ping about it in a thread from the original slack message. 
 
If the blog is connected to an integrated campaign, it will be picked up in the social issues and epics for that campaign. If it is a part of a press release/announcement, we'll also be tagged in related issues through the PR workflow.

### Sharing "Unfiltered" blog posts or videos
GitLab Team Members are encouraged to write "unfiltered" posts, or blogs that do not go through an editorial process. This is an efficent way for folks from across the company to share ideas, hacks, stories, and more. However, because these blogs do not go through the traditional editorial process, we cannot promote them on our GitLab brand social channels. The editorial process is viewed as the bar to pass through in order for the company to support the writings we promote. The best way forward is to [follow the plan as laid out in the Unfiltered Handbook](https://about.gitlab.com/handbook/marketing/blog/unfiltered/#featured-posts). If you publish an Unfiltered post that you think could be a good fit for either the Editorial and PR teams, feel free to share it in the `#content` Slack channel. The social team does not own the approval of shifting blogs from Unfiltered through the editorial process and therefore cannot assist beyond this guidance.
 
## Social during a crisis
### Going Dark
In times of crisis, it may be appropriate for the social team to pause all brand channel social posts. This is called "going dark", and is a regular part of evaluating whether or not company messaging is appropriate to share at any given time. It is considered good practice to minimize the digital space brands occupy on social media during times of crisis. This can help to not distract social conversation but can also reduce the probability of being accidentally caught up in conversation unintentionally, sounding tone-deaf, or otherwise coming across as insensitive. 

When this occurs, time-sensitive posts will be the first to be rescheduled. In the event time-sensitive posts could not be rescheduled, the social team will do what we can to update our stakeholders on what posts won't be published. Non-time sensitive posts will be moved to Sprout drafts, which can then be rescheduled at a later time.  

Going dark could have a negative impact on social metrics, depending on the severity and length of time we're dark. Going dark could also negatively impact specific CTAs if we're relying on organic social to perform. These considerations are a part of making the decision and will be communicated as often as appropriate.
 
### Rewriting copy or switching assets
It may be necessary to update copy or assets depending on cultural impact during a crisis. The social team reserves the agency to do this as we see necessary, however, we will communicate the changes to our stakeholders if we'd consider the changes to be major.

### Default social channel bios and links
Sometimes bios and links will change on select social channels, depending on corporate level campaigns. This section keeps our default bios in a public place to restore when those campaigns are over. The links here are also UTM links, ensuring that we can better measure visits to our site from link in bio clicks. 

| Channel   | Bio                                                                                                                                                                                | Link                                                                            | When does the bio change?         |
|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|---------------------------------------------------------------------------------|-----------------------------------|
| Twitter   | GitLab is a complete DevOps platform, delivered as a single application. Everyone can contribute. Follow @GitLabStatus for service information.                                    | https://about.gitlab.com/?utm_medium=social&utm_source=twitter&utm_content=bio  | Corporate Level Campaigns         |
| Instagram | GitLab is a complete #DevOps platform, delivered as a single application. An #AllRemote company in 60+ countries 🌎 Learn about #LifeAtGitLab ⬇️                                    | https://bit.ly/2vkcxI0                                                          | Corporate Level Campaigns         |
| Facebook  | GitLab is a complete DevOps platform, delivered as a single application. There are 100,000 organizations using GitLab and we have an active community of 2,200+ code contributors. | https://bit.ly/2vVJDP8                                                          | Rarely                            |
| LinkedIn  | GitLab is a complete DevOps platform, delivered as a single application. We believe that everyone can contribute!                                                                  | https://about.gitlab.com/?utm_medium=social&utm_source=linkedin&utm_content=bio | Sometimes, talent brand campaigns |

### Responding as the company on LinkedIn for any post
Because of limited API access, it's challenging to engage and respond with posts on LinkedIn that do not directly @mention the company page. However, you can use the following method.

Every specific LinkedIn post from a page or a person has its own URL, [like this one](https://www.linkedin.com/posts/wilspillane_open-source-social-media-strategies-from-activity-6674298269985767424-4SEx/). At the end of the URL, there is a string of characters at the end, like `6674298269985767424-4SEx/` from the example above. If you add the following to the end of the post URL when you are an admin of the GitLab LinkedIn page, you'll be able to engage and comment as GitLab.

`?actorCompanyId=5101804`

The URL example from earlier is\
`https://www.linkedin.com/posts/wilspillane_open-source-social-media-strategies-from-activity-6674298269985767424-4SEx/`.

Adding the `actorCompanyId` to the url would look like\
`https://www.linkedin.com/posts/wilspillane_open-source-social-media-strategies-from-activity-6674298269985767424-4SEx/?actorCompanyId=5101804`

This URL would allow for GitLab to comment and engage.

<!-- Social Marketing Handbook - ref https://gitlab.com/gitlab-com/marketing/issues/524 -->
<!-- identifiers, in alphabetical order -->

[blog]: /blog/
[description-tag]: http://www.wordstream.com/meta-tags
[facebook debugger]: https://developers.facebook.com/tools/debug/
[first post]: /2016/07/19/markdown-kramdown-tips-and-tricks/
[OG]: https://developers.facebook.com/docs/sharing/webmasters#markup
[second post]: /2016/07/20/gitlab-is-open-core-github-is-closed-source/
[twitter card validator]: https://cards-dev.twitter.com/validator
[twitter cards]: https://dev.twitter.com/cards/overview
[twitter-card-comp]: /images/handbook/marketing/twitter-card-complete.jpg
[twitter-card-incomp]: /images/handbook/marketing/twitter-card-incomplete.jpg
[www-gitlab-com]: https://gitlab.com/gitlab-com/www-gitlab-com