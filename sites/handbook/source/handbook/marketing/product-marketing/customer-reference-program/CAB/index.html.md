---
layout: markdown_page
title: "Customer Advisory Boards"
---
### GitLab Customer Advisory Boards
**Purpose:** To help foster DevOps transformation and adoption we are establishing a Customer Advisory Board, where we focus on sharing DevOps best practices and lessons learned with each other. We believe that transparency and sharing  is a key way to help encourage the success of DevOps transformations.  The GitLab customer advisory board is intended to be home to learning and collaboration so we can all experience success through DevOps transformation.

**Members** Executives/Champions for DevOps within their organizations

**Frequency:**  We meet virtually the first Wednesday of the month at 11 a.m. Eastern

**Membership:** As of July 2020, 31 customers, GitLab product team, UX team, Customer Success leadership

**Meeting Recordings** Meetings are recorded for internal and member use. Members can seek these recordings by emailing CAB@gitlab.com

**Recurring Content** We will frequently ask for GitLab Product Managers to join to [present the vision for their assigned DevOps stage](/product/#customer-advisory-board-meetings).
When doing so we will distribute a recorded video of their [stage direction](/handbook/product/product-management/process/#section-and-stage-direction) in advance.

**Recorded CAB meetings** Due to the private nature of the Customer Advisory Board, video recordings and presentations are not linked here. If you are a GitLab employee, search for "Customer Advisory Board" in the google drive to view all assets. 

### Future CAB meeting schedule
View the [schedule for upcoming CAB meetings](https://gitlab.com/groups/gitlab-com/marketing/-/epics/984). If you see availablity to present at a future meeting, add a comment to the desired meeting to get added to the agenda. 

### CAB-Shared Slack channel
A number of our customers continue the conversation with each other and with our product team in the CAB-Shared Slack channel. Because our customers are active in this channel, please be aware of content shared. 



### 2020 Meeting Schedule
***December 2, 2020***

***November 4, 2020***

***October 7, 2020***

***September 2, 2020***

***August 5, 2020***



***July 1, 2020***
- 14 customers in attendance
- Secure and Defend update: Acquisition discussion
- Portfolio Management update and roadmap discussion

***June 3, 2020***
- 18 customers in attendance
- Design Management capabilities discussion
- Conversation around Remote work challenges and successes

***May 6, 2020***
 - 12 customers in attendance
 - Create: Editor update
 - Customer sharing - U.S. Telecom

 ***April 21, 2020***
 - 19 customers in attendance
 - Half-day virtual event 

***March 4, 2020***
- 10 customers in attendance
- Analytics overview and update
- Runners permissions set discussion

***February 5, 2020***
- 10 customers in attendance
- Infrastructure as code review
- Customer sharing - media organization



### 2019 Meeting Schedule

**GitLab In-Person CAB Meeting - October 15 & 16, 2019***
- Space Needle
- Seattle, Washington

***GitLab Product Roadmap Review - September 4, 2019***
- Member introductions
- Stage discussion
- Customer Sharing

*** GitLab Product Roadmap Review - August 7, 2019***
- Member introductions
- Stage discussion
- Customer Sharing

***GitLab Product Roadmap review - July 10, 2019***
- Member introductions -
- Stage discussion
- Customer Sharing

***GitLab Verify direction update - June 5, 2019***
- Member introductions - 11 members in attendance
- Review of Verify direction
- Customer Sharing

***GitLab In-Person CAB Review -May 1, 2019
- Member introductions
- Review of In-Person Customer meeting
- Discussion around future of CAB and project to work on together

***In-Person CAB Meeting- April 18-19, 2019***
- Yankee Stadium
- Bronx, New York
- Number of attending customers - 17

***GitLab Configure review- March 6, 2019***
- Member introductions
- Customer Sharing
- Review of Configure

***GitLab Geo review-February 6, 2019***
- Member introductions
- Customer Sharing
- Review of Geo

***GitLab Manage review - January 9, 2019***
- Member introductions - 11 members in attendance
- Review and feedback around Manage
- Customer Sharing

###2018 Meetings

***GitLab Release review-December 5, 2018***
- Member introductions
- Review of Release
- Year-end check up and member discussion

***GitLab Create review-November 7, 2018***
- Member introductions
- Customer Sharing
- Review of Create

***GitLab Plan review-October 17, 2018***
- Member introductions- 8 members in attendance
- Review of Plan Roadmap
- Customer Sharing

***Kickoff Meeting-September 5, 2018***
- Member introductions-9 members in attendance
- Group goals  
- Top DevOps challenges


### GitLab Special Interest Group

***Purpose:*** We are forming Special Interest Groups to foster specific and focused discussions about how to apply DevOps practices and GitLab capabilities in specific domains such as planning, development,  CI/CD, security, etc. These Special Interest Groups will encourage sharing and collaboration of DevOps best practices and lessons learned between users and GitLab.

We believe that transparency and sharing is a key way to help us all learn and improve how we deliver for our customers. GitLab special interest groups are intended to be home to learning and collaboration.

***Members*** Technical utilizers and advocates of GitLab

***Frequency:***  We will try to meet virtually every 6 to 8 weeks

***Membership:*** Approximately 8-15 users, GitLab Product Manager

