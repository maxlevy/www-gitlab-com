---
layout: handbook-page-toc
title: Growth Marketing Handbook
---
## On this page
{:.no_toc .hidden-md .hidden-lg}
- TOC
{:toc .hidden-md .hidden-lg}

# Growth Marketing Overview
{:.no_toc}
---
## Goals

Growth Marketing is responsible for:
* Increasing GitLab.com website traffic 20% YoY on an ongoing basis
* Delivering Inbound MQLs
* Increasing Conversion of free GitLab.com users to paying customers

## Key Deliverables

* GitLab.com public-facing website [view issues board](https://gitlab.com/groups/gitlab-com/-/boards/1483370?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=mktg-website)
* GitLab [blog content](https://about.gitlab.com/blog/)
* GitLab organic discovery
* GitLab Newsletter

## Teams
Growth Marketing is comprised of these Marketing Teams. Visit their handbooks to learn more. 
* [Global Content](https://about.gitlab.com/handbook/marketing/growth-marketing/content/#what-does-the-global-content-team-do)
* [Brand and Digital Design](https://about.gitlab.com/handbook/marketing/growth-marketing/brand-and-digital-design/) 
* [Marketing Website](https://about.gitlab.com/handbook/marketing/website/)
* [Inbound Marketing](/handbook/marketing/growth-marketing/inbound-marketing/)

## Stable Counterparts
Growth Marketing works across teams and one way we stay efficient is by identifying and working with stable counterparts across GitLab. Identifying [stable counterparts](/blog/2018/10/16/an-ode-to-stable-counterparts/) help us know where to turn when we work on specific processes and share knowledge from our team.

### Current Stable Counterparts
* Shane Rice <> Dara Warde (Marketing Ops)
* Danielle Morrill <> Hila Qu (Growth)
* Michael Preuss <> Jean du Plessis (Static Site Editor)

## Communication Overview

### Meeting Cadence

Most of our team meetings are recorded, and can be viewed on GitLab Unfiltered in the [Growth Marketing playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp0T5rN49dxNeJ5uuJ73wMl).

* Monday team leads kickoff to review priorities and blockers
* Friday full team results recap & demo day to share completed work


#### The Handbook 
Is our single source of truth (SSoT) for processes and relevant links

*  All processes at the Growth Marketing team level will be put in the Growth Marketing Overarching Handbook. Individual team should link back to these SSoT sections to avoid confusion. 
*  Individual teams within Growth Marketing will create their handbook sections for specifics to their teams
*  The handbook will be iterated on as we establish and test processes

#### When you submit an issue 
You can expect these communications/notifications (either through GitLab or in a comment)

*  You will be provided with specific templates to help you input relevant information
*  Those issues will be vetted at the beginning of each week. The issue will either be: 
   *  Moved into `mktg-status::WIP` or;
   *  A comment will be added for what additional information is needed
*  The issues moved to `mktg-status::WIP` are then put into a sprint and assigned to a week sprint based on priorities, resources, weights and assignment loads
*  The `Assignee` will comment that they have received your Issue and when you can expect more information (ex: "On it. Will respond within the week on timeline")
*  If needed, the Issue Brief will be broken into a Project Epic with relevant issues and those issues re-assigned to the relevant DRIs as `Assignee(s)` 
*  A timeline  will be provided in that Project Epic or individual issue and the due date will be adjusted accordingly.
*  When feedback is needed the `Assignee(s)` will comment in the issue(s) asking for your input. They will be as specific as possible with what they are looking for comments on. 
*  Final deliverables will be added in the appropriate form (file, link to repository, web link, etc.) and the Assignee(s) will @ you
*  When final deliverables have been completed the `Assignee(s)` will close the issue. If additional items are needed please open a new issue and relate to each other.  
*  If an issue needs to be moved to the next milestone the `Assignee` will comment in the issue with the reason (ex: problems with testing)

## Requesting Support
Please fill out one of these Issue Templates to request support. Please note, if these are not filled out we won't have the proper information for us to support your request

#### Website Issue Templates
* [Requesting a new webpage](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-new-website-brief)
  * Use this template to request new webpage on logged-out gitlab.com, or about.gitlab.com
  * Do NOT use this for logged-in (In-App) gitlab.com or the Handbook
* [Requesting an update to an existing webpage](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-update-website-brief)  
  * Use this template to update an existing webpage on logged-out gitlab.com, or about.gitlab.com
  * Do NOT use this for logged-in (In-App) gitlab.com or the Handbook
* [Requesting homepage promotion](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-website-homepage-promotion)
   * Use this template to request to be put on the content calendar for homepage promotion
   * Note: this is a request and must be approved 
   * If this is for a campaign, please put in the request as part of the initial campaign distribution plan 
  
#### Brand and Design Issue Templates
  * [Requesting a new design](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=design-request-general)
    * Use this template to request a new design of a single asset
    * Do NOT use this template for complicated campaign design support
    * Do NOT use this template for brand reviews
  * [Requesting Brand review](https://gitlab.com/gitlab-com/marketing/growth-marketing/growth/-/issues/new?issuable_template=request-brand-review)
    * Use this template to request a brand or desgin review 
    * Do NOT use this template for requesting new assets or designs
   
## Boards
Overarching Marketing Growth Boards

#### [Triage Board](https://gitlab.com/groups/gitlab-com/-/boards/1761578?&label_name[]=mktg-growth&label_name[]=mktg-status%3A%3Atriage) 

*  **GOAL:** Weed out issues that superfluous or don’t contain extra information 
*  **ACTIONS:** Move to either
   *  Move to either `mktg-status:WIP` which puts into the Sprint cycle or;
   *  [mktg-status: blocked] and comment why it is being blocked

#### [Sprint Board](https://gitlab.com/groups/gitlab-com/-/boards/1761580?label_name[]=mktg-growth&label_name[]=mktg-status%3A%3Awip)
*  **GOAL:** Assign issues to milestones to be worked on then based on due dates, weights, priorities, resources
*  **ACTIONS:** 
   *  Move to a specific week taking into consideration the above 
   *  Backlog if:
      *  It doesn’t fit into current goals or priorities
      *  We don't have the time or resources currently
      *  It is just a cool idea (some of these may be labeled for future review for upcoming strategies & tactics) 
   *  Comment why it is backlogged
   *  Never backlog PR issues without prior discussion

   > HANDOFF: This is where the handoff to Growth Marketing Teams takes place to run their processes

#### [Growth Marketing Leads Overview](https://gitlab.com/groups/gitlab-com/-/boards/1787553?milestone_title=%23started&&label_name[]=mktg-growth&label_name[]=mktg-status%3A%3Awip)
* **GOAL:** An overview of the work on each of the Growth Marketing Teams during the current Milestone Sprint
* **ACTIONS:**
  * Review and help prioritze
  
## Labels
Meanings and usage

*  `mktg-growth`
   *  Denotes it could be part of the Growth Marketing Teams scope. 
   *  Used to help pull in boards
   *  Is not a label to denote the status of an issue
*  `mktg-status::triage`
   *  Universal marketing label
   *  Is used to denote the status of an issue as being reviewed to go into the working pipeline
   *  Does not denote the timeline it will be worked on
*  `mktg-status::WIP`
   *  Universal marketing label
   *  Is used to denote the status of an issue as having been reviewed for all details needed and will go into the working pipeline
   *  Does not denote the timeline it will be worked on
*  `mktg-status::blocked`
   *  Universal marketing label
   *  Is used to denote the status of an issue as not having enough information to proceed
   *  Does not denote the timeline it will be worked on
   *  This should ONLY be used in the Triage process or if an issue becomes so blocked it needs to be re-scheduled. 
      * If an issue is blocked it goes back into the Triage process and is scheduled from there
*  `mktg-status::review`
   *  Universal marketing label
   *  Is used to denote the status of an issue as in the peer-review process.
   *  Does not denote the timeline it will be worked on
*  [Group Labels] `design`, `web-analytics`, `mktg-website`, `global content`
   *  Denotes it could be part of a Growth Marketing Group scope
   *  Is not a label to denote the status of an issue
   * [Individual Team Labels] `content marketing`, `editorial`, `digital-production`, `Brand and Digital Design`
      * Denotes it could be part of Growth Marketing Group Team's scope
      * Is not a label to denote the status of an issue
*  `MG-Support`
   *  Denotes if this is support provided to another team outside of Growth Marketing
   *  Will be used to help quickly view how many issues Growth Marketing supported each year to provide as references for budget and resources requests.  
*  `MG-Review-Future`
   *  Will be reviewed by Marketing Growth for potential future strategy 

## Milestones
Meanings and usage

* `Fri: MONTH DAY`
   * Denotes which week an issue will be worked on 
   * Worked on = will be broken down into smaller Issues or Issue will be completed and closed
* `MG-Backlog:Goal` 
   *  Denotes that this not fit into current goals - can be reviewed later for informing future goals
   *  Does NOT mean the issue request is cancelled
*  `MG-Backlog:Time/Resources`
   *  Fits into goals, but;
   *  Needs to be reviewed later because we don't have the time or resources during this review
   *  Does NOT mean the issue request is cancelled
*  `MG-Backlog:General`
   *  Denotes general ideas that are cool ideas but doesn't move the boat forward. Will be reviewed when there is extra time or resources.

## Issues 

### Issue Types
This is how we work with Issues and their scope
#### External
(outside of Growth Marketing)
**REQUEST ONLY ISSUE**
*  This is for complicated projects (example: campaign). 
*  This type of Issue is submitted by a team OUTSIDE of Growth Marketing. 
*  This serves as a brief to us. 
*  For each type of request we have a corresponding  **Request [Type] Issue Template** that should be filled out to begin the process. 
     * [Requesting Support](https://about.gitlab.com/handbook/marketing/growth-marketing/#requesting-support)
* Actions taken on issue: 
  * Reviewed for all information and label changed to `mktg-status::WIP` if all there or `mktg-status::blocked` if not all info is there
  * Assigned to a team member to execute on
  * Assigned to a Sprint week to be worked on (worked on= broken down into Production Issues)
  * Issue is added to a child Epic and execution issues are made for creating
  * Assignee will comment and close the Request Issue and deliver the work in the Child Epic

**REQUEST AND PRODUCTION ISSUE**
* This is for a simple ask (example: single asset)
* This type of Issue is submitted by a team outside of Growth Marketing and will contain relevant information for the project AND;
* Execution Items/Production Checklists for the Growth Marketing Teams.
* For each type of request we have a corresponding **Request [Type] Issue Template** that should be filled out to begin the process. 
* Actions taken on issue: 
  * Reviewed for all information and label changed to `mktg-status::WIP` if all there or `mktg-status::blocked` if not all info is there
  * Assigned to a team member to execute on
  * Assigned to a Sprint week to be worked on (worked on = deliverables created)
  * Assignee will follow the production checklist and deliver work within the original Issue

#### Internal
(inside of Growth Marketing)

**PRODUCTION ISSUE**
 * This is an Issue we use internally to create and execute on the request/brief. 
 * These are only used by people inside of Growth Marketing 
 * For each type of request we have a corresponding  **Production [Type] Issue Template** that should be filled out to begin the process. 
 * The teams will deliver the individual pieces to these Issues, but not the completed ask. That should be delivered into the Child Epic created


## Issue Templates

#### External: 
(outside of Growth Marketing): 

**Issue Author**
* Pick template based on Handbook directions 
  * Template tags will be automatically added:
    * `mktg-growth`
    * `mktg-status::triage`
    * The relevant Group Label: `design`, `content marketing`, `mktg-website`, or `mktg-analytics`
    * `MG-Support` 
  * Template contains specific information to help fill out including directions for picking a due date
  * Template has auto-assignees
  * Links to Handbook page for additional information

#### Internal: 
(inside of Growth Marketing): 

**Assignees of Issues:**
*  Pick template for breakout of brief
  *  Template tags: 
     *  Keep existing above EXCEPT if we start the project then we remove `MG-Support`
     *  Add your team's process tags
* If the ask (External brief Issue) requires one ask:
  * Re-assign as need, make sure it is in a sprint week and follow documented processes
* If the ask (External brief Issue) requires more than one ask:
   * Create an Epic to hold project
   * Copy and paste markdown into Epic Description and use header: “External Brief”
   * Associate Issue with Epic 
   * Comment Close Issue that the project epic has been created
   * Build out necessary Issues in project, Associate with Epic, assign Issues and update Epic Description as needed with additional brief information 


## Weekly Process

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/xeFP0-Kdq-M" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

**Beginning of week:** 

*  Geekbot Check-In
* Triage board is vetted and items moved (see above for details on actions)
*  Growth Team Leadership meeting: 
   *  Discuss priorities for the week, Danielle relays exec and PR info, we decide if any project, company or larger Mkrg priorities are bubbling up that would make us reconsider priorities 
   *  Groom issues into weeks using the Sprint Board 

> HANDOFF: This is where the handoff to Growth Marketing Teams takes place to run their processes

*  Growth Marketing Leads meets with their teams (anyway they want: meeting, slack, async, geek bot) and relays that information
*  Teams: Assign (if not auto-assigned) Issues, Projects, Tasks and follow their process 


**End of week:**
*  Confirm if everything has been closed and move to next milestone sprint if not completed
*  Comment in issue why it was moved to the next milestone (ex: person x out sick, complications with testing, etc.)

