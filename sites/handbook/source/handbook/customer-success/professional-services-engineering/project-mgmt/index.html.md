---
layout: handbook-page-toc
title: "Professional Services Project Management"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

Gitlab takes our customer's success very seriously. In Professional Services (PS) we strive to provide a first class experience for our engagements. 

### PS methodology framework

#### 1. Initiate

* Scope project & success criteria 
* Solution alignment with business requirements
* Define project team and deliverables
* Customer initiation

#### 2. Plan and design

* Assessment & solution requirements
* Product awareness session
* Communication plan
* Detailed project plan
* Org readiness plan

#### 3. Configure, integrate, and test

* Install system
* System configuration
* Integration design
* System testing
* Admin training
* Prepare end-user training
* User acceptance testing preparation

#### 4. Validate

* User acceptance test
* Train end-users 
* Org readiness

#### 5. Deploy and close
* Implement production
* Go live support
* Monitor
* Stabilize
* Engagement closure & audit

### Project plan

Each services engagement will include a Project Plan compiled based on the Statement of Work (SOW) and other discussions the PS team has with the customer.  This can include a Gantt chart, spreadsheet, or another form of plan.

Each service engagement will have a Google Sheet that shows the contracted revenue and the estimated cost measured in hours or days of effort required to complete the engagement. The cost estimate must be completed prior to SoW signature and attached to the opportunity in SFDC.

### Project workflow

1. Sr. PS Project Coordinator: Once an SOW has been approved and moved for Closed Won, PS project/ issue is created and assigned a project owner.  This can be Project Manager, Technical Project Manager, or Professional Services Engineer (PSE).

1. Sr. PS Project Coordinator: Send [initiation email](https://docs.google.com/document/d/1je9dqVJpFFMv7aw9WhPeQ8aufx6Sj3OZveqaHHd212w/edit) and [existing customer initiation email](https://docs.google.com/document/d/1eNPXLmstMLoatpOBIhxrJgnPFgqYByPaJoFQRd2kz9U/edit).

1. Project Owner: Begins project with processes defined here
    1. Internal kickoff meeting
    1. Request PSE be assigned to project, if they are not the owner, via comment to the Sr. PS Project Coordinator in the project/ issue 
    1. Project kickoff: Use this [Kick-off slide template](https://docs.google.com/presentation/d/1HtVIE64N94Rcc774ujllClGmYZ5y1_ApE4-O3pazR6k/edit#slide=id.g59bfc474c5_2_145) and this [agenda/meeting minuntes template](https://docs.google.com/document/d/1WPnBQUOT2dug8rPkA-VFzXtE1AlQJGMMROQPhHOh4Bg/edit)
    1. Weekly project meeting (if applicable): Use [this template](https://docs.google.com/document/d/1WPnBQUOT2dug8rPkA-VFzXtE1AlQJGMMROQPhHOh4Bg/edit)
    1. Provide weekly Executive Status Report: Use [this template](https://docs.google.com/document/d/1tPsQbaq36zs4oKh6LXKXQPGy4Dmk7gbQfiZN4duu81o/edit)
    1. Change request when required for scope changes: Use [this workflow](https://docs.google.com/document/d/1zed5AsEpjzwII0HaIjsmXYaRAp5qHY-BGJfVCISVGcM/edit https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#how-to-process-a-professional-services-sow-amendment-change-order)
    1. Project closure meeting: Use [this agenda template](https://docs.google.com/document/d/1Cw5eLe8VKFtHG9xGqUiCua8Pbu52reMzHujcPWq3ofQ/edit)
    
1. Sr. PS Project Manager or Technical Project Manager: Complete [this sign off workflow](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/workflows/project_execution/sign-off.html) 

1. Sr. PS Project Manager or Technical Project Manager: Complete this [financial wrap-up workflow](https://about.gitlab.com/handbook/customer-success/professional-services-engineering/workflows/internal/financial-wrapup.html)
