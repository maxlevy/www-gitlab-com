# Checks the structure of the monorepo, to ensure no files are created in incorrect locations
require 'psych'

module Lint
  class MonorepoStructure
    attr_reader :monorepo_root, :sites_dir, :sites

    def run
      @monorepo_root = File.expand_path('../..', __dir__)
      @sites_dir = "#{monorepo_root}/sites"
      monorepo_config = Psych.load_file("#{monorepo_root}/data/monorepo.yml")
      @sites = monorepo_config.keys

      puts "\n=> Checking monorepo structure..."

      check_sites_subdirs
      check_each_site_subdirs
      check_root_source_dirs

      puts "\n=> Monorepo structure is good!"
    end

    private

    def check_sites_subdirs
      site_subdirs = FileUtils.chdir(sites_dir) do
        Dir.glob('*')
      end
      reject_ds_store!(site_subdirs)

      extra_dirs = site_subdirs - sites

      # Note: This guard clause is kind of confusing, but it's needed to keep Rubocop happy
      return if extra_dirs.blank?

      puts "Unexpected file(s) under 'sites' directory: #{extra_dirs.inspect}"
      exit_with_err
    end

    def check_each_site_subdirs
      sites_to_check = sites - ['blog'] # TODO: Skip checking 'blog' sub site until it is converted to new symlink-less approach
      sites_to_check.each do |site|
        site_dir = "#{sites_dir}/#{site}"
        FileUtils.chdir(site_dir) do
          site_dir_sub_files = Dir.glob('*')
          reject_ds_store!(site_dir_sub_files)

          # remove expected/allowed files
          site_dir_sub_files.reject! do |f|
            [
              /config.*\.rb/, # middleman config files
              /^source$/
            ].any? { |regex| f =~ regex }
          end

          unless site_dir_sub_files.empty?
            puts "Unexpected file(s) under '#{site_dir}' directory: #{site_dir_sub_files.inspect}"
            exit_with_err
          end
        end
      end
    end

    def check_root_source_dirs
      sites_to_check = sites - ['blog'] # TODO: Skip checking 'blog' sub site until it is converted to new symlink-less approach

      sites_to_check.each do |site|
        source_subdir = "#{monorepo_root}/source/#{site}"

        FileUtils.chdir(source_subdir) do
          source_subdir_sub_files = Dir.glob('*')
          reject_ds_store!(source_subdir_sub_files)

          # remove expected/allowed files
          expected_readme = source_subdir_sub_files.delete('README')
          unless expected_readme
            puts "There should be a 'README' under the '#{source_subdir}' directory"
            exit_with_err
          end

          unless source_subdir_sub_files.empty?
            puts "Unexpected file(s) under '#{source_subdir}' directory: #{source_subdir_sub_files.inspect}"
            exit_with_err
          end
        end
      end
    end

    # Remove '.DS_Store' (created by Finder on MacOS) from an array of file basenames
    def reject_ds_store!(files)
      files.reject! { |d| d =~ /.DS_Store/ }
    end

    def exit_with_err
      exit 1
    end
  end
end
